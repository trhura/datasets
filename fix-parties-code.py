import string
import json

exclude = set(string.punctuation + string.whitespace)
party_key = lambda p: "".join(c.lower() for c in p if c not in exclude)

with open('parties.json', 'r') as iFile:
    parties = json.load(iFile)


with open('election_records.json', 'r') as iFile:
    records = json.load(iFile)

parties_cbn = {}
for p in parties['parties']:
    pk = party_key(p['name']['en'])
    parties_cbn[pk] = p['code']
del parties_cbn['0']

from difflib import SequenceMatcher
def similar(a, b):
        return SequenceMatcher(None, a, b).ratio()

def getcode (party):
    if party.isupper(): return party # already captalize
    pk = party_key(party)
    if pk in parties_cbn:
        return parties_cbn[pk]
    else:
        ratio, key = max(map(lambda x: (similar(pk, x[0]), x[1]), parties_cbn.items()), key=lambda x: x[0])
        if ratio > 0.9:
            return key
        else:
            return party

for r in records:
    r['party'] = getcode(r['party'])

with open('out.json', 'w') as oFile:
    json.dump(records, oFile, indent=4, ensure_ascii=False)
