var c2010 = [
  {
    "votes": 3502,
    "candidate": {
      "name": {
        "my": "ဦးချိုဝင်း",
        "en": "U CHO Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": " Peace and Diversity Party",
    "constituency": "MMR017024-1"
  },
  {
    "votes": 2455,
    "candidate": {
      "name": {
        "my": "ဦးစောစိန်ဦး",
        "en": "U SAW SEIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017024-1"
  },
  {
    "votes": 4887,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဝင်းအေး",
        "en": "U Chit Win Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017024-1"
  },
  {
    "votes": 41856,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဝင်း",
        "en": "U MYO WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017024-1"
  },
  {
    "votes": 17721,
    "candidate": {
      "name": {
        "my": "ဦးသန်း",
        "en": "U THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017024-1"
  },
  {
    "votes": 2444,
    "candidate": {
      "name": {
        "my": "ဦးမြဝင်း",
        "en": "U MYA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": " Peace and Diversity Party",
    "constituency": "MMR017024-2"
  },
  {
    "votes": 3084,
    "candidate": {
      "name": {
        "my": "ဦးရဲဝင်း",
        "en": "U YE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017024-2"
  },
  {
    "votes": 7464,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်စုစုအောင်",
        "en": "Daw Khin Su Su Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017024-2"
  },
  {
    "votes": 41460,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမောင်",
        "en": "U SAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017024-2"
  },
  {
    "votes": 19851,
    "candidate": {
      "name": {
        "my": "ဦးလှအေး",
        "en": "U HLA AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017024-2"
  },
  {
    "votes": 5031,
    "candidate": {
      "name": {
        "my": "ဦးစောထူးဖလှယ်",
        "en": "U SAW HTOO PHALAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017022-1"
  },
  {
    "votes": 31050,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ဝင်း",
        "en": "U NAING WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017022-1"
  },
  {
    "votes": 11561,
    "candidate": {
      "name": {
        "my": " ဒေါ်တင်တင်အေး",
        "en": "DAW TIN TIN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017022-1"
  },
  {
    "votes": 31518,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017022-2"
  },
  {
    "votes": 16402,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရေွှ",
        "en": "U TIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017022-2"
  },
  {
    "votes": 7419,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်းထွန်း",
        "en": "U ZAW WIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017026-1"
  },
  {
    "votes": 32673,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဦး",
        "en": "U MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017026-1"
  },
  {
    "votes": 30248,
    "candidate": {
      "name": {
        "my": "ဦးသိမ်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017026-2"
  },
  {
    "votes": 18959,
    "candidate": {
      "name": {
        "my": "ဦးတင်အုန်း",
        "en": "U TIN OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017026-2"
  },
  {
    "votes": 25247,
    "candidate": {
      "name": {
        "my": "ဦးမန်းနယ်လ်ဆင်",
        "en": "MAHN NELSON"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017015-1"
  },
  {
    "votes": 25517,
    "candidate": {
      "name": {
        "my": "ဦးငြိမ်းရေွှ",
        "en": "U NYEIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017015-1"
  },
  {
    "votes": 8330,
    "candidate": {
      "name": {
        "my": "ဦးမန်းဘဲလေ",
        "en": "U MAHN BLAIR"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017015-2"
  },
  {
    "votes": 21882,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": "U MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017015-2"
  },
  {
    "votes": 19036,
    "candidate": {
      "name": {
        "my": "ဦးမန်းအေးသိန်း",
        "en": "MAHN THEIN AY E"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017015-2"
  },
  {
    "votes": 69668,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017008-1"
  },
  {
    "votes": 18830,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017008-1"
  },
  {
    "votes": 4724,
    "candidate": {
      "name": {
        "my": "ဦးစောဌေးဝင်း",
        "en": "U SAW HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017008-2"
  },
  {
    "votes": 16792,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သောင်း",
        "en": "U KHIN MAUNG THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017008-2"
  },
  {
    "votes": 66799,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသိမ်းစိုး",
        "en": "DR. THEIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017008-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "Y",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017013-1"
  },
  {
    "votes": 12618,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": "U TIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017013-2"
  },
  {
    "votes": 49394,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်စိုး",
        "en": "U MYAT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017013-2"
  },
  {
    "votes": 7523,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မော်စိုး",
        "en": "U MAUNG MAW SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017002-1"
  },
  {
    "votes": 16176,
    "candidate": {
      "name": {
        "my": "ဦးချစ်လွင်",
        "en": "U CHIT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017002-1"
  },
  {
    "votes": 13241,
    "candidate": {
      "name": {
        "my": "ဦးချစ်လိှုင်",
        "en": "U CHIT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017002-1"
  },
  {
    "votes": 8150,
    "candidate": {
      "name": {
        "my": "ဦးမန်းစံထွန်း",
        "en": "U MAHN SAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017002-2"
  },
  {
    "votes": 23171,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်စောမူ",
        "en": "DAW KHIN SAW MU"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017002-2"
  },
  {
    "votes": 12961,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းလိှုင်",
        "en": "U OHN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017002-2"
  },
  {
    "votes": 6013,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017025-1"
  },
  {
    "votes": 33346,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လှ",
        "en": "U KYAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017025-1"
  },
  {
    "votes": 11850,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017025-1"
  },
  {
    "votes": 3509,
    "candidate": {
      "name": {
        "my": "ဦးစန်းဌေး",
        "en": "U SAN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017025-2"
  },
  {
    "votes": 8381,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017025-2"
  },
  {
    "votes": 7400,
    "candidate": {
      "name": {
        "my": "ဦးတင့်အောင်(ခ) ဦးပေါက်",
        "en": "U TINT AUNG (A) U PAUGHT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017025-2"
  },
  {
    "votes": 25936,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိုးအောင်",
        "en": "U THAN SOE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017025-2"
  },
  {
    "votes": 12065,
    "candidate": {
      "name": {
        "my": "ဦးခင်ကျော်ညွန့်",
        "en": "U KHIN KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017012-1"
  },
  {
    "votes": 11789,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဌေးဝင်း",
        "en": "U THEIN HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017012-1"
  },
  {
    "votes": 13049,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017012-2"
  },
  {
    "votes": 13425,
    "candidate": {
      "name": {
        "my": "ဦးစိုးပိုင်",
        "en": "U SOE PAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017012-2"
  },
  {
    "votes": 18600,
    "candidate": {
      "name": {
        "my": "ဦးခင်စိုး",
        "en": "U KHIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017007-1"
  },
  {
    "votes": 24235,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာထိန်ဝင်း",
        "en": "DR. HTEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017007-1"
  },
  {
    "votes": 17345,
    "candidate": {
      "name": {
        "my": "ဦးမန်းမျိုးဝင်းထွန်း",
        "en": "MAHN MYO WIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017007-2"
  },
  {
    "votes": 23234,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းနိုင်",
        "en": "U THEIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017007-2"
  },
  {
    "votes": 33933,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017005-1"
  },
  {
    "votes": 26072,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းမြင့်သန်း",
        "en": "NAN MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017005-1"
  },
  {
    "votes": 36841,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017005-2"
  },
  {
    "votes": 24503,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမောင်",
        "en": "U SAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017005-2"
  },
  {
    "votes": 48985,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဆေွ",
        "en": "U WIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017016-1"
  },
  {
    "votes": 52424,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017016-1"
  },
  {
    "votes": 59912,
    "candidate": {
      "name": {
        "my": "ဦးထိန်လင်း",
        "en": "U HTEIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017016-2"
  },
  {
    "votes": 46372,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းသိန်း",
        "en": "U TUN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017016-2"
  },
  {
    "votes": 17532,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်းနိုင်",
        "en": "U KYAW WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017010-1"
  },
  {
    "votes": 11552,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မင်းသိန်း",
        "en": "U ZAW MIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017010-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017010-2"
  },
  {
    "votes": 3820,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မင်း",
        "en": "U ZAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017019-1"
  },
  {
    "votes": 67580,
    "candidate": {
      "name": {
        "my": "ဦးရန်ဝင်း",
        "en": "U YAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017019-1"
  },
  {
    "votes": 10769,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သောင်း",
        "en": "U AUNG THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017019-1"
  },
  {
    "votes": 5357,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမင်း",
        "en": "U TUN MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017019-2"
  },
  {
    "votes": 55803,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017019-2"
  },
  {
    "votes": 14832,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017019-2"
  },
  {
    "votes": 6724,
    "candidate": {
      "name": {
        "my": "ဦးစိုးရည်",
        "en": "U SOE YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017018-1"
  },
  {
    "votes": 50947,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇော်လှုိင်",
        "en": "U AUNG ZAW HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017018-1"
  },
  {
    "votes": 21166,
    "candidate": {
      "name": {
        "my": "ဦးမန်းခိုင်ထွဏ်း",
        "en": "MAHN KHAING TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017018-1"
  },
  {
    "votes": 12347,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်စိုး",
        "en": "U AUNG KYAW SOW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017018-2"
  },
  {
    "votes": 42289,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017018-2"
  },
  {
    "votes": 22148,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017018-2"
  },
  {
    "votes": 39525,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုးညွန့်",
        "en": "U AUNG MYO NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017011-1"
  },
  {
    "votes": 13795,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမြင့်",
        "en": "U HTAY MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017011-1"
  },
  {
    "votes": 40680,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်းဆေွ",
        "en": "U AUNG WIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017011-2"
  },
  {
    "votes": 13303,
    "candidate": {
      "name": {
        "my": "ဦးရဲဆေွ",
        "en": "U YE SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017011-2"
  },
  {
    "votes": 39839,
    "candidate": {
      "name": {
        "my": "ဦးပြုံးချို",
        "en": "U PYONE CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017014-1"
  },
  {
    "votes": 13333,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်အောင်",
        "en": "U NYAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017014-1"
  },
  {
    "votes": 14061,
    "candidate": {
      "name": {
        "my": "ဦးမန်းအောင်ကျော်ဦး",
        "en": "MAHN AUNG KYAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017014-1"
  },
  {
    "votes": 39136,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကိုကို",
        "en": "U WIN KOKO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017014-2"
  },
  {
    "votes": 19726,
    "candidate": {
      "name": {
        "my": "ဦးစောရီချက်",
        "en": "SAW RICHARD"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017014-2"
  },
  {
    "votes": 14839,
    "candidate": {
      "name": {
        "my": "ဦးစောကျော်ဇော",
        "en": "U SAW KYAW ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017004-1"
  },
  {
    "votes": 18913,
    "candidate": {
      "name": {
        "my": "ဦးဟန်တင်မောင်",
        "en": "U HAN TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017004-1"
  },
  {
    "votes": 6137,
    "candidate": {
      "name": {
        "my": "ဦးစောမိုးခိုင်",
        "en": "U SAW MOE KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017004-2"
  },
  {
    "votes": 11698,
    "candidate": {
      "name": {
        "my": "ဦးအေးကြည်",
        "en": "U AYE KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017004-2"
  },
  {
    "votes": 7983,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအေး",
        "en": "U THAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017004-2"
  },
  {
    "votes": 5309,
    "candidate": {
      "name": {
        "my": "ဦးစန်းသောင်း",
        "en": "U SANN THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017021-1"
  },
  {
    "votes": 22297,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာလှမင်း",
        "en": "DR. HLA MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017021-1"
  },
  {
    "votes": 18523,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သင်",
        "en": "U MAUNG MAUNG THIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017021-1"
  },
  {
    "votes": 4085,
    "candidate": {
      "name": {
        "my": "ဦးထိန်လင်း",
        "en": "U HTEIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017021-1"
  },
  {
    "votes": 4640,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကြိုင်",
        "en": "U WIN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017021-2"
  },
  {
    "votes": 27091,
    "candidate": {
      "name": {
        "my": "ဦးဘုန်းမြင့်",
        "en": "U BON MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017021-2"
  },
  {
    "votes": 19952,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဆေွ",
        "en": "U TINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017021-2"
  },
  {
    "votes": 29115,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017020-1"
  },
  {
    "votes": 29680,
    "candidate": {
      "name": {
        "my": "မန်းသက်တင်",
        "en": "MAHN THET TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017020-1"
  },
  {
    "votes": 2368,
    "candidate": {
      "name": {
        "my": "ဦးမန်းကြွယ်ဝင်း",
        "en": "U MAHN KYWE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017020-2"
  },
  {
    "votes": 29742,
    "candidate": {
      "name": {
        "my": "ဦးငြိမ်းမြိုင်",
        "en": "U NYAING MYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017020-2"
  },
  {
    "votes": 28885,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017020-2"
  },
  {
    "votes": 53716,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်ခင်စီ",
        "en": "DR. KHIN KHIN SI"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017001-1"
  },
  {
    "votes": 25171,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်မြင့်",
        "en": "U MAUNG MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017001-1"
  },
  {
    "votes": 48814,
    "candidate": {
      "name": {
        "my": " ဦးအောင်ကျော်စိန်",
        "en": "U AUNG KYAW SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017001-2"
  },
  {
    "votes": 48814,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017001-2"
  },
  {
    "votes": 4011,
    "candidate": {
      "name": {
        "my": "ဦးလွင်ဦး",
        "en": "U LWIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017023-1"
  },
  {
    "votes": 50640,
    "candidate": {
      "name": {
        "my": "ဦးလှဝင်းဗိုလ်",
        "en": "U HLA WIN BO"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017023-1"
  },
  {
    "votes": 13893,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွဋ်",
        "en": "U TIN HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017023-1"
  },
  {
    "votes": 1849,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းတန်",
        "en": "U THEIN DAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017023-2"
  },
  {
    "votes": 40901,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017023-2"
  },
  {
    "votes": 21614,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းနိုင်ဌေး(ခ)ဦးမောင်နီ",
        "en": "U THAUNG NAING TAY (A) U MAUNG NE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017023-2"
  },
  {
    "votes": 20595,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသိန်း",
        "en": "U WIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017003-1"
  },
  {
    "votes": 15969,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသိန်း",
        "en": "U SOE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017003-1"
  },
  {
    "votes": 9710,
    "candidate": {
      "name": {
        "my": "ဦးစောဖလန်စစ်",
        "en": "U SAW FRANCIS"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017003-2"
  },
  {
    "votes": 20262,
    "candidate": {
      "name": {
        "my": "ဦးစောလေးထော",
        "en": "SAW LAY TAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017003-2"
  },
  {
    "votes": 9771,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်သန်းမြင့်",
        "en": "DAW KHIN THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017003-2"
  },
  {
    "votes": 4233,
    "candidate": {
      "name": {
        "my": "ဦးမိုးကျော်",
        "en": "U MOE KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017017-1"
  },
  {
    "votes": 31987,
    "candidate": {
      "name": {
        "my": "ဦးစတင်ထွဋ်",
        "en": "SA TIN HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017017-1"
  },
  {
    "votes": 34247,
    "candidate": {
      "name": {
        "my": "စောမြသိန်း",
        "en": "SAW MYA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017017-1"
  },
  {
    "votes": 38361,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လွင်",
        "en": "U KYAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017017-2"
  },
  {
    "votes": 27168,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိုး",
        "en": "U AUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017017-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းဆင့်",
        "en": "U SAN SINT"
      }
    },
    "acclaim": "Y",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017006-1"
  },
  {
    "votes": 23423,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းဟန်",
        "en": "U THAUNG HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017006-2"
  },
  {
    "votes": 20447,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ကြိုင်",
        "en": "U MYINT KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017006-2"
  },
  {
    "votes": 2860,
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်မျိုးကျော်",
        "en": "U SAW AUNG MYO KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017009-1"
  },
  {
    "votes": 31427,
    "candidate": {
      "name": {
        "my": "ဦးလှခိုင်",
        "en": "U HLA KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017009-1"
  },
  {
    "votes": 9756,
    "candidate": {
      "name": {
        "my": "ဦးမြသောင်း",
        "en": "U MYA THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017009-1"
  },
  {
    "votes": 4550,
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်မြသန်း",
        "en": "U SAW AUNG MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017009-2"
  },
  {
    "votes": 6423,
    "candidate": {
      "name": {
        "my": "ဦးစံအေး",
        "en": "U SAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017009-2"
  },
  {
    "ethnic_seat": true,
    "votes": 204692,
    "acclaim": "N",
    "ethnicity": " Kayin",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017009",
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစောစိုင်းမွန်သာ",
        "en": "DR. SIMON THA"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 215398,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017009",
    "candidate": {
      "name": {
        "my": "မန်းသန်းရေွှ",
        "en": "MAHN THAN SHWE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 173896,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017009",
    "candidate": {
      "name": {
        "my": "မန်းသိန်းလှ",
        "en": "MAHN THEIN HLA"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 0,
    "acclaim": "Y",
    "ethnicity": "Rakhine",
    "parliament_code": "AYEYARWADY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017009",
    "candidate": {
      "name": {
        "my": "ဦးဘကြူး",
        "en": "U BA KYU"
      }
    }
  },
  {
    "votes": 40889,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းတင်",
        "en": "U WIN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007001-1"
  },
  {
    "votes": 32838,
    "candidate": {
      "name": {
        "my": "ဦးကောင်းမြတ်စံ",
        "en": "U KHAUNG MYAT SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007001-1"
  },
  {
    "votes": 6526,
    "candidate": {
      "name": {
        "my": "ဦးကြီးမြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR007001-2"
  },
  {
    "votes": 46515,
    "candidate": {
      "name": {
        "my": "ဦးရဲမြင့်ထွန်း",
        "en": "U YE MYINT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007001-2"
  },
  {
    "votes": 21582,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သန်း",
        "en": "U MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007001-2"
  },
  {
    "votes": 2363,
    "candidate": {
      "name": {
        "my": "ဦးစောထွန်းတင်",
        "en": "SAW TUN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR007001-2"
  },
  {
    "votes": 2489,
    "candidate": {
      "name": {
        "my": "ဦးထင်ပေါ်(ခ)ဦးတင်ယု",
        "en": "U HTIN PAW (A) U TIN YU"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR007001-2"
  },
  {
    "votes": 22599,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုဌေး",
        "en": "U KOKO HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007007-1"
  },
  {
    "votes": 12514,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007007-1"
  },
  {
    "votes": 25659,
    "candidate": {
      "name": {
        "my": "ဦးမင်းအောင်",
        "en": "U MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007007-2"
  },
  {
    "votes": 16843,
    "candidate": {
      "name": {
        "my": "ဦးစောမော်ရယ်",
        "en": "SAW MAW REH"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007007-2"
  },
  {
    "votes": 31310,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသိမ်း",
        "en": "U SOE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007003-1"
  },
  {
    "votes": 16450,
    "candidate": {
      "name": {
        "my": "ဦးမြသောင်း",
        "en": "U MYA THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007003-1"
  },
  {
    "votes": 33314,
    "candidate": {
      "name": {
        "my": "ဦးအေးနိုင်",
        "en": "U AYE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007003-2"
  },
  {
    "votes": 18323,
    "candidate": {
      "name": {
        "my": "ဦးကြည်လွင်",
        "en": "U KYI LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007003-2"
  },
  {
    "votes": 7161,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်",
        "en": "U MAUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007011-1"
  },
  {
    "votes": 12622,
    "candidate": {
      "name": {
        "my": "ဦးကောင်းညွန့်",
        "en": "U KAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007011-1"
  },
  {
    "votes": 6715,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဗိုလ်",
        "en": "U WIN BO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007011-2"
  },
  {
    "votes": 16036,
    "candidate": {
      "name": {
        "my": "ဦးဘေဘီအုန်း",
        "en": "U BABE OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007011-2"
  },
  {
    "votes": 29517,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဆေွ",
        "en": "U THEIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007006-1"
  },
  {
    "votes": 26347,
    "candidate": {
      "name": {
        "my": "ဦးမှင်",
        "en": "U MINN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007006-1"
  },
  {
    "votes": 29681,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်းဦး",
        "en": "U KYAW SAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007006-2"
  },
  {
    "votes": 25371,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စိုး",
        "en": "U MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007006-2"
  },
  {
    "votes": 22603,
    "candidate": {
      "name": {
        "my": " ဦးစောလင်းအောင်",
        "en": "SAW LIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007005-1"
  },
  {
    "votes": 16846,
    "candidate": {
      "name": {
        "my": " ဦးမြင့်နိုင်",
        "en": "U MYINT NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007005-1"
  },
  {
    "votes": 22101,
    "candidate": {
      "name": {
        "my": "ဦးစိုးပိုင်",
        "en": "U SOE PAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007005-2"
  },
  {
    "votes": 13686,
    "candidate": {
      "name": {
        "my": "ဦးဘုန်းကျော်",
        "en": "U BONG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007005-2"
  },
  {
    "votes": 23981,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထိုက်",
        "en": "U THAN TIGHT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007013-1"
  },
  {
    "votes": 12020,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး",
        "en": "U KHIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007013-1"
  },
  {
    "votes": 25040,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007013-2"
  },
  {
    "votes": 10798,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007013-2"
  },
  {
    "votes": 39109,
    "candidate": {
      "name": {
        "my": "ဦးတင့်အောင်",
        "en": "U TINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007012-1"
  },
  {
    "votes": 19459,
    "candidate": {
      "name": {
        "my": "ဦးဘိုသိန်း",
        "en": "U BO THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007012-1"
  },
  {
    "votes": 45941,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007012-2"
  },
  {
    "votes": 16630,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007012-2"
  },
  {
    "votes": 6903,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007008-1"
  },
  {
    "votes": 8943,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်နွဲ့",
        "en": "DAW KHIN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007008-1"
  },
  {
    "votes": 9196,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007008-2"
  },
  {
    "votes": 10217,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007008-2"
  },
  {
    "votes": 15502,
    "candidate": {
      "name": {
        "my": " ဦးခွန်တင်မြင့်",
        "en": "U KHUN TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007014-1"
  },
  {
    "votes": 9854,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007014-1"
  },
  {
    "votes": 15474,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဌေး",
        "en": "U KYAW HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007014-2"
  },
  {
    "votes": 9644,
    "candidate": {
      "name": {
        "my": "ဦးပေါ်ငြိမ်း",
        "en": "U PAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007014-2"
  },
  {
    "votes": 3159,
    "candidate": {
      "name": {
        "my": "ဦးစောမောင်မြ",
        "en": "U SAW MAUNG MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR007009-1"
  },
  {
    "votes": 56744,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007009-1"
  },
  {
    "votes": 16301,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007009-1"
  },
  {
    "votes": 7258,
    "candidate": {
      "name": {
        "my": "ဦးစောဒေးဗစ်အေးကို",
        "en": "U SAW DAVID AYE KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR007009-2"
  },
  {
    "votes": 45783,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်ဦး",
        "en": "DR. KYAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007009-2"
  },
  {
    "votes": 16394,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007009-2"
  },
  {
    "votes": 15245,
    "candidate": {
      "name": {
        "my": "ဦးအေးစိုး",
        "en": "U AYE SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007002-1"
  },
  {
    "votes": 8325,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007002-1"
  },
  {
    "votes": 2697,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သိန်း",
        "en": "U KYAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR007002-1"
  },
  {
    "votes": 18544,
    "candidate": {
      "name": {
        "my": "ဦးကိုကို",
        "en": "U KOKO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007002-2"
  },
  {
    "votes": 11155,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းလွင်",
        "en": "U WIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007002-2"
  },
  {
    "votes": 23304,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": "U AUNG NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007004-1"
  },
  {
    "votes": 14352,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဆေွ",
        "en": "U MYO SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007004-1"
  },
  {
    "votes": 23256,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ယု",
        "en": "U MAUNG YU"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007004-2"
  },
  {
    "votes": 11791,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ဆန်း",
        "en": "U MAUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007004-2"
  },
  {
    "votes": 30949,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမြင့်ထွန်း",
        "en": "U THEIN MYINT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007010-1"
  },
  {
    "votes": 13328,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ခင်",
        "en": "U CHIT KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007010-1"
  },
  {
    "votes": 28690,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သက်",
        "en": "U MAUNG MAUNG THET"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007010-2"
  },
  {
    "votes": 19067,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းကြိုင်",
        "en": "U OHN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007010-2"
  },
  {
    "votes": 10055,
    "candidate": {
      "name": {
        "my": "ဦးမင်းသွင်ဦး",
        "en": "U MIN THWIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008014-1"
  },
  {
    "votes": 6758,
    "candidate": {
      "name": {
        "my": "ဦးဆင့်လေး",
        "en": "U SINT LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008014-1"
  },
  {
    "votes": 13672,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်",
        "en": "U KHIN MAUNG LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008014-1"
  },
  {
    "votes": 8592,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008014-2"
  },
  {
    "votes": 7164,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008014-2"
  },
  {
    "votes": 13980,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်စိုးု",
        "en": "U WIN MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008014-2"
  },
  {
    "votes": 21845,
    "candidate": {
      "name": {
        "my": "ဒေါ်အေးအေးခိုင်",
        "en": "DAW AYE AYE KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008008-1"
  },
  {
    "votes": 16873,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းလိှုင်",
        "en": "U WIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008008-1"
  },
  {
    "votes": 17790,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်း",
        "en": "U KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008008-2"
  },
  {
    "votes": 11984,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008008-2"
  },
  {
    "votes": 5003,
    "candidate": {
      "name": {
        "my": "ဦးအေးထွန်း",
        "en": "U AYE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR008008-2"
  },
  {
    "votes": 18804,
    "candidate": {
      "name": {
        "my": "ဦးဌေးအောင်ကြွယ်",
        "en": "U HTAY AUNG KYEAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008009-1"
  },
  {
    "votes": 9143,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008009-1"
  },
  {
    "votes": 17225,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွဋ်အောင်",
        "en": "U YE HTUT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008009-2"
  },
  {
    "votes": 9201,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြ",
        "en": "U AUNG MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008009-2"
  },
  {
    "votes": 6248,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လတ်",
        "en": "U KHIN MAUNG LATT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR008013-1"
  },
  {
    "votes": 15112,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဝေ",
        "en": "U TUN WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008013-1"
  },
  {
    "votes": 11175,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်စန်းဝင်း",
        "en": "DAW KHIN SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008013-1"
  },
  {
    "votes": 13825,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းဌေးဦး",
        "en": "DR. WIN HTAY OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008013-2"
  },
  {
    "votes": 16831,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညွန့်",
        "en": "U KHIN MAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008013-2"
  },
  {
    "votes": 27791,
    "candidate": {
      "name": {
        "my": "ဦးမြစိုး",
        "en": "U MYA SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008012-1"
  },
  {
    "votes": 14260,
    "candidate": {
      "name": {
        "my": " ဦးမောင်သိမ်း",
        "en": "U MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008012-1"
  },
  {
    "votes": 27534,
    "candidate": {
      "name": {
        "my": "ဦးတင့်လွင်",
        "en": "U TIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008012-2"
  },
  {
    "votes": 15217,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းသိမ်း",
        "en": "U TUN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008012-2"
  },
  {
    "votes": 18025,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008010-1"
  },
  {
    "votes": 6149,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008010-1"
  },
  {
    "votes": 6519,
    "candidate": {
      "name": {
        "my": "ဦးဗိုလ်ဝင်းရှိန်",
        "en": "U BO WIN SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR008010-1"
  },
  {
    "votes": 20056,
    "candidate": {
      "name": {
        "my": "ဦးထိန်လင်း",
        "en": "U THEIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008010-2"
  },
  {
    "votes": 10401,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008010-2"
  },
  {
    "votes": 28785,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008003-1"
  },
  {
    "votes": 10441,
    "candidate": {
      "name": {
        "my": "ဦးစောထွဋ်လင်း",
        "en": "SAW HTUT LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008003-1"
  },
  {
    "votes": 25837,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဇော်",
        "en": "U THAN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008003-2"
  },
  {
    "votes": 10903,
    "candidate": {
      "name": {
        "my": "ဦးစောလိှုင်",
        "en": "U SAW HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008003-2"
  },
  {
    "votes": 14263,
    "candidate": {
      "name": {
        "my": " ဦးဝင်းထွန်းမြင့်",
        "en": "U WIN TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008002-1"
  },
  {
    "votes": 11575,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဌေး",
        "en": "U MYINT HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008002-1"
  },
  {
    "votes": 13875,
    "candidate": {
      "name": {
        "my": "ဦးလိှုင်မြင့်ဦး",
        "en": "U HLAING MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008002-2"
  },
  {
    "votes": 13059,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်မောင်",
        "en": "U TIN MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008002-2"
  },
  {
    "votes": 22747,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဦး",
        "en": "U AUNG NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008004-1"
  },
  {
    "votes": 11632,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမြင့်",
        "en": "U SAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008004-1"
  },
  {
    "votes": 22117,
    "candidate": {
      "name": {
        "my": " ဦးလှဖေ",
        "en": "U HLA PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008004-2"
  },
  {
    "votes": 11656,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008004-2"
  },
  {
    "votes": 34690,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်လင်း",
        "en": "U KYAW KYAW LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008001-1"
  },
  {
    "votes": 22083,
    "candidate": {
      "name": {
        "my": "ဦးလှရေွှ",
        "en": "U HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008001-1"
  },
  {
    "votes": 30905,
    "candidate": {
      "name": {
        "my": "ဦးမျိုး(ခ)ဦးမျိုးအောင်",
        "en": "U MYO KHA (A) MYO AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008001-2"
  },
  {
    "votes": 19699,
    "candidate": {
      "name": {
        "my": "ဦးရဲဝင်း",
        "en": "U YE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008001-2"
  },
  {
    "votes": 23538,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်မြင့်",
        "en": "U TIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008006-1"
  },
  {
    "votes": 9243,
    "candidate": {
      "name": {
        "my": "ဦးရေွှအုံး",
        "en": "U SHWE OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008006-1"
  },
  {
    "votes": 16950,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းထွန်းဦး",
        "en": "U TUN TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008006-2"
  },
  {
    "votes": 9288,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008006-2"
  },
  {
    "votes": 19856,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လွင်ဦး",
        "en": "U MYINT LWIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008007-1"
  },
  {
    "votes": 11873,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းကြွယ်",
        "en": "U OHN KYAWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008007-1"
  },
  {
    "votes": 4058,
    "candidate": {
      "name": {
        "my": "ဦးငေွလိှုင်",
        "en": "U NGWE HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008007-1"
  },
  {
    "votes": 20183,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008007-2"
  },
  {
    "votes": 9563,
    "candidate": {
      "name": {
        "my": "ဦးဖေအောင်သူ",
        "en": "U PAE AUNG THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008007-2"
  },
  {
    "votes": 4206,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းစိန်",
        "en": "U THAUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008007-2"
  },
  {
    "votes": 16361,
    "candidate": {
      "name": {
        "my": " ဒေါ်ဥမ္မာမို့မို့ဇော်",
        "en": "DAW OHNMAR MO MO ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008005-1"
  },
  {
    "votes": 13591,
    "candidate": {
      "name": {
        "my": "ဦးလှ၀င်း",
        "en": "U HLA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008005-1"
  },
  {
    "votes": 12908,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008005-2"
  },
  {
    "votes": 13792,
    "candidate": {
      "name": {
        "my": "ဦးရဲလင်းအောင်",
        "en": "U YE LIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008005-2"
  },
  {
    "votes": 7637,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MUANG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008005-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးညဏ်ဝင်း",
        "en": "U NYAN WIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008011-1"
  },
  {
    "votes": 4990,
    "candidate": {
      "name": {
        "my": "ဦးချို",
        "en": "U CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008011-2"
  },
  {
    "votes": 10357,
    "candidate": {
      "name": {
        "my": "ဦးထူးခိုင်",
        "en": "U THOO KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008011-2"
  },
  {
    "votes": 3640,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းချစ်",
        "en": "U TUN CHIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008011-2"
  },
  {
    "ethnic_seat": true,
    "votes": 25355,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008",
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်ဇေယျ",
        "en": "SAW AUNG ZAYA"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 33525,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008",
    "candidate": {
      "name": {
        "my": "ဦးစောကပေါ်လေး",
        "en": "SAW KAPAW LAY"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 34588,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "BAGO-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR0080",
    "candidate": {
      "name": {
        "my": "ဦးစောဂျူဘလီစံလှ",
        "en": "SAW JUBLIE SANHLA"
      }
    }
  },
  {
    "votes": 3889,
    "candidate": {
      "name": {
        "my": "ဦးငွန်လာလ်",
        "en": "U NGUN LAL"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004001-1"
  },
  {
    "votes": 2117,
    "candidate": {
      "name": {
        "my": "ဦးသန်းခင်",
        "en": "U THAN KIM"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004001-1"
  },
  {
    "votes": 4830,
    "candidate": {
      "name": {
        "my": "ဦးနထန်း",
        "en": "U NAH THANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004001-1"
  },
  {
    "votes": 3979,
    "candidate": {
      "name": {
        "my": "ဦးကောလ်လွယ်",
        "en": "U KUI LUAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004001-2"
  },
  {
    "votes": 513,
    "candidate": {
      "name": {
        "my": "ဦးလာသိန်းလီမာ",
        "en": "U LAR THEIN HLIMAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004001-2"
  },
  {
    "votes": 1043,
    "candidate": {
      "name": {
        "my": "ဦးခွန်လျန်း",
        "en": "U KHUANG LIAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004001-2"
  },
  {
    "votes": 4525,
    "candidate": {
      "name": {
        "my": "ဦးနိုးဆွန်း",
        "en": "U NO SUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004001-2"
  },
  {
    "votes": 3004,
    "candidate": {
      "name": {
        "my": "ဦးထန်ဟဲ",
        "en": "U HTANG HE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004002-1"
  },
  {
    "votes": 747,
    "candidate": {
      "name": {
        "my": "ဦးမေရီ",
        "en": "U HMAY GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004002-1"
  },
  {
    "votes": 4933,
    "candidate": {
      "name": {
        "my": "ဦးရမ်မန်း",
        "en": "U RAMAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004002-1"
  },
  {
    "votes": 746,
    "candidate": {
      "name": {
        "my": "ဦးလီယန်ချောင်",
        "en": "U LIAN CHUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004002-2"
  },
  {
    "votes": 5191,
    "candidate": {
      "name": {
        "my": "ဦးချန်ပွန်",
        "en": "U CHAN PUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004002-2"
  },
  {
    "votes": 2378,
    "candidate": {
      "name": {
        "my": "ဦးဟရမ်ကျဲအို",
        "en": "U HARAN KYA OH"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004002-2"
  },
  {
    "votes": 68,
    "candidate": {
      "name": {
        "my": "ဦးလှမာ",
        "en": "U HLA MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "ENDP",
    "constituency": "MMR004002-2"
  },
  {
    "votes": 3946,
    "candidate": {
      "name": {
        "my": "ဦးရေွှထီးအိုး",
        "en": "U SHWE HTEE O"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004003-1"
  },
  {
    "votes": 1125,
    "candidate": {
      "name": {
        "my": "ဦးတန်ဒီယန်",
        "en": "U HTAN DEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004003-1"
  },
  {
    "votes": 6185,
    "candidate": {
      "name": {
        "my": "ဦးလာလ်မောင်ကျုံး",
        "en": "U LAL MANG CUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004003-1"
  },
  {
    "votes": 4645,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမှုထန်",
        "en": "DR. HMU HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004003-2"
  },
  {
    "votes": 553,
    "candidate": {
      "name": {
        "my": "ဦးဘန်ဇာရိုင်",
        "en": "U VAN ZAR RONG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004003-2"
  },
  {
    "votes": 5480,
    "candidate": {
      "name": {
        "my": "ဦးရိုဘင်",
        "en": "U ROBIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004003-2"
  },
  {
    "votes": 1451,
    "candidate": {
      "name": {
        "my": "ဦးကီွးထန်း",
        "en": "U KWEE HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004008-1"
  },
  {
    "votes": 1035,
    "candidate": {
      "name": {
        "my": "ဦးလင်းဟာ",
        "en": "U LEIN HAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004008-1"
  },
  {
    "votes": 599,
    "candidate": {
      "name": {
        "my": "ဦးဟုန်းထန်း",
        "en": "U HUNG THANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004008-1"
  },
  {
    "votes": 840,
    "candidate": {
      "name": {
        "my": "ဦးမန်တပ်",
        "en": "U MAN TUP"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004008-1"
  },
  {
    "votes": 1252,
    "candidate": {
      "name": {
        "my": "ဦးဒိုင်ရ်ဆောမနား",
        "en": "U DAIYE SAWMANA"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004008-2"
  },
  {
    "votes": 424,
    "candidate": {
      "name": {
        "my": "ဦးဖေွးမနာ",
        "en": "U PWE MANA"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004008-2"
  },
  {
    "votes": 2486,
    "candidate": {
      "name": {
        "my": "ဦးကီးထန်းလွန်း",
        "en": "U KI THANG LUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004008-2"
  },
  {
    "votes": 2839,
    "candidate": {
      "name": {
        "my": "ဦးဒိုင်ထုန်း",
        "en": "Dai Thung (U"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004007-1"
  },
  {
    "votes": 4595,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းလွင်",
        "en": "U OHN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004007-1"
  },
  {
    "votes": 3298,
    "candidate": {
      "name": {
        "my": "ဦးဟွန်းခဲန်",
        "en": "U HON KAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR004007-1"
  },
  {
    "votes": 1594,
    "candidate": {
      "name": {
        "my": "ဦးနီလင်း",
        "en": "U NI LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004007-2"
  },
  {
    "votes": 3397,
    "candidate": {
      "name": {
        "my": "ဦးဗန်ဘောင်",
        "en": "U VAN BAWM (a) PA BAWM"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004007-2"
  },
  {
    "votes": 4974,
    "candidate": {
      "name": {
        "my": "ဦးဗန်ကျင်း",
        "en": "U VAN KYIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004007-2"
  },
  {
    "votes": 2004,
    "candidate": {
      "name": {
        "my": "ဦးရာစိုး",
        "en": "U YAR SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR004007-2"
  },
  {
    "votes": 3251,
    "candidate": {
      "name": {
        "my": "ဦးနိန်နိုင်း",
        "en": "U NEIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004006-1"
  },
  {
    "votes": 2192,
    "candidate": {
      "name": {
        "my": "ဦးဟာလင်းဟိုး",
        "en": "U HAR LEIN HOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004006-1"
  },
  {
    "votes": 1085,
    "candidate": {
      "name": {
        "my": "ဦးတမ်လောကီး",
        "en": "U TAM LAW KI"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004006-1"
  },
  {
    "votes": 1688,
    "candidate": {
      "name": {
        "my": "ဦးတမ်အွမ်မာန်",
        "en": "U TAM UN MANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004006-1"
  },
  {
    "votes": 5168,
    "candidate": {
      "name": {
        "my": "ဦးဟုန်းငိုင်း",
        "en": "U HUN NGAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004006-2"
  },
  {
    "votes": 1068,
    "candidate": {
      "name": {
        "my": "ဦးချွဲရိုင်",
        "en": "U CHWE YINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR004006-2"
  },
  {
    "votes": 2031,
    "candidate": {
      "name": {
        "my": "ဦးဂျီးဟုမ်း",
        "en": "U GEI HONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004006-2"
  },
  {
    "votes": 3165,
    "candidate": {
      "name": {
        "my": "ဦးဆန်နီအောင်",
        "en": "U SONNY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004009-1"
  },
  {
    "votes": 17347,
    "candidate": {
      "name": {
        "my": "ဦးစိန်သာဦး",
        "en": "U SEIN THAR OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004009-1"
  },
  {
    "votes": 9045,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ငြိမ်း",
        "en": "U KYAW NGEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004009-1"
  },
  {
    "votes": 4852,
    "candidate": {
      "name": {
        "my": "ဦးဂျိုးဇက်အမ်ဘေးမော်",
        "en": "U JOSEPH M.BAY MAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "UDP1",
    "constituency": "MMR004009-1"
  },
  {
    "votes": 4109,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004009-2"
  },
  {
    "votes": 2638,
    "candidate": {
      "name": {
        "my": "ဦးဗန်ထန်း",
        "en": "U VAN TAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004009-2"
  },
  {
    "votes": 4419,
    "candidate": {
      "name": {
        "my": "ဦးလန်ဖား",
        "en": "U LAN PAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR004009-2"
  },
  {
    "votes": 6181,
    "candidate": {
      "name": {
        "my": "ဦးနန်းဆိုက်",
        "en": "U NAN SAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004009-2"
  },
  {
    "votes": 6124,
    "candidate": {
      "name": {
        "my": "ဦးနန်ဇမုန်",
        "en": "U NANZA MONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004004-1"
  },
  {
    "votes": 4762,
    "candidate": {
      "name": {
        "my": "ဦးတန်ဂိုခိုင်",
        "en": "U HTANGO KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004004-1"
  },
  {
    "votes": 5717,
    "candidate": {
      "name": {
        "my": "ဦးကမ်လျန်ထန်",
        "en": "U KHAM LIAN HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004004-1"
  },
  {
    "votes": 751,
    "candidate": {
      "name": {
        "my": "ဦးထွမ်းဂိုးထန်",
        "en": "U HTAN GO HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004004-1"
  },
  {
    "votes": 5747,
    "candidate": {
      "name": {
        "my": "ဦးဟန်ခန့်ခိုင်",
        "en": "U HAN KHANT KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004004-2"
  },
  {
    "votes": 2954,
    "candidate": {
      "name": {
        "my": "ဦးဟူဇာဒေါ",
        "en": "U HAU ZARDAL"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004004-2"
  },
  {
    "votes": 2230,
    "candidate": {
      "name": {
        "my": "ဥိးငင်ထန်",
        "en": "U NGIN HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004004-2"
  },
  {
    "votes": 6342,
    "candidate": {
      "name": {
        "my": "ဦးဇင့်ကျင်ပေါ် (ခ) ဦးဇိုဇမ်း",
        "en": "U ZIN KYIN PAW (A) U ZOZAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004004-2"
  },
  {
    "votes": 2803,
    "candidate": {
      "name": {
        "my": "ဦးကျင့်လျန်းပေါင်",
        "en": "U CIN LYAN PAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004005-1"
  },
  {
    "votes": 1123,
    "candidate": {
      "name": {
        "my": "ဦးဒိုင်လီယန်ဖေါ",
        "en": "U DAI LIAN PAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004005-1"
  },
  {
    "votes": 2367,
    "candidate": {
      "name": {
        "my": "ဦးထန်ခင်းကပ်",
        "en": "U HTAN KIN KAP"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004005-1"
  },
  {
    "votes": 2752,
    "candidate": {
      "name": {
        "my": "ဦးဟောက်ခမ်ခမ်း",
        "en": "U HAU KHIN KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004005-2"
  },
  {
    "votes": 753,
    "candidate": {
      "name": {
        "my": "ဦးထောဒူဖေါ",
        "en": "U TUA DO PAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004005-2"
  },
  {
    "votes": 2374,
    "candidate": {
      "name": {
        "my": "ဦးခိုင်ဒိုခမ်",
        "en": "U KHO DO CAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "CHIN-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004005-2"
  },
  {
    "votes": 17334,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆေွ",
        "en": "U KYAW SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001010-1"
  },
  {
    "votes": 7764,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ခမ်း",
        "en": "U AIK KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001010-1"
  },
  {
    "votes": 8507,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001010-2"
  },
  {
    "votes": 9394,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရှိန်",
        "en": "U TUN SHEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001010-2"
  },
  {
    "votes": 4486,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းထွန်းဦး",
        "en": "U TUN TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001010-2"
  },
  {
    "votes": 2570,
    "candidate": {
      "name": {
        "my": "ဦးဘီထောဇောင်း",
        "en": "U BI HTAW ZAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001005-1"
  },
  {
    "votes": 1583,
    "candidate": {
      "name": {
        "my": "ဦးလန်းဂျောယိန်းဇုံး",
        "en": "U LANGYAW ZEINYONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001005-1"
  },
  {
    "votes": 603,
    "candidate": {
      "name": {
        "my": "ဦးမဒိန်ဇုန်းတိန့်",
        "en": "U MADEIN ZONE TEINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001005-2"
  },
  {
    "votes": 1928,
    "candidate": {
      "name": {
        "my": "ဦးဇခုန်ယိန်းဆောင်",
        "en": "U ZAKHUNG YEIN SAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001005-2"
  },
  {
    "votes": 1500,
    "candidate": {
      "name": {
        "my": "ဦးလာဆီး",
        "en": "U LAR SI"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "",
    "constituency": "MMR001005-2"
  },
  {
    "votes": 4048,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နီ",
        "en": "U Aung Ni"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001009-1"
  },
  {
    "votes": 12637,
    "candidate": {
      "name": {
        "my": "ဦးအင်ဖရောင်းဂမ်",
        "en": "U M.PHRAUNGAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001009-1"
  },
  {
    "votes": 8511,
    "candidate": {
      "name": {
        "my": "ဦးပရန်ဆိုင်းဂူ",
        "en": "U PRENG SENG GU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001009-1"
  },
  {
    "votes": 8521,
    "candidate": {
      "name": {
        "my": "စိုင်းပန်း (ခ) ဦးမောင်ပန်း",
        "en": "Sai Pan (a) U Maung Pan"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001009-2"
  },
  {
    "votes": 19207,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမြင့်ကျော်",
        "en": "U SAI MYINT KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001009-2"
  },
  {
    "votes": 475,
    "candidate": {
      "name": {
        "my": "ဦးအလေးပါး",
        "en": " U AH LAY PAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001003-1"
  },
  {
    "votes": 383,
    "candidate": {
      "name": {
        "my": "ဦးအူရားနော်",
        "en": "U OORANAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001003-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဘရန်ရေှာင်",
        "en": "U BRENG SHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001003-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအင်ထုံးခါးနော်ဆမ်",
        "en": "U EIN HTONKHAR NAWSAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001003-2"
  },
  {
    "votes": 1605,
    "candidate": {
      "name": {
        "my": "ဦးဒိန်ခန်ဒါ၀ိ်",
        "en": "U DEIN KHAN DAWI"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008-1"
  },
  {
    "votes": 1382,
    "candidate": {
      "name": {
        "my": "ဦးလေမယ်ဖူ",
        "en": "U LAY MAE PHU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001008-1"
  },
  {
    "votes": 1687,
    "candidate": {
      "name": {
        "my": "ဦးမီးကော်ဖူ",
        "en": "U MI KHAW PHOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008-2"
  },
  {
    "votes": 1792,
    "candidate": {
      "name": {
        "my": "ဦးခင်လေးဖုန်",
        "en": "U KHIN LAY PHONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001008-2"
  },
  {
    "votes": 1363,
    "candidate": {
      "name": {
        "my": "ဦးရားဝမ်ဂျုံ",
        "en": "U RAWANG JYUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001006-1"
  },
  {
    "votes": 2369,
    "candidate": {
      "name": {
        "my": "ဦးယောစီ",
        "en": "U YAWSI"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001006-1"
  },
  {
    "votes": 846,
    "candidate": {
      "name": {
        "my": "ဥိးငွားဒီးယော",
        "en": "U NGWA DIYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001006-2"
  },
  {
    "votes": 1293,
    "candidate": {
      "name": {
        "my": "ဦးထိန်မောင်တူး",
        "en": "U HTEIN MAUNG TU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001006-2"
  },
  {
    "votes": 6429,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကြိုင်",
        "en": "U TUN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001003-1"
  },
  {
    "votes": 5763,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်ဌေး",
        "en": "Y THANT ZIN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001003-1"
  },
  {
    "votes": 1923,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ရေွှ",
        "en": "U AIK SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001003-1"
  },
  {
    "votes": 862,
    "candidate": {
      "name": {
        "my": "ဦးကွမ်ဆန်း",
        "en": "U KWAN SANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001003-2"
  },
  {
    "votes": 719,
    "candidate": {
      "name": {
        "my": "ဦးယောဟန်",
        "en": "U YAW HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001003-2"
  },
  {
    "votes": 2723,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမောင်ရေွှ",
        "en": "U SAI MAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001003-2"
  },
  {
    "votes": 6009,
    "candidate": {
      "name": {
        "my": "ဦးချစ်အောင်",
        "en": "U Chit Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001008-1"
  },
  {
    "votes": 10823,
    "candidate": {
      "name": {
        "my": "ဦးစိုးနွယ်",
        "en": "U SOE NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008-1"
  },
  {
    "votes": 2817,
    "candidate": {
      "name": {
        "my": "ဦးလှငေွ",
        "en": "U HLA NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001008-1"
  },
  {
    "votes": 5891,
    "candidate": {
      "name": {
        "my": "ဦးဘဂျမ်း",
        "en": "U Ba Gang"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001008-2"
  },
  {
    "votes": 19246,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008-2"
  },
  {
    "votes": 7703,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": "U Tin Tun"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001007-1"
  },
  {
    "votes": 18824,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်အောင်",
        "en": "U SAI NYUNT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001007-1"
  },
  {
    "votes": 6976,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": "U AUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001007-1"
  },
  {
    "votes": 8943,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်ပြုံးရီ",
        "en": "Daw Khin Pyone Ye"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001007-2"
  },
  {
    "votes": 15354,
    "candidate": {
      "name": {
        "my": "ဦးညီလေး",
        "en": "U NYI LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001007-2"
  },
  {
    "votes": 6609,
    "candidate": {
      "name": {
        "my": "ဦးသက်ထွန်း",
        "en": "U THET TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001007-2"
  },
  {
    "votes": 3808,
    "candidate": {
      "name": {
        "my": "ဦးမရန်အောင်",
        "en": "U MARANG AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001012-1"
  },
  {
    "votes": 4827,
    "candidate": {
      "name": {
        "my": "ဦးဆွက်နောင်",
        "en": "U SWET NAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001012-1"
  },
  {
    "votes": 2932,
    "candidate": {
      "name": {
        "my": "ဦးဒေါ့ဆန်အောင်",
        "en": "U DAWSON AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001012-1"
  },
  {
    "votes": 7160,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဝိုင်း",
        "en": "DAW WAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001012-2"
  },
  {
    "votes": 9513,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးအောင်",
        "en": "U MYO AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001012-2"
  },
  {
    "votes": 17161,
    "candidate": {
      "name": {
        "my": "ဦးတူးဂျာ",
        "en": "U TU JA"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001001-1"
  },
  {
    "votes": 17657,
    "candidate": {
      "name": {
        "my": "ဦးကမန်းဒူနော်",
        "en": "U KAMAN DU NAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001001-1"
  },
  {
    "votes": 9201,
    "candidate": {
      "name": {
        "my": "ဦးငေွသိန်း",
        "en": "U Ngwe Thein"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001001-2"
  },
  {
    "votes": 26073,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထွန်း",
        "en": "U KHIN MAUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001001-2"
  },
  {
    "votes": 11913,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင််",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001001-2"
  },
  {
    "votes": 1494,
    "candidate": {
      "name": {
        "my": "ဦးချန်တန်ခင်",
        "en": "U CHAN TAN KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001017-1"
  },
  {
    "votes": 407,
    "candidate": {
      "name": {
        "my": "ဦးဝန်လောန်",
        "en": "U WON LAUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001017-1"
  },
  {
    "votes": 1563,
    "candidate": {
      "name": {
        "my": "ဦးမဘုဒဂုဏ်",
        "en": "U MABU DAGON"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001017-2"
  },
  {
    "votes": 341,
    "candidate": {
      "name": {
        "my": "ဦးချန်းခင်",
        "en": "U CHAN KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001017-2"
  },
  {
    "votes": 4170,
    "candidate": {
      "name": {
        "my": "ဦးစောဝင်းခွန်",
        "en": "U SAW WIN KHUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001014-1"
  },
  {
    "votes": 7042,
    "candidate": {
      "name": {
        "my": "ဦးအားရီ",
        "en": "U AR YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001014-1"
  },
  {
    "votes": 786,
    "candidate": {
      "name": {
        "my": "ဦးစစ်နွဲ့နိုခမ်း",
        "en": "U SIT NWE NOKHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001014-1"
  },
  {
    "votes": 4821,
    "candidate": {
      "name": {
        "my": "ဦးဖုန်ရိမင််",
        "en": "U PHUN YI MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001014-2"
  },
  {
    "votes": 7609,
    "candidate": {
      "name": {
        "my": "ဦးလီပေါ်ရံ",
        "en": "U LI PAW YAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001014-2"
  },
  {
    "votes": 9129,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": "U AUNG NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001011-1"
  },
  {
    "votes": 8611,
    "candidate": {
      "name": {
        "my": "ဦးစံညွန့်",
        "en": "U SAN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001011-1"
  },
  {
    "votes": 9655,
    "candidate": {
      "name": {
        "my": "ဦးဖိုးပါကြွယ်",
        "en": "U PHO PAR KYWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001011-2"
  },
  {
    "votes": 8284,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001011-2"
  },
  {
    "votes": 1220,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဘောက်ဂျာ",
        "en": "DAW BAWK JA"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001015-1"
  },
  {
    "votes": 875,
    "candidate": {
      "name": {
        "my": "ဦးကဒန်မုန်ဒွယ်(ခ)ဦးမွန်တွဲ",
        "en": "U KADON MHONDWE (a) U MON TWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001015-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမုန်ပေါန်နော်",
        "en": "U MONPAUN NAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001015-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးလအောင်််",
        "en": "U LA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001015-2"
  },
  {
    "votes": 4433,
    "candidate": {
      "name": {
        "my": "ဦးလဂျွန်ငန်ဆိုင်း",
        "en": "U LAJUNG NGAN SENG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001004-1"
  },
  {
    "votes": 2965,
    "candidate": {
      "name": {
        "my": "ဦးအင်ဝမ်ခန်းဇော်လောန်",
        "en": "U N.WONKHAN ZAW LAWNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001004-1"
  },
  {
    "votes": 2678,
    "candidate": {
      "name": {
        "my": "ဦးကွမ်ဆောင်းဆမ်အောန်််",
        "en": "U KWAN SAUNG SAMAWNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001004-2"
  },
  {
    "votes": 2489,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သန်း",
        "en": "U KHIN MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001004-2"
  },
  {
    "votes": 278,
    "candidate": {
      "name": {
        "my": "ဒေါ်လွမ်းလမ်း",
        "en": "DAW LWAN LWAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001006-1"
  },
  {
    "votes": 522,
    "candidate": {
      "name": {
        "my": "ဦးဇောင်းခေါင်",
        "en": "U ZAWNG KHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001006-1"
  },
  {
    "votes": 34,
    "candidate": {
      "name": {
        "my": "ဦးဇေလွမ်း",
        "en": "U ZAY LWAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001006-1"
  },
  {
    "votes": 449,
    "candidate": {
      "name": {
        "my": "ဦးဘောမ်လန်း",
        "en": "U BAWM LANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001006-2"
  },
  {
    "votes": 793,
    "candidate": {
      "name": {
        "my": "ဦးယောနာ",
        "en": "U YAW NAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001006-2"
  },
  {
    "votes": 5662,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ထေွး",
        "en": " U Chit Htwe"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001002-1"
  },
  {
    "votes": 4795,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းယီကျန််",
        "en": "U SAI YI KYAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001002-1"
  },
  {
    "votes": 6290,
    "candidate": {
      "name": {
        "my": "ဦးမြအောင်",
        "en": "U MYA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001002-1"
  },
  {
    "votes": 3723,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းလိှုင်",
        "en": "U San Hlaing"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001002-2"
  },
  {
    "votes": 7813,
    "candidate": {
      "name": {
        "my": "ဦးထောလွမ်း",
        "en": "U HTAW LWAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001002-2"
  },
  {
    "votes": 3625,
    "candidate": {
      "name": {
        "my": "ဦးလဆန်အောင်ဝါ",
        "en": "U LA SAWNG AUNG WA"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001002-2"
  },
  {
    "votes": 2144,
    "candidate": {
      "name": {
        "my": "ဦးဝေါ်တား",
        "en": "U WAW HTAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001002-2"
  },
  {
    "ethnic_seat": true,
    "votes": 104297,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးပါ (ခ) ဦးခင်မောင်ဆေွ",
        "en": "U PA (a) U KHIN MAUNG SWE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 98457,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လှ",
        "en": "U KHIN MAUNG HLA"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 19752,
    "acclaim": "N",
    "ethnicity": "Lisu",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးအားဆီ",
        "en": "U AR SI"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 14204,
    "acclaim": "N",
    "ethnicity": "Lisu",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးခေါ်တား",
        "en": "U KHAW TAR"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 15097,
    "acclaim": "N",
    "ethnicity": "Rawang",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးဂွမ်ရိန်ဒီး",
        "en": "U GWAN REINDI"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 6212,
    "acclaim": "N",
    "ethnicity": "Rawang",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးထမိုင်ရုံ",
        "en": "U HTA MAING YON"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 52238,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်ပြုံးရည်",
        "en": "Daw Khin Pyone Ye"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 18953,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးချစ်ရေွှ",
        "en": "U CHIT SHWE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 16965,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "KACHIN-RGH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001",
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအေးကျော်",
        "en": "U SAI AYE KYAW"
      }
    }
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး(ခ)ဦးဘူးရယ််",
        "en": "U KHIN MAUNG OO(a) U BU YAE"
      }
    },
    "acclaim": "Y",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002005-1"
  },
  {
    "votes": 2205,
    "candidate": {
      "name": {
        "my": "ဦးချစ်လှ",
        "en": "U CHIT HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002005-2"
  },
  {
    "votes": 188,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဝင်း",
        "en": "U THAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002005-2"
  },
  {
    "votes": 12008,
    "candidate": {
      "name": {
        "my": "ဦးတောရယ်",
        "en": "U TAW YAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002002-1"
  },
  {
    "votes": 1169,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002002-1"
  },
  {
    "votes": 1704,
    "candidate": {
      "name": {
        "my": "ဦးလောလေထူး",
        "en": "U LAW LAY HTOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR002002-1"
  },
  {
    "votes": 15309,
    "candidate": {
      "name": {
        "my": "ဦးပိုးရယ်(ခ)ဦးပိုးရယ်ရန်အောင်",
        "en": "U PO YAE (a) U PO YAE YAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002002-2"
  },
  {
    "votes": 1007,
    "candidate": {
      "name": {
        "my": "ဦးအန်းဂျေလို",
        "en": "U ANGELO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002002-2"
  },
  {
    "votes": 3073,
    "candidate": {
      "name": {
        "my": "ဦးနန်းရီ",
        "en": "U NAN YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR002002-2"
  },
  {
    "votes": 3396,
    "candidate": {
      "name": {
        "my": "ဦးသန်းကျော်စိုး",
        "en": "U THAN KYAW SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002006-1"
  },
  {
    "votes": 204,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002006-1"
  },
  {
    "votes": 2978,
    "candidate": {
      "name": {
        "my": "ဦးစောကိုဒီ",
        "en": "U SAW KODEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002006-2"
  },
  {
    "votes": 96,
    "candidate": {
      "name": {
        "my": "ဦးစောဘစိန်",
        "en": "U SAW BA SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002006-2"
  },
  {
    "votes": 3340,
    "candidate": {
      "name": {
        "my": "ဦးစောဟူးဟူး",
        "en": "U SAW HU HU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002003-1"
  },
  {
    "votes": 1657,
    "candidate": {
      "name": {
        "my": "ဦးသိုးရယ်",
        "en": "U THOE YAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002003-1"
  },
  {
    "votes": 3860,
    "candidate": {
      "name": {
        "my": "ဦးရီးချက်",
        "en": "U RICHARD"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002003-2"
  },
  {
    "votes": 1311,
    "candidate": {
      "name": {
        "my": "ဦးပီတာဂျော့ဆို(ခ)ပေတလူ",
        "en": "U PETER JOSO (a) PAE TA LU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002003-2"
  },
  {
    "votes": 20422,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆေွ",
        "en": "U KYAW SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002001-1"
  },
  {
    "votes": 4692,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ထွန်း(ခ)လူရယ်",
        "en": "U CHIT TUN (a) U LU YAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002001-1"
  },
  {
    "votes": 4399,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR002001-1"
  },
  {
    "votes": 19822,
    "candidate": {
      "name": {
        "my": "ဦးကိုးရယ်",
        "en": "O KOE YAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002001-2"
  },
  {
    "votes": 8581,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညိမ်း",
        "en": "U KYAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002001-2"
  },
  {
    "votes": 1851,
    "candidate": {
      "name": {
        "my": "ဦးစောလူးလူ",
        "en": "?U SAW LUU LU"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR002001-2"
  },
  {
    "votes": 1465,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002007-1"
  },
  {
    "votes": 73,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002007-1"
  },
  {
    "votes": 1149,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဦး",
        "en": "U AUNG NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002007-2"
  },
  {
    "votes": 172,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရင်",
        "en": "U TUN YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002007-2"
  },
  {
    "votes": 133,
    "candidate": {
      "name": {
        "my": "ဦးရဲဝင်း",
        "en": "U YE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002004-1"
  },
  {
    "votes": 10,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထိန်လင်းဦး",
        "en": "U SAI HTEIN LIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002004-1"
  },
  {
    "votes": 1388,
    "candidate": {
      "name": {
        "my": "ဦးအေးမောင်",
        "en": "U AYE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002004-2"
  },
  {
    "votes": 199,
    "candidate": {
      "name": {
        "my": "ဦးဖြေရယ်စူး",
        "en": "U PHYAY YAE SOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002004-2"
  },
  {
    "ethnic_seat": true,
    "votes": 0,
    "acclaim": "Y",
    "ethnicity": "Burman",
    "parliament_code": "KAYAH-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002",
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဦး",
        "en": "U SEIN OO"
      }
    }
  },
  {
    "votes": 14669,
    "candidate": {
      "name": {
        "my": "စောမူးအောင်လင်း",
        "en": "Saw Mu Aung Lin"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003002-1"
  },
  {
    "votes": 8766,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်ဝင်း",
        "en": "U TIN AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003002-1"
  },
  {
    "votes": 11943,
    "candidate": {
      "name": {
        "my": "ဦးစောသာထူးကျော်",
        "en": "U SAW THAR HTOO KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "KSDPP",
    "constituency": "MMR003002-1"
  },
  {
    "votes": 13529,
    "candidate": {
      "name": {
        "my": "စောရှားထွန့်ဖောင်",
        "en": "Saw Shah Htunt Paung"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003002-2"
  },
  {
    "votes": 5168,
    "candidate": {
      "name": {
        "my": "ဦးစောခင်စိုး",
        "en": "U SAW KHIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003002-2"
  },
  {
    "votes": 2905,
    "candidate": {
      "name": {
        "my": "ဦးခွန်မန်းတင်",
        "en": "U KHUN MAHN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003002-2"
  },
  {
    "votes": 26602,
    "candidate": {
      "name": {
        "my": "စောကြည်လင်း",
        "en": "Saw Kyi Lin"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003001-1"
  },
  {
    "votes": 14318,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်မြင့်",
        "en": "U TIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003001-1"
  },
  {
    "votes": 19386,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ရှိန်",
        "en": "U MAUNG SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003001-1"
  },
  {
    "votes": 22628,
    "candidate": {
      "name": {
        "my": "စောခင်မောင်မြင့်",
        "en": "Saw Khin Maung Myint (U"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003001-2"
  },
  {
    "votes": 22197,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ဝင်းနောင်",
        "en": "U KUN WIN NAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003001-2"
  },
  {
    "votes": 12437,
    "candidate": {
      "name": {
        "my": "ဦးစောစံဝင်း",
        "en": "U SAW SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003001-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "စောဝင်းထိန်",
        "en": "SAW WIN HTEIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003003-1"
  },
  {
    "votes": 2291,
    "candidate": {
      "name": {
        "my": "ဦးကြည်စိုးထွန်း",
        "en": "U KYI SOE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003003-2"
  },
  {
    "votes": 2409,
    "candidate": {
      "name": {
        "my": "ပဒိုအောင်ဆန်း",
        "en": "PADOH AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003003-2"
  },
  {
    "votes": 8261,
    "candidate": {
      "name": {
        "my": "စဘီကျင်ဦး",
        "en": "Sabi Kyin Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003006-1"
  },
  {
    "votes": 11765,
    "candidate": {
      "name": {
        "my": "ဦးမန်းလှမြိုင်",
        "en": "U MAHN HLA MYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003006-1"
  },
  {
    "votes": 8207,
    "candidate": {
      "name": {
        "my": "ဦးအေးစိုး",
        "en": "U AYE SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003006-1"
  },
  {
    "votes": 5906,
    "candidate": {
      "name": {
        "my": "နော်ယုဇနဝါး",
        "en": "NAW YUZANA WAH"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003006-2"
  },
  {
    "votes": 10368,
    "candidate": {
      "name": {
        "my": "မင်းစိုသိန်း",
        "en": "Min Soe Thein"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR003006-2"
  },
  {
    "votes": 7907,
    "candidate": {
      "name": {
        "my": "ဦးမင်းချစ်သိုင်း",
        "en": "U MIN CHIT THAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003006-2"
  },
  {
    "votes": 2504,
    "candidate": {
      "name": {
        "my": "စောထယ်ရ",
        "en": "Saw Terry"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003007-1"
  },
  {
    "votes": 7784,
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်ကျော်မင်း",
        "en": "U SAW AUNG KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003007-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစောဆာလေလ",
        "en": "U SAW SARLAYLA"
      }
    },
    "acclaim": "Y",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003007-2"
  },
  {
    "votes": 6049,
    "candidate": {
      "name": {
        "my": "ဦးချစ်လိှုင်",
        "en": "U CHIT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "KSDPP",
    "constituency": "MMR003005-1"
  },
  {
    "votes": 2716,
    "candidate": {
      "name": {
        "my": "ဦးအောင်တိုး",
        "en": "U AUNG TOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003005-1"
  },
  {
    "votes": 5679,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းသိန်း(ခ) ဦးကျော်",
        "en": "U OHN THEIN (A) U KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003005-1"
  },
  {
    "votes": 5018,
    "candidate": {
      "name": {
        "my": "မန်းအောင်ပြည်စိုး",
        "en": "Mahn Aung Pyi Soe"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003005-2"
  },
  {
    "votes": 7789,
    "candidate": {
      "name": {
        "my": "ဦးသံဒိုင်",
        "en": "U THAN DAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003005-2"
  },
  {
    "votes": 6175,
    "candidate": {
      "name": {
        "my": "ဦးစောဝင်းမြင့်",
        "en": "U SAW WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR003004-1"
  },
  {
    "votes": 4086,
    "candidate": {
      "name": {
        "my": "ဦးစောအဲမရီ",
        "en": "U SAW M. MARI"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003004-1"
  },
  {
    "votes": 4467,
    "candidate": {
      "name": {
        "my": "ဦးစောခရစ်စတိုဖာ",
        "en": "U SAW CHRISTOPHER"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR003004-2"
  },
  {
    "votes": 2299,
    "candidate": {
      "name": {
        "my": "ဦးစောလှမောင်",
        "en": "??U SAW HLA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003004-2"
  },
  {
    "votes": 2718,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမောင်",
        "en": "U SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR003004-2"
  },
  {
    "ethnic_seat": true,
    "votes": 49439,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ဦးခင်ကြူး",
        "en": "U KHIN KYU"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 17360,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ဒေါ်ကြည်ဝင်း",
        "en": "DAW KYI WIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 17898,
    "acclaim": "N",
    "ethnicity": "Mon",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "နိုင်ချစ်ဦး",
        "en": "Naing Chit Oo"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 11741,
    "acclaim": "N",
    "ethnicity": "Mon",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ဦးနိုင်မောင်မောင်လွင်",
        "en": "U NAING MAUNG MAUNG LWIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 4692,
    "acclaim": "N",
    "ethnicity": "Mon",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြွယ်",
        "en": "U AUNG KWAY"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 10997,
    "acclaim": "N",
    "ethnicity": "PaO",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ဦးခွန်သန်းမြင့်",
        "en": "U KHUN THAN MYINT"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 4888,
    "acclaim": "N",
    "ethnicity": "PaO",
    "parliament_code": "KAYIN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003",
    "candidate": {
      "name": {
        "my": "ခွန်အောင်သိန်းဝင်း",
        "en": "KHUN AUNG THEIN WIN"
      }
    }
  },
  {
    "votes": 10948,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဝင်း",
        "en": "U HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR009016-1"
  },
  {
    "votes": 41482,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဆေွ",
        "en": "U KHIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009016-1"
  },
  {
    "votes": 14423,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန်းစန်းအေး",
        "en": "DAW SAN SAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009016-1"
  },
  {
    "votes": 12494,
    "candidate": {
      "name": {
        "my": "ဦးကြင်သန်း",
        "en": "U KYIN THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR009016-2"
  },
  {
    "votes": 39520,
    "candidate": {
      "name": {
        "my": "ဦးစန်းလွင်",
        "en": "U SAN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009016-2"
  },
  {
    "votes": 12132,
    "candidate": {
      "name": {
        "my": "ဦးအေးရေွှ",
        "en": "U AYE SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009016-2"
  },
  {
    "votes": 13111,
    "candidate": {
      "name": {
        "my": "ဦးသံစိုး",
        "en": "U Thann Soe"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009003-1"
  },
  {
    "votes": 28147,
    "candidate": {
      "name": {
        "my": "ဦးနေရှင်း",
        "en": "U NAY SHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009003-1"
  },
  {
    "votes": 7806,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009003-1"
  },
  {
    "votes": 10701,
    "candidate": {
      "name": {
        "my": "ဦးအေးမင်းထွန်း",
        "en": "U Aye Min Htun"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009003-2"
  },
  {
    "votes": 30943,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်ဦး",
        "en": "U WIN NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009003-2"
  },
  {
    "votes": 11628,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထွန်း",
        "en": "U KHIN MAUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009003-2"
  },
  {
    "votes": 10924,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မင်းစိန်",
        "en": "U Zaw Min Sein"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009023-1"
  },
  {
    "votes": 16106,
    "candidate": {
      "name": {
        "my": " ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009023-1"
  },
  {
    "votes": 9583,
    "candidate": {
      "name": {
        "my": "ဦးညိုထွား",
        "en": "U NYO TWAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009023-1"
  },
  {
    "votes": 7521,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ထွန်း",
        "en": "U SEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009023-1"
  },
  {
    "votes": 16874,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်အောင်",
        "en": "U WIN MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009023-2"
  },
  {
    "votes": 4459,
    "candidate": {
      "name": {
        "my": "ဦးဖေညို",
        "en": "U PAE NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009023-2"
  },
  {
    "votes": 1806,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြည်သန်း",
        "en": "U AUNG KYI THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009023-2"
  },
  {
    "votes": 7691,
    "candidate": {
      "name": {
        "my": "ဦးစံငေွ",
        "en": "U San Ngwe"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009015-1"
  },
  {
    "votes": 11819,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်",
        "en": "U WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009015-1"
  },
  {
    "votes": 3855,
    "candidate": {
      "name": {
        "my": "ဦးတင်ကြိုင်",
        "en": "U TIN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009015-1"
  },
  {
    "votes": 6830,
    "candidate": {
      "name": {
        "my": "ဦးမြကြိုင်",
        "en": "U Mya Kyaing"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009015-2"
  },
  {
    "votes": 16090,
    "candidate": {
      "name": {
        "my": "ဦးတင်မျိုးလိှုင်",
        "en": "U TIN MYO HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009015-2"
  },
  {
    "votes": 2235,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းရေွှ",
        "en": "U THEIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009015-2"
  },
  {
    "votes": 58766,
    "candidate": {
      "name": {
        "my": "ဦးရဲမြင့်",
        "en": "U YE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009001-1"
  },
  {
    "votes": 20916,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009001-1"
  },
  {
    "votes": 51294,
    "candidate": {
      "name": {
        "my": "ဦးဟန်မောင်",
        "en": "U HAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009001-2"
  },
  {
    "votes": 22359,
    "candidate": {
      "name": {
        "my": "ဦးထေွးလှအောင်",
        "en": "U HTWE HLA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009001-2"
  },
  {
    "votes": 36792,
    "candidate": {
      "name": {
        "my": "ဦးဘုန်းမော်ရေွှ",
        "en": "U BOM MAW SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009007-1"
  },
  {
    "votes": 11423,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းဝင်း",
        "en": "U THAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009007-1"
  },
  {
    "votes": 7157,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ဝေ",
        "en": "U NYUNT WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR009007-2"
  },
  {
    "votes": 27577,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်တင်",
        "en": "U MAUNG MAUNG TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009007-2"
  },
  {
    "votes": 12523,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": "U MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009007-2"
  },
  {
    "votes": 14524,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဇော်",
        "en": "U THEIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009014-1"
  },
  {
    "votes": 6034,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သောင်း",
        "en": "U KYAW THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009014-1"
  },
  {
    "votes": 13514,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009014-2"
  },
  {
    "votes": 6292,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညွန့်",
        "en": "U KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009014-2"
  },
  {
    "votes": 20287,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009013-1"
  },
  {
    "votes": 18028,
    "candidate": {
      "name": {
        "my": "ဦးစန်းအောင်",
        "en": "U SAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009013-1"
  },
  {
    "votes": 23988,
    "candidate": {
      "name": {
        "my": "ဦးလှစိုး",
        "en": "U HLA SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009013-2"
  },
  {
    "votes": 12244,
    "candidate": {
      "name": {
        "my": "ဦးသန်းလှုိင်",
        "en": "U THAN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009013-2"
  },
  {
    "votes": 2473,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR009013-2"
  },
  {
    "votes": 58807,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရန်",
        "en": "U TIN YAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009020-1"
  },
  {
    "votes": 5905,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009020-1"
  },
  {
    "votes": 48203,
    "candidate": {
      "name": {
        "my": " ဦးစောဝင်း",
        "en": "U SAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009020-2"
  },
  {
    "votes": 11322,
    "candidate": {
      "name": {
        "my": " ဦးတင်ဝမ်း",
        "en": "U TIN WAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009020-2"
  },
  {
    "votes": 25403,
    "candidate": {
      "name": {
        "my": "ဦးချစ်သိန်း",
        "en": "U CHIT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009005-1"
  },
  {
    "votes": 11638,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009005-1"
  },
  {
    "votes": 27383,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009005-2"
  },
  {
    "votes": 12461,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်သောင်း(ခ)ဦးစိန်ဟန်",
        "en": "U NYUNT THAUNG (A) U SEIN HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009005-2"
  },
  {
    "votes": 34022,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်မင်း",
        "en": "U AUNG KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009006-1"
  },
  {
    "votes": 17374,
    "candidate": {
      "name": {
        "my": "ဦးလေးဆင့်",
        "en": "U LAY SINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009006-1"
  },
  {
    "votes": 35742,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009006-2"
  },
  {
    "votes": 15781,
    "candidate": {
      "name": {
        "my": "ဦးတင်ညွန့်",
        "en": "U TIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009006-2"
  },
  {
    "votes": 9339,
    "candidate": {
      "name": {
        "my": "ဦးလှပြုံး",
        "en": "U HLA PYONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009009-1"
  },
  {
    "votes": 5057,
    "candidate": {
      "name": {
        "my": "ဦးမြမောင်",
        "en": "U MYA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009009-1"
  },
  {
    "votes": 9643,
    "candidate": {
      "name": {
        "my": "ဦးတင်ညွန့်",
        "en": "U TIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009009-2"
  },
  {
    "votes": 6186,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သိန်း",
        "en": "U MAUNG MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009009-2"
  },
  {
    "votes": 56225,
    "candidate": {
      "name": {
        "my": " ဦးသိန်းထွန်း",
        "en": "U THAIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009018-1"
  },
  {
    "votes": 18717,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သောင်း",
        "en": "U AUNG THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009018-1"
  },
  {
    "votes": 50471,
    "candidate": {
      "name": {
        "my": " ဦးဇော်မိုးသိန်း",
        "en": "U ZAW MOE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009018-2"
  },
  {
    "votes": 23587,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009018-2"
  },
  {
    "votes": 8464,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်လင်း",
        "en": "U NAING LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009021-1"
  },
  {
    "votes": 27342,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုးဝင်း",
        "en": "U MYINT SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009021-1"
  },
  {
    "votes": 8047,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ထွန်း",
        "en": "U CHIT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009021-1"
  },
  {
    "votes": 7079,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ရေွှ",
        "en": "U MYINT SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009021-2"
  },
  {
    "votes": 28422,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009021-2"
  },
  {
    "votes": 9379,
    "candidate": {
      "name": {
        "my": " ဦးတင်ညွန့်",
        "en": "U TIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009021-2"
  },
  {
    "votes": 35193,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မိုးမြင့်",
        "en": "U ZAW MOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009008-1"
  },
  {
    "votes": 11685,
    "candidate": {
      "name": {
        "my": "ဦးလှကို",
        "en": "U HLA KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009008-1"
  },
  {
    "votes": 13678,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THAIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009008-2"
  },
  {
    "votes": 29045,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": "U AUNG NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009008-2"
  },
  {
    "votes": 36535,
    "candidate": {
      "name": {
        "my": "ဦးသစ်လွင်ခိုင်",
        "en": "U THINT LWIN KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009010-1"
  },
  {
    "votes": 24852,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရီ",
        "en": "U TUN YE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009010-1"
  },
  {
    "votes": 35022,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြိုင်",
        "en": "U WIN MYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009010-2"
  },
  {
    "votes": 22873,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းခင်",
        "en": "U AUNG THAN KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009010-2"
  },
  {
    "votes": 5024,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မင်းထွန်း",
        "en": "U AUNG MIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009025-1"
  },
  {
    "votes": 10539,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်",
        "en": "U HTAY LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009025-1"
  },
  {
    "votes": 5662,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ရီ",
        "en": "U MAUNG YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009025-1"
  },
  {
    "votes": 9809,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်",
        "en": "U KHIN MAUNG LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009025-2"
  },
  {
    "votes": 11307,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဖေ",
        "en": "U WIN PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009025-2"
  },
  {
    "votes": 11352,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009022-1"
  },
  {
    "votes": 11612,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုး",
        "en": "U MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009022-1"
  },
  {
    "votes": 5273,
    "candidate": {
      "name": {
        "my": "ဦးကံကောင်း",
        "en": "U KAUNG KONG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR009022-1"
  },
  {
    "votes": 12691,
    "candidate": {
      "name": {
        "my": "ဦးဘအောင်",
        "en": "U BA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009022-2"
  },
  {
    "votes": 13843,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်",
        "en": "U MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009022-2"
  },
  {
    "votes": 7367,
    "candidate": {
      "name": {
        "my": " ဦးလင်းအောင်",
        "en": "U LIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009011-1"
  },
  {
    "votes": 4814,
    "candidate": {
      "name": {
        "my": "ဦးဘသန်း",
        "en": "U BA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009011-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လိှုင်",
        "en": "U SEIN HLAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009011-2"
  },
  {
    "votes": 25452,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်စိုး",
        "en": "U MAUNG MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009017-1"
  },
  {
    "votes": 10055,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလိှုင်",
        "en": "U THAN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009017-1"
  },
  {
    "votes": 25730,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ငေွ",
        "en": "U MYINT NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009017-2"
  },
  {
    "votes": 8978,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဦး",
        "en": "U THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009017-2"
  },
  {
    "votes": 17697,
    "candidate": {
      "name": {
        "my": "ဦးငြိမ်းချမ်းဇော်",
        "en": "U Nyein Chan Zaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009004-1"
  },
  {
    "votes": 44500,
    "candidate": {
      "name": {
        "my": "ဦးကြည်သိန်း",
        "en": "U KYI THAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009004-1"
  },
  {
    "votes": 9918,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်ဆေွ",
        "en": "DR. KYAW SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009004-1"
  },
  {
    "votes": 37051,
    "candidate": {
      "name": {
        "my": "ဦးစောဝင်းမောင်",
        "en": "U SAW WIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009004-2"
  },
  {
    "votes": 37051,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်သိန်း",
        "en": "U NYAN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009004-2"
  },
  {
    "votes": 22977,
    "candidate": {
      "name": {
        "my": "ဦးကြည်မင်း",
        "en": "U KYI MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009012-1"
  },
  {
    "votes": 10181,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွဋ်",
        "en": "U TIN HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009012-1"
  },
  {
    "votes": 28246,
    "candidate": {
      "name": {
        "my": "ဒေါ်ယဉ်ယဉ်လှ",
        "en": "DAW YIN YIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009012-2"
  },
  {
    "votes": 6462,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009012-2"
  },
  {
    "votes": 6020,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထေွး",
        "en": "U KHIN MAUNG HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009024-1"
  },
  {
    "votes": 6498,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဆေွ",
        "en": "U THEIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009024-1"
  },
  {
    "votes": 2987,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဖေ(ခ)ဦးပိုက်",
        "en": "U WIN PAE (A) U PIGHT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009024-1"
  },
  {
    "votes": 3498,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိန်",
        "en": "U KYAW SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009024-2"
  },
  {
    "votes": 7749,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းအောင်",
        "en": "U WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009024-2"
  },
  {
    "votes": 21001,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဝင်း",
        "en": "U AUNG NAING WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009002-1"
  },
  {
    "votes": 12538,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009002-1"
  },
  {
    "votes": 8057,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ညီညီ",
        "en": "U Chit Nyi Nyi"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009002-2"
  },
  {
    "votes": 24570,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းတင့်",
        "en": "U WIN TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009002-2"
  },
  {
    "votes": 5136,
    "candidate": {
      "name": {
        "my": "ဦးပု(ခ)ဦးမောင်ပု",
        "en": "U PU (A) U NYI PU"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009002-2"
  },
  {
    "votes": 50144,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်နိုင်",
        "en": "U MYINT NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009019-1"
  },
  {
    "votes": 13830,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လင်း",
        "en": "U AUNG LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009019-1"
  },
  {
    "votes": 52696,
    "candidate": {
      "name": {
        "my": " ဦးတင်မောင်ကျော်",
        "en": "U TIN MAUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009019-2"
  },
  {
    "votes": 12667,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိန်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009019-2"
  },
  {
    "ethnic_seat": true,
    "votes": 26855,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009",
    "candidate": {
      "name": {
        "my": "ဦးထွန်းခင်",
        "en": "U TUN KHIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 30961,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "MAGWAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009",
    "candidate": {
      "name": {
        "my": "ဆလိုင်းလှထွန်း",
        "en": "SAHLAING HLA TUN"
      }
    }
  },
  {
    "votes": 23700,
    "candidate": {
      "name": {
        "my": "ဦးမိုးနိုင်ကျော်",
        "en": "U MOE NAING KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010006-1"
  },
  {
    "votes": 14510,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010006-1"
  },
  {
    "votes": 12114,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသန်းမြင့်",
        "en": "DR. THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010006-1"
  },
  {
    "votes": 32786,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သိန်း",
        "en": "U AUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010006-2"
  },
  {
    "votes": 16371,
    "candidate": {
      "name": {
        "my": "ဦးဖေကျော်",
        "en": "U PAE KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010006-2"
  },
  {
    "votes": 9988,
    "candidate": {
      "name": {
        "my": "ဦးသန်းလှုိင်",
        "en": "U THAN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010001-1"
  },
  {
    "votes": 26866,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းလိှုင်",
        "en": "DR. WIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010001-1"
  },
  {
    "votes": 10338,
    "candidate": {
      "name": {
        "my": "ဦးခင်ကြေွလှ",
        "en": "U KHIN KYEW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010001-1"
  },
  {
    "votes": 9406,
    "candidate": {
      "name": {
        "my": "ဦးဖြိုးဝေသက်",
        "en": "U Pho Way Thet"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010001-2"
  },
  {
    "votes": 22280,
    "candidate": {
      "name": {
        "my": "ဦးဖုန်းဇော်ဟန်",
        "en": "U PONE ZAW HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010001-2"
  },
  {
    "votes": 10386,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010001-2"
  },
  {
    "votes": 11432,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ရေွှ",
        "en": "U AUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010002-1"
  },
  {
    "votes": 18725,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်လှ",
        "en": "DR. KYAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010002-1"
  },
  {
    "votes": 6076,
    "candidate": {
      "name": {
        "my": "ဦးမိုးကြည်",
        "en": "U MOE KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010002-1"
  },
  {
    "votes": 14397,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်တင်မာ",
        "en": "DAW TIN TIN MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010002-2"
  },
  {
    "votes": 13926,
    "candidate": {
      "name": {
        "my": "ဦးမြဝင်း",
        "en": "U MYA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010002-2"
  },
  {
    "votes": 6961,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဆေွ",
        "en": "U THEIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010002-2"
  },
  {
    "votes": 20509,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်ကြူ",
        "en": "DR. MYINT KYU"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010004-1"
  },
  {
    "votes": 19764,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရှိန်",
        "en": "U TUN SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010004-1"
  },
  {
    "votes": 20721,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဝင်း",
        "en": "U TUN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010004-2"
  },
  {
    "votes": 20319,
    "candidate": {
      "name": {
        "my": "ဦးလှခိုင်",
        "en": "U HLA KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010004-2"
  },
  {
    "votes": 52266,
    "candidate": {
      "name": {
        "my": "ဦးတိုင်းအေး",
        "en": "U TIME AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010020-1"
  },
  {
    "votes": 21389,
    "candidate": {
      "name": {
        "my": "ဦးလှကို",
        "en": "U HLA KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010020-1"
  },
  {
    "votes": 58914,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010020-2"
  },
  {
    "votes": 16534,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဌေး",
        "en": "U TIN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010020-2"
  },
  {
    "votes": 64346,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်သန်း",
        "en": "U AUNG MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010013-1"
  },
  {
    "votes": 10067,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကြည်",
        "en": "U TUN KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010013-1"
  },
  {
    "votes": 54532,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိုးမြင့်",
        "en": "U THAN SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010013-2"
  },
  {
    "votes": 5089,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်စိုး",
        "en": "U KHIN MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010013-2"
  },
  {
    "votes": 31168,
    "candidate": {
      "name": {
        "my": " ဦးသန်းဦး",
        "en": "U THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010009-1"
  },
  {
    "votes": 19874,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010009-1"
  },
  {
    "votes": 31201,
    "candidate": {
      "name": {
        "my": "ဦးကိုလေး",
        "en": "U KO LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010009-2"
  },
  {
    "votes": 18962,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010009-2"
  },
  {
    "votes": 16868,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မောင်း",
        "en": "U AUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010003-1"
  },
  {
    "votes": 14788,
    "candidate": {
      "name": {
        "my": " ဦးမျိုးချစ်",
        "en": "U MYO CHIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010003-1"
  },
  {
    "votes": 20800,
    "candidate": {
      "name": {
        "my": "ဦးပြေဝင်း",
        "en": "U PYA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010003-2"
  },
  {
    "votes": 19495,
    "candidate": {
      "name": {
        "my": " ဦးတင်ဖေ",
        "en": "U TIN PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010003-2"
  },
  {
    "votes": 33869,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမော်ရီ",
        "en": "U SOE MAW YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010029-1"
  },
  {
    "votes": 5863,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်မာကြည်",
        "en": "DAW KHIN MAR KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010029-1"
  },
  {
    "votes": 33703,
    "candidate": {
      "name": {
        "my": "ဦးရေွှနန်",
        "en": "U SHWE NAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010029-2"
  },
  {
    "votes": 5864,
    "candidate": {
      "name": {
        "my": "ဦးတင်ညို",
        "en": "U TIN NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010029-2"
  },
  {
    "votes": 64350,
    "candidate": {
      "name": {
        "my": "ဦးစောဌေး",
        "en": "U SAW HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010028-1"
  },
  {
    "votes": 19459,
    "candidate": {
      "name": {
        "my": "ဒေါ်သန်းသန်းမြင့်",
        "en": "DAW THAN THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010028-1"
  },
  {
    "votes": 61483,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": "U KYAW AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010028-2"
  },
  {
    "votes": 13507,
    "candidate": {
      "name": {
        "my": "ဦးပေါ်ဦး",
        "en": "U PAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010028-2"
  },
  {
    "votes": 11227,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ငေွ",
        "en": "U AUNG NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010011-1"
  },
  {
    "votes": 13347,
    "candidate": {
      "name": {
        "my": "ဦးဝုန်ဂိုလျန်",
        "en": "?U WAN GAO LAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010011-1"
  },
  {
    "votes": 14863,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010011-2"
  },
  {
    "votes": 9359,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010011-2"
  },
  {
    "votes": 49380,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်း",
        "en": "U KYAW SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010017-1"
  },
  {
    "votes": 7614,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ရင်",
        "en": "U KYAW YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010017-1"
  },
  {
    "votes": 9799,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သိန်း",
        "en": "U AUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR010017-1"
  },
  {
    "votes": 45890,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇံ",
        "en": "U AUNG ZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010017-2"
  },
  {
    "votes": 9329,
    "candidate": {
      "name": {
        "my": "ဦးလှကြိုင်",
        "en": "U HLA KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010017-2"
  },
  {
    "votes": 9387,
    "candidate": {
      "name": {
        "my": "ဒေါ်စိုးစိုး(ခ)ဒေါ်စိုးစိုးထွဏ်း",
        "en": "DAW SOE SOE (A) DAW SOE SOE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR010017-2"
  },
  {
    "votes": 44160,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြည်",
        "en": "U AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010015-1"
  },
  {
    "votes": 8774,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကို",
        "en": "U MAUNG KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010015-1"
  },
  {
    "votes": 7423,
    "candidate": {
      "name": {
        "my": "ဦးအေးကို",
        "en": "U AYE KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010015-2"
  },
  {
    "votes": 38950,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လင်း",
        "en": "U AUNG LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010015-2"
  },
  {
    "votes": 6640,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သန်း",
        "en": "U MAUNG MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010015-2"
  },
  {
    "votes": 48728,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဖေ",
        "en": "U KYI PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010019-1"
  },
  {
    "votes": 3800,
    "candidate": {
      "name": {
        "my": " ဦးရာတိုး",
        "en": "U YAR TOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010019-1"
  },
  {
    "votes": 47998,
    "candidate": {
      "name": {
        "my": "ဦးအေးသန့်ဆေွ",
        "en": "U AYE THANT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010019-2"
  },
  {
    "votes": 5563,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညွန့်",
        "en": "U KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010019-2"
  },
  {
    "votes": 22548,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ထင်",
        "en": "U KYAW TAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010021-1"
  },
  {
    "votes": 9742,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး",
        "en": "U KHIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010021-1"
  },
  {
    "votes": 16373,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်သူ",
        "en": "U MYUNT THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010021-2"
  },
  {
    "votes": 7949,
    "candidate": {
      "name": {
        "my": "ဦးတင်နွယ်",
        "en": "U TIN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010021-2"
  },
  {
    "votes": 37371,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်",
        "en": "U HTAY LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010022-1"
  },
  {
    "votes": 19753,
    "candidate": {
      "name": {
        "my": "ဦးစံရေွှ",
        "en": "U SAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010022-1"
  },
  {
    "votes": 40640,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဌေးအောင်",
        "en": "U THAN HTAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010022-2"
  },
  {
    "votes": 15251,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိုး",
        "en": "U AUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010022-2"
  },
  {
    "votes": 32574,
    "candidate": {
      "name": {
        "my": "ဦးဆေွသန်း",
        "en": "U SWE THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010007-1"
  },
  {
    "votes": 21132,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010007-1"
  },
  {
    "votes": 24694,
    "candidate": {
      "name": {
        "my": " ဦးကိုကိုလွင်",
        "en": "U KO KO LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010007-2"
  },
  {
    "votes": 12809,
    "candidate": {
      "name": {
        "my": "သူရဦးရေွှလှ",
        "en": "THURA OO SHWE HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010007-2"
  },
  {
    "votes": 69788,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010024-1"
  },
  {
    "votes": 8452,
    "candidate": {
      "name": {
        "my": "ဦးမာတင်",
        "en": "U MARTIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010024-1"
  },
  {
    "votes": 64356,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး",
        "en": "U TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010024-2"
  },
  {
    "votes": 9200,
    "candidate": {
      "name": {
        "my": "ဦးလှအောင်",
        "en": "U HLA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010024-2"
  },
  {
    "votes": 15097,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010005-1"
  },
  {
    "votes": 12220,
    "candidate": {
      "name": {
        "my": "ဦးစိန်သန်း",
        "en": "U SEIN THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010005-1"
  },
  {
    "votes": 17173,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလှ",
        "en": "U THEIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010005-2"
  },
  {
    "votes": 12140,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စန်း",
        "en": "U MYINT SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010005-2"
  },
  {
    "votes": 36211,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဌေးကျော်",
        "en": "U AUNG HTAY KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010008-1"
  },
  {
    "votes": 18022,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010008-1"
  },
  {
    "votes": 41944,
    "candidate": {
      "name": {
        "my": "ဦးရဲမြင့်",
        "en": "U YE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010008-2"
  },
  {
    "votes": 16450,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်မောင်",
        "en": "U MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010008-2"
  },
  {
    "votes": 18107,
    "candidate": {
      "name": {
        "my": "ဦးဗိုလ်ချုပ်",
        "en": "U BO GYOUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010010-1"
  },
  {
    "votes": 7455,
    "candidate": {
      "name": {
        "my": "ဦးတင်သိန်း",
        "en": "U TIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010010-1"
  },
  {
    "votes": 19434,
    "candidate": {
      "name": {
        "my": "ဦးတောက်",
        "en": "U TAUNGT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010010-2"
  },
  {
    "votes": 11020,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်သန်း",
        "en": "DR. MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010010-2"
  },
  {
    "votes": 28995,
    "candidate": {
      "name": {
        "my": "ဦးညီညီ",
        "en": "U KYI KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010014-1"
  },
  {
    "votes": 9866,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010014-1"
  },
  {
    "votes": 26391,
    "candidate": {
      "name": {
        "my": "ဦးစိန်သောင်း",
        "en": "U SEIN THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010014-2"
  },
  {
    "votes": 7302,
    "candidate": {
      "name": {
        "my": "ဦးသက်တင်",
        "en": "U THET TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010014-2"
  },
  {
    "votes": 37584,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သန်း",
        "en": "U MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010016-1"
  },
  {
    "votes": 4497,
    "candidate": {
      "name": {
        "my": "ဦးမြစိန်",
        "en": "U MYA SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010016-1"
  },
  {
    "votes": 30568,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သူ",
        "en": "U AUNG THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010016-2"
  },
  {
    "votes": 7207,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ညို",
        "en": "U MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010016-2"
  },
  {
    "votes": 56379,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဌေးအောင်",
        "en": "U THAN HTAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010018-1"
  },
  {
    "votes": 4769,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းမြင့်",
        "en": "U THAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010018-1"
  },
  {
    "votes": 66707,
    "candidate": {
      "name": {
        "my": " ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010018-2"
  },
  {
    "votes": 3632,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဆောင်",
        "en": "U WIN SAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010018-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "Y",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010012-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": "U TIN TUN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010012-2"
  },
  {
    "votes": 38359,
    "candidate": {
      "name": {
        "my": "ဦးမိုးမြင့်သိန်း",
        "en": "U MOE MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010030-1"
  },
  {
    "votes": 11344,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010030-1"
  },
  {
    "votes": 34092,
    "candidate": {
      "name": {
        "my": "ဦးစန်ထွန်း",
        "en": "U SAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010030-2"
  },
  {
    "votes": 6655,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010030-2"
  },
  {
    "votes": 8999,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်း",
        "en": "U KYAW SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010030-2"
  },
  {
    "votes": 40370,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ဦး",
        "en": "U MAUNG MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010031-1"
  },
  {
    "votes": 21037,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010031-1"
  },
  {
    "votes": 37979,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010031-2"
  },
  {
    "votes": 19007,
    "candidate": {
      "name": {
        "my": "ုဦးကြည်အောင်",
        "en": "U KYI AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010031-2"
  },
  {
    "votes": 68111,
    "candidate": {
      "name": {
        "my": " ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010023-1"
  },
  {
    "votes": 11622,
    "candidate": {
      "name": {
        "my": "ဦးစိုးလွင်",
        "en": "U SOE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010023-1"
  },
  {
    "votes": 53695,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010023-2"
  },
  {
    "votes": 13165,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010023-2"
  },
  {
    "ethnic_seat": true,
    "votes": 7821,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010",
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းနွမ်(ခ)ဦးစိုင်းဆိုင်နွမ်",
        "en": "U SAI NWANG (A) SAI SAING NWANG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 13636,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "MANDALAY-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR010",
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမောင်လှ",
        "en": "U SAI MAUNG HLA"
      }
    }
  },
  {
    "votes": 13528,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်မောင်သွင်",
        "en": "DR. KHIN MAUNG THWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011010-1"
  },
  {
    "votes": 13281,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမောင်",
        "en": "U SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011010-1"
  },
  {
    "votes": 12138,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမော်ဦး",
        "en": "U WIN MAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011010-2"
  },
  {
    "votes": 10507,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011010-2"
  },
  {
    "votes": 14870,
    "candidate": {
      "name": {
        "my": "မင်းအောင်နိုင်ဦး",
        "en": "Min Aung Naing Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011003-1"
  },
  {
    "votes": 6852,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရှိန်",
        "en": "U TIN SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011003-1"
  },
  {
    "votes": 2320,
    "candidate": {
      "name": {
        "my": "ဦးမင်းမောင်မြင့်",
        "en": "U MIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011003-1"
  },
  {
    "votes": 9707,
    "candidate": {
      "name": {
        "my": "နိုင်သောင်းတင်",
        "en": "Naing Thaung Tin"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011003-2"
  },
  {
    "votes": 12358,
    "candidate": {
      "name": {
        "my": "ဦးကျင်ဖေ",
        "en": "U KYIN PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011003-2"
  },
  {
    "votes": 3937,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းလွင်",
        "en": "U OHN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011003-2"
  },
  {
    "votes": 6351,
    "candidate": {
      "name": {
        "my": "နိုင်အုန်းသောင်း",
        "en": "Naing Ohn Thaung"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011002-1"
  },
  {
    "votes": 4512,
    "candidate": {
      "name": {
        "my": "စောသန်းကျော်ဦး",
        "en": "Saw Than Kyaw Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR011002-1"
  },
  {
    "votes": 4656,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းညိန်း",
        "en": "U TUN NYAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011002-1"
  },
  {
    "votes": 12145,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011002-1"
  },
  {
    "votes": 1476,
    "candidate": {
      "name": {
        "my": "ဗိုလ်မှူးကြီးသာအောင်(ငြိမ်း",
        "en": "COLONEL THAR AUNG (RETD"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR011002-2"
  },
  {
    "votes": 11724,
    "candidate": {
      "name": {
        "my": "နို်င်စံတင်",
        "en": "Naing San Tin"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011002-2"
  },
  {
    "votes": 7214,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြို့",
        "en": "U AUNG MYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011002-2"
  },
  {
    "votes": 2175,
    "candidate": {
      "name": {
        "my": "ဦးငေွစိုး",
        "en": "U NWE SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011002-2"
  },
  {
    "votes": 14705,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်",
        "en": "U HTAY LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011009-1"
  },
  {
    "votes": 11296,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011009-1"
  },
  {
    "votes": 13000,
    "candidate": {
      "name": {
        "my": "ဦးစိန်မြင့်",
        "en": "U SEIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011009-2"
  },
  {
    "votes": 10506,
    "candidate": {
      "name": {
        "my": "ဦးဝမ်စိန်",
        "en": "U WAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011009-2"
  },
  {
    "votes": 10383,
    "candidate": {
      "name": {
        "my": "နိုင်စိုးသိန်း",
        "en": "Naing Soe Thein"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011001-1"
  },
  {
    "votes": 9156,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိန်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011001-1"
  },
  {
    "votes": 4531,
    "candidate": {
      "name": {
        "my": "ဦးလှရှိန်",
        "en": "U HLA SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR011001-1"
  },
  {
    "votes": 14600,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာလှဦး",
        "en": "DR. HLA OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011001-1"
  },
  {
    "votes": 11810,
    "candidate": {
      "name": {
        "my": "နိုင်ကျန်ရစ်",
        "en": "Naing Kyant Yaint"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011001-2"
  },
  {
    "votes": 11734,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011001-2"
  },
  {
    "votes": 24948,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာတိုးတိုးအောင်",
        "en": "DR. TOE TOE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011001-2"
  },
  {
    "votes": 9678,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သိုက်",
        "en": "U Maung Theit"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011005-1"
  },
  {
    "votes": 21530,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းမြင့်",
        "en": "U OHN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011005-1"
  },
  {
    "votes": 2612,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011005-1"
  },
  {
    "votes": 16964,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမင်းနွယ်စိုး",
        "en": "Dr. Min Nwe Soe"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011005-2"
  },
  {
    "votes": 9334,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ထွန်း",
        "en": "U KYI TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011005-2"
  },
  {
    "votes": 2493,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းငယ်",
        "en": "U THAUNG NGE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011005-2"
  },
  {
    "votes": 14245,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်",
        "en": "U THANT ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011008-1"
  },
  {
    "votes": 14267,
    "candidate": {
      "name": {
        "my": "ဦးမောင်လန်း",
        "en": "U MAUNG LAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011008-1"
  },
  {
    "votes": 9446,
    "candidate": {
      "name": {
        "my": "ဦးမင်းဝဏ္ဏကြီး",
        "en": "U MIN WANA GYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011008-1"
  },
  {
    "votes": 12016,
    "candidate": {
      "name": {
        "my": "မင်းချမ်းမြေ့",
        "en": "Min Chan Myant"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011008-2"
  },
  {
    "votes": 10164,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းရေွှ",
        "en": "DR. WIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011008-2"
  },
  {
    "votes": 9878,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်လိှုင်",
        "en": "U NYUNT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011008-2"
  },
  {
    "votes": 5948,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းရှိန်",
        "en": "U Win Shaine"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011004-1"
  },
  {
    "votes": 4578,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": "U TIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011004-1"
  },
  {
    "votes": 6314,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးညွန့်",
        "en": "U MYO NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011004-1"
  },
  {
    "votes": 3419,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဌေး",
        "en": "U THEIN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR011004-1"
  },
  {
    "votes": 6343,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ဦး (ခ) ဦးဝင်း",
        "en": "U Naing Oo (a) U Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011004-2"
  },
  {
    "votes": 3267,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းတည်",
        "en": "U WIN TEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011004-2"
  },
  {
    "votes": 1739,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းရှိန်",
        "en": "U WIN SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011004-2"
  },
  {
    "votes": 15013,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသန်းမျိုး",
        "en": "DR. THAN MYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011007-1"
  },
  {
    "votes": 16025,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလိှုင်",
        "en": "U TUN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011007-1"
  },
  {
    "votes": 19461,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဇော်",
        "en": "U THEIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011007-2"
  },
  {
    "votes": 18876,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ညွန့်",
        "en": "U MAUNG MAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011007-2"
  },
  {
    "votes": 16850,
    "candidate": {
      "name": {
        "my": "နိုင်လဝီအောင်",
        "en": "Naing Lawei Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011006-1"
  },
  {
    "votes": 6842,
    "candidate": {
      "name": {
        "my": "ဦးဖေအောင်",
        "en": "U PAE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011006-1"
  },
  {
    "votes": 2090,
    "candidate": {
      "name": {
        "my": "ဦးမင်းထိန်",
        "en": "U MIN THEAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011006-1"
  },
  {
    "votes": 19198,
    "candidate": {
      "name": {
        "my": "နိုင်သိန်းဟန်",
        "en": "Naing Thein Han"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011006-2"
  },
  {
    "votes": 3300,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": "U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011006-2"
  },
  {
    "votes": 7964,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မြင့်",
        "en": "U MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011006-2"
  },
  {
    "ethnic_seat": true,
    "votes": 121621,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးသက်ဝင်း",
        "en": "U THET WIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 97966,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 17362,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "နော်ဆာထူး",
        "en": "DAW NAW SAR HTOO"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 23247,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်သိန်း",
        "en": "U AUNG KYAW THEIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 22243,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးမောင်ရေွှ",
        "en": "U MAUNG SHWE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 9512,
    "acclaim": "N",
    "ethnicity": "PaO",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးဖေမြ(ခ)ဦးခွန်ဖေမြ",
        "en": "U PAE MYA (A) KHUN PAE MYA"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 9482,
    "acclaim": "N",
    "ethnicity": "PaO",
    "parliament_code": "MON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011",
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    }
  },
  {
    "votes": 22490,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်တင်",
        "en": "U HLA MAUNG TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012014-1"
  },
  {
    "votes": 3615,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး",
        "en": "U TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012014-1"
  },
  {
    "votes": 19888,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012014-2"
  },
  {
    "votes": 3958,
    "candidate": {
      "name": {
        "my": "ဦးချက်ဖါး",
        "en": "U CHAT PAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012014-2"
  },
  {
    "votes": 31678,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စန်ရေွှ",
        "en": "U MAUNG SAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012010-1"
  },
  {
    "votes": 4510,
    "candidate": {
      "name": {
        "my": "ဦးကေပေါက်",
        "en": "U K. PAUGHT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012010-1"
  },
  {
    "votes": 34158,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်(ခ)ဇာဟီဒူလ္လာ",
        "en": "U AUNG MYINT (A) ZAHI DULAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012010-1"
  },
  {
    "votes": 8936,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်သိန်း",
        "en": "U TUN AUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012010-2"
  },
  {
    "votes": 23417,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဘော်ရှီရ်အာမက်",
        "en": "DR. BARSHI ARMAD"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012010-2"
  },
  {
    "votes": 27942,
    "candidate": {
      "name": {
        "my": "ဦးဘော်ရှီးအာမက်",
        "en": "U BAW SHE ARMAD"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012010-2"
  },
  {
    "votes": 5538,
    "candidate": {
      "name": {
        "my": " ဦးထိန်လင်း",
        "en": "U HTEIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012017-1"
  },
  {
    "votes": 4680,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းအေး",
        "en": "U THAUNG AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012017-1"
  },
  {
    "votes": 7471,
    "candidate": {
      "name": {
        "my": "ဦးစိုးအေး",
        "en": "U SOE AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012017-2"
  },
  {
    "votes": 6654,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012017-2"
  },
  {
    "votes": 10218,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဖေ",
        "en": "U TIN PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012011-1"
  },
  {
    "votes": 8575,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအေး",
        "en": "U THAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012011-1"
  },
  {
    "votes": 3096,
    "candidate": {
      "name": {
        "my": "ဦးခင်ရေွှ",
        "en": "U KHIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012011-1"
  },
  {
    "votes": 10814,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လွင်",
        "en": "U KYAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012011-2"
  },
  {
    "votes": 7623,
    "candidate": {
      "name": {
        "my": "ဦးနန်းအောင်ထွန်း",
        "en": "U NAN AUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012011-2"
  },
  {
    "votes": 2768,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြ",
        "en": "U TUN MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012011-2"
  },
  {
    "votes": 9357,
    "candidate": {
      "name": {
        "my": "ဦးဘသာ",
        "en": "U BA THA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 18542,
    "candidate": {
      "name": {
        "my": "ဦးမောင်လှကျော်",
        "en": "U MAUNG HLA KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 5984,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်း",
        "en": "U KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 1292,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ရင်ထေွး",
        "en": "U MAUNG YIN HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 1428,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်သိန်း",
        "en": "U AUNG KYAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 5463,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်းစိန်",
        "en": "U AUNG TUN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012004-1"
  },
  {
    "votes": 5401,
    "candidate": {
      "name": {
        "my": "ဦးမီးလုံး",
        "en": "U MEI LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 22712,
    "candidate": {
      "name": {
        "my": "ဦးစောငြိမ်း",
        "en": "U SAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 4501,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ထွန်းနိုင်",
        "en": "U KYAW TUN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 1533,
    "candidate": {
      "name": {
        "my": "ဦးလှစော",
        "en": "U HLA SAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 2065,
    "candidate": {
      "name": {
        "my": "ဥိးထွန်းစိန်",
        "en": "U TUN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 7319,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်းလိှုင်",
        "en": "U AUNG TUN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012004-2"
  },
  {
    "votes": 38867,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012009-1"
  },
  {
    "votes": 71508,
    "candidate": {
      "name": {
        "my": "ဦးဇာဟိန်ဂိရ်(ခ)အောင်မျိုးမင်း",
        "en": "U ZAHEAING GI (A) AUNG MYO MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012009-1"
  },
  {
    "votes": 2263,
    "candidate": {
      "name": {
        "my": "ဦးဦးထွန်းမောင်",
        "en": "U TUN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012009-1"
  },
  {
    "votes": 31320,
    "candidate": {
      "name": {
        "my": "ချေးခေါက်အလီ (ခ) ဦးစောမင်း",
        "en": "CHAY KAUK ALI (a) U SAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012009-2"
  },
  {
    "votes": 52923,
    "candidate": {
      "name": {
        "my": "ဦးမြအောင်",
        "en": "U MYA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012009-2"
  },
  {
    "votes": 2205,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ထွန်းအောင်",
        "en": "U CHIT TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012009-2"
  },
  {
    "votes": 690,
    "candidate": {
      "name": {
        "my": "ဦးအိုမောရ်ဖိုက်ဆဲလ်",
        "en": "U OHMAN PIP HSA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPP",
    "constituency": "MMR012009-2"
  },
  {
    "votes": 13654,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလင်း",
        "en": "U TUN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012005-1"
  },
  {
    "votes": 8468,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ထွန်း",
        "en": "U MAUNG MAUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012005-1"
  },
  {
    "votes": 4470,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ဘတင်",
        "en": "U MAUNG BA TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012005-1"
  },
  {
    "votes": 1161,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကြိုင်",
        "en": "U WIN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012005-1"
  },
  {
    "votes": 16430,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်ဇံ",
        "en": "U MAUNG KYAW ZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012005-2"
  },
  {
    "votes": 7182,
    "candidate": {
      "name": {
        "my": "ဦးဘသိန်း",
        "en": "U BA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012005-2"
  },
  {
    "votes": 3895,
    "candidate": {
      "name": {
        "my": "ဦးရေွှကျော်အောင်",
        "en": "U SHWE KYAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012005-2"
  },
  {
    "votes": 1004,
    "candidate": {
      "name": {
        "my": "ဥိးကျော်သိန်းလှ",
        "en": "U KYAW THEIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012005-2"
  },
  {
    "votes": 23845,
    "candidate": {
      "name": {
        "my": "ဦးဦးထွန်းလှုိင်",
        "en": "U U TUN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012003-1"
  },
  {
    "votes": 7246,
    "candidate": {
      "name": {
        "my": "ဦးဘိုမောင်",
        "en": "U BO MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012003-1"
  },
  {
    "votes": 3699,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သာကျော်",
        "en": "U MAUNG THA KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012003-1"
  },
  {
    "votes": 1960,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇော်ဇံ",
        "en": "U AUNG ZAW ZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012003-1"
  },
  {
    "votes": 1549,
    "candidate": {
      "name": {
        "my": "ဦးဘသိန်း",
        "en": "U BA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012003-1"
  },
  {
    "votes": 20518,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သိန်း",
        "en": "U KYAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 6095,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သန်းစိန်",
        "en": "U MAUNG THAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 2495,
    "candidate": {
      "name": {
        "my": "ဦးသာဇံလှ",
        "en": "U THA ZAN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 2646,
    "candidate": {
      "name": {
        "my": "ဦးလာဘေွ",
        "en": "U BA BWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 1876,
    "candidate": {
      "name": {
        "my": "ဦးအေးကြိုင်",
        "en": "U AYE KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 6935,
    "candidate": {
      "name": {
        "my": " ဦးနုရ်အာလောင််််",
        "en": "U NU ARLAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012003-2"
  },
  {
    "votes": 3257,
    "candidate": {
      "name": {
        "my": "ဦးဘကန်",
        "en": "U BA KAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012012-1"
  },
  {
    "votes": 3052,
    "candidate": {
      "name": {
        "my": "ဦးအေးဖေ",
        "en": "U AYE PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012012-1"
  },
  {
    "votes": 3593,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြီး",
        "en": "U AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012012-1"
  },
  {
    "votes": 1744,
    "candidate": {
      "name": {
        "my": "ဦးဘိုးသာဦး",
        "en": "U BOE THA OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR012012-1"
  },
  {
    "votes": 4230,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ညွန့်",
        "en": "U AUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012012-2"
  },
  {
    "votes": 4890,
    "candidate": {
      "name": {
        "my": "ဦးသာညွန့်",
        "en": "U THA NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012012-2"
  },
  {
    "votes": 3540,
    "candidate": {
      "name": {
        "my": "ဦးငေွယဉ်",
        "en": "U NGWE YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012012-2"
  },
  {
    "votes": 7855,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012006-1"
  },
  {
    "votes": 6272,
    "candidate": {
      "name": {
        "my": "ဦးလှစောအောင်",
        "en": "U HLA SAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012006-1"
  },
  {
    "votes": 2799,
    "candidate": {
      "name": {
        "my": "ဦးသာဖြူအောင်",
        "en": "U THAR PHYU AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012006-1"
  },
  {
    "votes": 5900,
    "candidate": {
      "name": {
        "my": "ဦးမောင်လုံး",
        "en": "U MAUNG LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012006-2"
  },
  {
    "votes": 4736,
    "candidate": {
      "name": {
        "my": " ဥိးကျော်လှ",
        "en": "U KYAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012006-2"
  },
  {
    "votes": 1573,
    "candidate": {
      "name": {
        "my": " ဦးစိန်ချမ်းနု",
        "en": "U SEIN CHAN NU"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012006-2"
  },
  {
    "votes": 17557,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်သိန်း",
        "en": "U MAUNG KYAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012007-1"
  },
  {
    "votes": 6518,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ထွန်းမြ",
        "en": "U MUANG TUN MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012007-1"
  },
  {
    "votes": 2670,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ဝင်း",
        "en": "U AUNG KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012007-1"
  },
  {
    "votes": 13282,
    "candidate": {
      "name": {
        "my": "ဦးတက်ထွန်းအောင်",
        "en": "U TET TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012007-2"
  },
  {
    "votes": 4549,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သိန်း",
        "en": "U KHIN MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012007-2"
  },
  {
    "votes": 3048,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သာကျော်",
        "en": "U AUNG THAR KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012007-2"
  },
  {
    "votes": 1015,
    "candidate": {
      "name": {
        "my": "ဥိးခင်မောင်သန်း",
        "en": "U KHIN MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO",
    "constituency": "MMR012007-2"
  },
  {
    "votes": 12603,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်းမြ",
        "en": "U AUNG TUN MRA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012002-1"
  },
  {
    "votes": 6376,
    "candidate": {
      "name": {
        "my": " ဦးမောင်ကျော်အေး",
        "en": "U MAUNG KYAW AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012002-1"
  },
  {
    "votes": 4215,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမောင်",
        "en": "U THEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012002-1"
  },
  {
    "votes": 11615,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းတင်",
        "en": "U AUNG THAN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012002-2"
  },
  {
    "votes": 4631,
    "candidate": {
      "name": {
        "my": " ဦးမောင်ကြီး",
        "en": "U MAUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012002-2"
  },
  {
    "votes": 3531,
    "candidate": {
      "name": {
        "my": "ဦးစောသာမောင်",
        "en": "U SAW THA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012002-2"
  },
  {
    "votes": 2632,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်းံ",
        "en": "U KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012002-2"
  },
  {
    "votes": 8273,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012013-1"
  },
  {
    "votes": 8563,
    "candidate": {
      "name": {
        "my": "ဦးသန်းနိုင်(ခ)ဦးဘသိန်း",
        "en": "U THAN NAING (A) U BA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012013-1"
  },
  {
    "votes": 1239,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": " Kaman National Progressive Party",
    "constituency": "MMR012013-2"
  },
  {
    "votes": 5162,
    "candidate": {
      "name": {
        "my": "ဦးစံခင်",
        "en": "U SAN KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012013-2"
  },
  {
    "votes": 14649,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ခင်",
        "en": "U KYAW KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012013-2"
  },
  {
    "votes": 12675,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်သိန်း",
        "en": "U HLA MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012008-1"
  },
  {
    "votes": 10617,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမောင်",
        "en": "U THEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012008-1"
  },
  {
    "votes": 4743,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သိန်းလိှုင်",
        "en": "U MAUNG THEIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012008-1"
  },
  {
    "votes": 15587,
    "candidate": {
      "name": {
        "my": "ဦးဖိုးမင်း",
        "en": "U PO MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012008-2"
  },
  {
    "votes": 7237,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012008-2"
  },
  {
    "votes": 2843,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမောင်",
        "en": "U THEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012008-2"
  },
  {
    "votes": 14493,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012001-1"
  },
  {
    "votes": 1976,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ငြိမ်း",
        "en": "U KYAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": " Kaman National Progressive Party",
    "constituency": "MMR012001-1"
  },
  {
    "votes": 16998,
    "candidate": {
      "name": {
        "my": "ဦးသာလူရှည်",
        "en": "U THAR LU SHAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012001-1"
  },
  {
    "votes": 12620,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဇံလှ",
        "en": "U KYAW ZAN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012001-1"
  },
  {
    "votes": 1567,
    "candidate": {
      "name": {
        "my": "ဦးနီအောင်ကျော်",
        "en": "U NE AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012001-1"
  },
  {
    "votes": 3795,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လှထွန်း",
        "en": "U AUNG HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": " Kaman National Progressive Party",
    "constituency": "MMR012001-2"
  },
  {
    "votes": 24211,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြကျော်",
        "en": "U AUNG MYA KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012001-2"
  },
  {
    "votes": 14345,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစန်းရေွှ",
        "en": "DR. SAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012001-2"
  },
  {
    "votes": 2030,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်ဇံ",
        "en": "U MAUNG KYAW ZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012001-2"
  },
  {
    "votes": 9747,
    "candidate": {
      "name": {
        "my": "ဦးလှဟန်",
        "en": "U HLA HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012015-1"
  },
  {
    "votes": 6564,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012015-1"
  },
  {
    "votes": 6209,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းထွန်း",
        "en": "U THAUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012015-1"
  },
  {
    "votes": 9413,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဦး",
        "en": "U AUNG NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012015-2"
  },
  {
    "votes": 6523,
    "candidate": {
      "name": {
        "my": "ဦးချစ်တင်",
        "en": "U CHIT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012015-2"
  },
  {
    "votes": 5736,
    "candidate": {
      "name": {
        "my": " ဦးဘရှင်",
        "en": "U BA SHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012015-2"
  },
  {
    "votes": 7208,
    "candidate": {
      "name": {
        "my": "ဒေါ်သန်းစိန်",
        "en": "DAW THAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012016-1"
  },
  {
    "votes": 6614,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သိန်း",
        "en": "U MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012016-1"
  },
  {
    "votes": 11013,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာအောင်ကျော်ဝင်း",
        "en": "DR. AUNG KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012016-2"
  },
  {
    "votes": 6791,
    "candidate": {
      "name": {
        "my": "ဦးဘဝင်း",
        "en": "U BA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012016-2"
  },
  {
    "ethnic_seat": true,
    "votes": 0,
    "acclaim": "Y",
    "ethnicity": "Chin",
    "parliament_code": "RAKHINE-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012",
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုနိုင်",
        "en": "U KO KO"
      }
    }
  },
  {
    "votes": 25020,
    "candidate": {
      "name": {
        "my": "ဦးစောဦးထွန်း",
        "en": "U SAW TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005014-1"
  },
  {
    "votes": 10324,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသိန်း",
        "en": "U WIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005014-1"
  },
  {
    "votes": 27024,
    "candidate": {
      "name": {
        "my": "ဦးကံထွန်း",
        "en": "U KAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005014-2"
  },
  {
    "votes": 6881,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရေွှ",
        "en": "U TIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005014-2"
  },
  {
    "votes": 4725,
    "candidate": {
      "name": {
        "my": "ဦးမာကျင်",
        "en": "U Mar Kyin"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR005023-1"
  },
  {
    "votes": 1724,
    "candidate": {
      "name": {
        "my": "စိုင်းအောင်စိုးထွန်း",
        "en": "Sai Aung Soe Tun"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR005023-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်းလိှုင်",
        "en": "U SEIN WIN HLAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005013-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသာအေး",
        "en": "U THAR AYE"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005013-2"
  },
  {
    "votes": 6698,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်အေး",
        "en": "U Khin Maung Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR005015-1"
  },
  {
    "votes": 13317,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထိုက်",
        "en": "U THAN HTIGHT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005015-1"
  },
  {
    "votes": 3690,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမေွှး",
        "en": "U THAN MWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005015-1"
  },
  {
    "votes": 5086,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်",
        "en": "U TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005015-2"
  },
  {
    "votes": 14966,
    "candidate": {
      "name": {
        "my": "ဦးမှတ်(ခ) ဒေါက်တာမြင့်သိန်း",
        "en": "U MAT (A) DR. MYINT SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005015-2"
  },
  {
    "votes": 4723,
    "candidate": {
      "name": {
        "my": " ဦးစောမြင့်ဦး",
        "en": " U SAW MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005033-1"
  },
  {
    "votes": 1018,
    "candidate": {
      "name": {
        "my": "ဦးမိုးဇော်သွင်(ခ)ဥိးမြင့်ဇော်",
        "en": "U MOE ZAW THWIN (A) U MYINT ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005033-1"
  },
  {
    "votes": 3856,
    "candidate": {
      "name": {
        "my": "ဦးမြမောင်",
        "en": "U MYA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005033-2"
  },
  {
    "votes": 1138,
    "candidate": {
      "name": {
        "my": "ဦးရေွှထွန်း",
        "en": "U SHWE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005033-2"
  },
  {
    "votes": 821,
    "candidate": {
      "name": {
        "my": " ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR005033-2"
  },
  {
    "votes": 13951,
    "candidate": {
      "name": {
        "my": "ှုဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005034-1"
  },
  {
    "votes": 14360,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005034-1"
  },
  {
    "votes": 17036,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဆေွ",
        "en": "U KHIN MAUNG SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005034-2"
  },
  {
    "votes": 13106,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ညွန့်",
        "en": "U AUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005034-2"
  },
  {
    "votes": 11095,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005021-1"
  },
  {
    "votes": 11269,
    "candidate": {
      "name": {
        "my": "ဦးစံရွှင်",
        "en": "U SAN SWING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005021-1"
  },
  {
    "votes": 9254,
    "candidate": {
      "name": {
        "my": "ဦးတင်ငေွ",
        "en": "U TIN NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005021-2"
  },
  {
    "votes": 9643,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005021-2"
  },
  {
    "votes": 20516,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005027-1"
  },
  {
    "votes": 19897,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005027-1"
  },
  {
    "votes": 11274,
    "candidate": {
      "name": {
        "my": "ဦးဖူခေါ်တင်သန်",
        "en": "U PU KHAW TIN THANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005027-1"
  },
  {
    "votes": 28970,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သန်း",
        "en": "U KHIN MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005027-2"
  },
  {
    "votes": 14094,
    "candidate": {
      "name": {
        "my": "ဦးပြည်တင်",
        "en": "U PYE TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005027-2"
  },
  {
    "votes": 18734,
    "candidate": {
      "name": {
        "my": "ဦးပူဇံခံသန်",
        "en": "U PU ZAM KHEN THANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005027-2"
  },
  {
    "votes": 9162,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမောင်",
        "en": "U TUN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005028-1"
  },
  {
    "votes": 4488,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမန်း",
        "en": "U SHWE MAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005028-1"
  },
  {
    "votes": 8370,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုးဝင်း",
        "en": "U KYAW SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005028-2"
  },
  {
    "votes": 5937,
    "candidate": {
      "name": {
        "my": "ဦးဘဟိန်",
        "en": "U BA HEAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005028-2"
  },
  {
    "votes": 56198,
    "candidate": {
      "name": {
        "my": "ဦးသင်းလိှုင်",
        "en": "U THIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005007-1"
  },
  {
    "votes": 9388,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရေွှ",
        "en": "U TUN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005007-1"
  },
  {
    "votes": 43362,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်မောင်ဝင်း",
        "en": "DR. KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005007-2"
  },
  {
    "votes": 11843,
    "candidate": {
      "name": {
        "my": "ဦးဘိုမြင့်အောင်",
        "en": "U BO MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005007-2"
  },
  {
    "votes": 32932,
    "candidate": {
      "name": {
        "my": "ဦးရှိန်",
        "en": "U SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005017-1"
  },
  {
    "votes": 2497,
    "candidate": {
      "name": {
        "my": " ဦးအေးမင်း",
        "en": "U AYE MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005017-1"
  },
  {
    "votes": 30672,
    "candidate": {
      "name": {
        "my": "ဦးဇေယျ",
        "en": "U ZAYAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005017-2"
  },
  {
    "votes": 3783,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": "U KYAW AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005017-2"
  },
  {
    "votes": 14827,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005020-1"
  },
  {
    "votes": 10824,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထွန်း",
        "en": "U WIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005020-1"
  },
  {
    "votes": 17994,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ထွန်း",
        "en": "U SEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005020-2"
  },
  {
    "votes": 12160,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005020-2"
  },
  {
    "votes": 21709,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းဦး",
        "en": "U AUNG THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005024-1"
  },
  {
    "votes": 14172,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဝေ",
        "en": "U TUN WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005024-1"
  },
  {
    "votes": 10000,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005024-2"
  },
  {
    "votes": 10330,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005024-2"
  },
  {
    "votes": 28255,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်သန်း",
        "en": "U AUNG KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005005-1"
  },
  {
    "votes": 7048,
    "candidate": {
      "name": {
        "my": "ဦးချစ်တင််",
        "en": "U CHIT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005005-1"
  },
  {
    "votes": 30347,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုးငြိမ်း",
        "en": "U AUNG MYO NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005005-2"
  },
  {
    "votes": 7877,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005005-2"
  },
  {
    "votes": 14130,
    "candidate": {
      "name": {
        "my": "ဥိးစိုးကျော်နိုင်",
        "en": "U SOE KYAW NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005008-1"
  },
  {
    "votes": 5458,
    "candidate": {
      "name": {
        "my": " ဦးထွန်းအိ",
        "en": "U TUN EI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005008-1"
  },
  {
    "votes": 13148,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်ထွန်း",
        "en": "U MYO MYINT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005008-2"
  },
  {
    "votes": 9942,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005008-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": " ဦးရူစံကြူး",
        "en": "U RU SAN KYU"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005036-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဝါလို",
        "en": "U WAH LO"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005036-2"
  },
  {
    "votes": 2453,
    "candidate": {
      "name": {
        "my": " ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005035-1"
  },
  {
    "votes": 812,
    "candidate": {
      "name": {
        "my": "ဦးကီရှီမူး",
        "en": "U KI SHE MU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005035-1"
  },
  {
    "votes": 3216,
    "candidate": {
      "name": {
        "my": "ဦးကျော့နား",
        "en": "U KAW NAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005035-2"
  },
  {
    "votes": 499,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်းတင်",
        "en": "U AUNG WIN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005035-2"
  },
  {
    "votes": 5807,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်မြ",
        "en": "DAW KHIN MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005031-1"
  },
  {
    "votes": 6100,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005031-1"
  },
  {
    "votes": 6177,
    "candidate": {
      "name": {
        "my": "ဦးစိန်မောင်း",
        "en": "U SEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005031-2"
  },
  {
    "votes": 4657,
    "candidate": {
      "name": {
        "my": "ဦးဇော်လွင်",
        "en": "U ZAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005031-2"
  },
  {
    "votes": 18644,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဝင်း",
        "en": "U HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005029-1"
  },
  {
    "votes": 7203,
    "candidate": {
      "name": {
        "my": " ဦးဌေးအောင်",
        "en": "U HTAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005029-1"
  },
  {
    "votes": 22486,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်စန်း",
        "en": "U MAUNG MAUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005029-2"
  },
  {
    "votes": 6685,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညို",
        "en": "U KHIN MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005029-2"
  },
  {
    "votes": 42899,
    "candidate": {
      "name": {
        "my": "ဦးတင့်လိှုင်မြင့်",
        "en": "U TIN HLAING MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005012-1"
  },
  {
    "votes": 21591,
    "candidate": {
      "name": {
        "my": "ဦးငေွှငိမ်း",
        "en": "U NGWE NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005012-1"
  },
  {
    "votes": 40091,
    "candidate": {
      "name": {
        "my": "ဦးတင့်နိုင်",
        "en": "U TINT NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005012-2"
  },
  {
    "votes": 19784,
    "candidate": {
      "name": {
        "my": " ဒေါ်အုန်းစိန်",
        "en": "DAW OHN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005012-2"
  },
  {
    "votes": 6783,
    "candidate": {
      "name": {
        "my": "ဦးစန်း",
        "en": "U SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005003-1"
  },
  {
    "votes": 20045,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်",
        "en": "U WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005003-1"
  },
  {
    "votes": 19908,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ရေွှ",
        "en": "U NYUNT SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005003-2"
  },
  {
    "votes": 7205,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းရေွှ",
        "en": "U THEIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005003-2"
  },
  {
    "votes": 12166,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းမြင့်",
        "en": "U THAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005002-1"
  },
  {
    "votes": 6877,
    "candidate": {
      "name": {
        "my": "ဦးသက်အောင်",
        "en": "U THET AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005002-1"
  },
  {
    "votes": 12535,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဆေွ",
        "en": "U THEIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005002-2"
  },
  {
    "votes": 8280,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဦး",
        "en": "U THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005002-2"
  },
  {
    "votes": 3750,
    "candidate": {
      "name": {
        "my": "ဦးယောင်ရန်:",
        "en": "U WAUNG YAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005037-1"
  },
  {
    "votes": 3750,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မောင််",
        "en": "U KYAW MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005037-1"
  },
  {
    "votes": 6510,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ညွန့်",
        "en": "U MAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005037-2"
  },
  {
    "votes": 3777,
    "candidate": {
      "name": {
        "my": "ဦးတန်ချော",
        "en": "U TAN CHAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005037-2"
  },
  {
    "votes": 33323,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဆေွ",
        "en": "U TINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005019-1"
  },
  {
    "votes": 2882,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုဆေွ",
        "en": "U KOKO SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005019-1"
  },
  {
    "votes": 33180,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းစိန်",
        "en": "U THAUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005019-2"
  },
  {
    "votes": 3371,
    "candidate": {
      "name": {
        "my": "ဦးသန်းရေွှ",
        "en": "U THAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005019-2"
  },
  {
    "votes": 12336,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005032-1"
  },
  {
    "votes": 12502,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ထွန်း",
        "en": "U MAUNG MUANG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005032-1"
  },
  {
    "votes": 11980,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထွန်း",
        "en": "U KHIN MAUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005032-2"
  },
  {
    "votes": 10495,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005032-2"
  },
  {
    "votes": 11971,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ကျော်",
        "en": "U MYINT KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005026-1"
  },
  {
    "votes": 8636,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မောင်း",
        "en": "U AUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005026-1"
  },
  {
    "votes": 14174,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005026-2"
  },
  {
    "votes": 11424,
    "candidate": {
      "name": {
        "my": "ဦးလူမြင့်",
        "en": "U LU MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005026-2"
  },
  {
    "votes": 40799,
    "candidate": {
      "name": {
        "my": " ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005001-1"
  },
  {
    "votes": 17752,
    "candidate": {
      "name": {
        "my": "ဦးသက်ထွန်း",
        "en": "U THET TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005001-1"
  },
  {
    "votes": 52562,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ခဲ",
        "en": "U SEIN KHAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005001-2"
  },
  {
    "votes": 11695,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်",
        "en": "U HLA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005001-2"
  },
  {
    "votes": 21507,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005018-1"
  },
  {
    "votes": 9134,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လင်းသန်း",
        "en": "U KYAW LIN THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005018-1"
  },
  {
    "votes": 21990,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇော်ဦး",
        "en": "U AUNG ZAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005018-2"
  },
  {
    "votes": 7339,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ရီ",
        "en": "U NYUNT YE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005018-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးနီပွန်",
        "en": "U NI PONG"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005004-1"
  },
  {
    "votes": 38384,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကိုကို",
        "en": "U TUN KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005004-2"
  },
  {
    "votes": 15059,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005004-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်းဦး",
        "en": "U THEIN TUN OO"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005010-1"
  },
  {
    "votes": 14167,
    "candidate": {
      "name": {
        "my": "ဦးဇင်မင်းထင်(ခ)ဦးဖိုးခါ",
        "en": "U ZIN MIN TIN (A) U POE KHAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005010-2"
  },
  {
    "votes": 9241,
    "candidate": {
      "name": {
        "my": "ဦးတိုက်ရီ",
        "en": "U TIGHT YE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005010-2"
  },
  {
    "votes": 13139,
    "candidate": {
      "name": {
        "my": " ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005030-1"
  },
  {
    "votes": 5657,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရှိန်",
        "en": "U THAUNG SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005030-1"
  },
  {
    "votes": 5077,
    "candidate": {
      "name": {
        "my": "ဦးလားလ်ဗင်ထန်း",
        "en": "U LAR VIN HTAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005030-1"
  },
  {
    "votes": 9454,
    "candidate": {
      "name": {
        "my": "ဦးထန်လျန်နား",
        "en": "U TAN LAN NAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005030-2"
  },
  {
    "votes": 3008,
    "candidate": {
      "name": {
        "my": "ဦးလွန်ခိုထန်း(ခ)ဦးတင်မောင်",
        "en": "U LON KHO TAN (A) U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005030-2"
  },
  {
    "votes": 5256,
    "candidate": {
      "name": {
        "my": "ဦးထန်မင်းလှျင့်",
        "en": "U HTAN MIN LINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005030-2"
  },
  {
    "votes": 21646,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဇော်မြင့်",
        "en": "U WIN ZAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005011-1"
  },
  {
    "votes": 7664,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ရင်",
        "en": "U KYAW YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005011-1"
  },
  {
    "votes": 27824,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းအောင်",
        "en": "U WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005011-2"
  },
  {
    "votes": 4863,
    "candidate": {
      "name": {
        "my": " ဦးသန်းဝေ",
        "en": "U THAN WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005011-2"
  },
  {
    "votes": 11556,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ကြိုင်",
        "en": "U SEIN KYAINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005022-1"
  },
  {
    "votes": 10727,
    "candidate": {
      "name": {
        "my": "ဦးလေးမောင်",
        "en": "U LAY MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005022-1"
  },
  {
    "votes": 10649,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမောင်",
        "en": "U SAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005022-2"
  },
  {
    "votes": 9422,
    "candidate": {
      "name": {
        "my": "ဦးဖုန်း",
        "en": "U PONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005022-2"
  },
  {
    "votes": 4832,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005006-1"
  },
  {
    "votes": 40364,
    "candidate": {
      "name": {
        "my": "ဥှီးတင်ငေွ",
        "en": "U TIN NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005006-1"
  },
  {
    "votes": 9553,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်စိုး",
        "en": "U KHIN MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005006-2"
  },
  {
    "votes": 40394,
    "candidate": {
      "name": {
        "my": "ဦးရဲတင့်",
        "en": "U YE TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005006-2"
  },
  {
    "votes": 5072,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်လှ",
        "en": "U TIN MAUNG HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005025-1"
  },
  {
    "votes": 11290,
    "candidate": {
      "name": {
        "my": "ဦးထိန်မင်း",
        "en": "U HTEIN MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005025-1"
  },
  {
    "votes": 10710,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လတ်",
        "en": "U KHIN MAUNG LATT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005025-2"
  },
  {
    "votes": 5063,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005025-2"
  },
  {
    "votes": 17335,
    "candidate": {
      "name": {
        "my": "ဦးညိုဝင်း",
        "en": "U NYO WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005009-1"
  },
  {
    "votes": 8093,
    "candidate": {
      "name": {
        "my": "ဦးနိုးနိုး",
        "en": "U NOE NOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005009-1"
  },
  {
    "votes": 18313,
    "candidate": {
      "name": {
        "my": "ဦးစိုးတင့်အောင်",
        "en": "U SOE TINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005009-2"
  },
  {
    "votes": 8446,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမောင်",
        "en": "U THAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005009-2"
  },
  {
    "votes": 18726,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်သိန်း",
        "en": "U KYAW MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005016-1"
  },
  {
    "votes": 10086,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005016-1"
  },
  {
    "votes": 7344,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမောင်",
        "en": "U SAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005016-2"
  },
  {
    "votes": 22516,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဗိုလ်မြင့်",
        "en": "U AUNG BO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005016-2"
  },
  {
    "ethnic_seat": true,
    "votes": 4526,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးစင်ဇာပန်",
        "en": "U CIN ZA PUM"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 17563,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးကျင်လျန်းမာန်",
        "en": "U KYIN LAN MAN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 33068,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးနိုထန်ဘဲလ်(ခ)နိုထန်ကပ်",
        "en": "U NOE TAN BAL (A) NO TAN KAT"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 11185,
    "acclaim": "N",
    "ethnicity": "Chin",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးအေးနော့စ်",
        "en": "U AYE NAUGH"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 38388,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးစန်းရေွှ",
        "en": "U SAN SHWE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 35746,
    "acclaim": "N",
    "ethnicity": "Shan",
    "parliament_code": "SAGAING-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005",
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်သိန်း",
        "en": "U NYAN THEIN"
      }
    }
  },
  {
    "votes": 9114,
    "candidate": {
      "name": {
        "my": "စိုင်းလုံးကျောက်",
        "en": "Sai Lon Kyaunt"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016001-1"
  },
  {
    "votes": 20902,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလုံးဆိုင်",
        "en": "U SAI LON SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016001-1"
  },
  {
    "votes": 1909,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆမ်ရှဲန်",
        "en": "U SAI SUM SHEN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016001-1"
  },
  {
    "votes": 2737,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအိုက်",
        "en": "U SAI AIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016001-1"
  },
  {
    "votes": 6920,
    "candidate": {
      "name": {
        "my": "စိုင်းသောင်းစိန်",
        "en": "Sai Thaung Sein"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016001-2"
  },
  {
    "votes": 23609,
    "candidate": {
      "name": {
        "my": "ဦးအားဖါ",
        "en": "U ARPHAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016001-2"
  },
  {
    "votes": 901,
    "candidate": {
      "name": {
        "my": "ဦးနန်ယီ",
        "en": "U NAN YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016001-2"
  },
  {
    "votes": 3556,
    "candidate": {
      "name": {
        "my": "ဦးအွန်ခမ်း",
        "en": "U ORN KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016001-2"
  },
  {
    "votes": 1354,
    "candidate": {
      "name": {
        "my": "ဦးကျာလော",
        "en": "U KYA LAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016001-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ထွန်း",
        "en": "U KHUN TUN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR016004-1"
  },
  {
    "votes": 1310,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်မွန်း",
        "en": "U AIK MOON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR016004-2"
  },
  {
    "votes": 2419,
    "candidate": {
      "name": {
        "my": "ဒေါ်မာရီ",
        "en": "DAW MAR YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016004-2"
  },
  {
    "votes": 84,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ကျောက်",
        "en": "U AIK KYAUK"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016010-1"
  },
  {
    "votes": 1849,
    "candidate": {
      "name": {
        "my": "ဦးနော်ဆိုင်း",
        "en": "U Naw Saing"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016010-1"
  },
  {
    "votes": 3002,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ဝင်း",
        "en": "U NYUNT WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016010-1"
  },
  {
    "votes": 210,
    "candidate": {
      "name": {
        "my": "ဦးကျိုင်းဆောင်",
        "en": "U KYAING SAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016010-1"
  },
  {
    "votes": 1213,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ဆုတ်",
        "en": "U Aik Sot"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016010-2"
  },
  {
    "votes": 4236,
    "candidate": {
      "name": {
        "my": "ဦးမောရှည်",
        "en": "U MAW SHAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016010-2"
  },
  {
    "votes": 375,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်တို့",
        "en": "U AIK TOTE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016010-2"
  },
  {
    "votes": 1261,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလုံ",
        "en": "U SAI LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016006-1"
  },
  {
    "votes": 8458,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းရေွှ",
        "en": "U SAI HTUN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016006-1"
  },
  {
    "votes": 907,
    "candidate": {
      "name": {
        "my": "ဦးကျာရှီး",
        "en": "U KYAR SHI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016006-1"
  },
  {
    "votes": 7863,
    "candidate": {
      "name": {
        "my": "ဦးယောဘ",
        "en": "U YAW BA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016006-2"
  },
  {
    "votes": 1215,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဝါလိန်",
        "en": "U SAI WA LEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016006-2"
  },
  {
    "votes": 526,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်ဌေး",
        "en": "U SAI KYAW HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016006-2"
  },
  {
    "votes": 2634,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမုန့်",
        "en": "U SAI HMONT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016002-1"
  },
  {
    "votes": 234,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်အေး",
        "en": "U KHIN MAUNG AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016002-1"
  },
  {
    "votes": 688,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတင်ဖေ",
        "en": "U SAI TIN PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016002-1"
  },
  {
    "votes": 2149,
    "candidate": {
      "name": {
        "my": "ဦးယာကုပ်",
        "en": "U YA KOTE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016002-2"
  },
  {
    "votes": 650,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ရီနွံ",
        "en": "U AIK YI NUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016002-2"
  },
  {
    "votes": 3853,
    "candidate": {
      "name": {
        "my": "စိုင်းကောလိန္ဒ",
        "en": "Sai Kaw LanDa"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016007-1"
  },
  {
    "votes": 7089,
    "candidate": {
      "name": {
        "my": "ဦးအီးရှာမေွ",
        "en": "U EI SHAR MWAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016007-1"
  },
  {
    "votes": 3262,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဇော်",
        "en": "U THAN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016007-1"
  },
  {
    "votes": 3064,
    "candidate": {
      "name": {
        "my": "စိုင်းယီတစ်",
        "en": "Sai Ye Tit"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016007-2"
  },
  {
    "votes": 8763,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှ၀င်း",
        "en": "U SAI HLA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016007-2"
  },
  {
    "votes": 6764,
    "candidate": {
      "name": {
        "my": "ဦးစောမူးလာ",
        "en": "U SAW MU LAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016008-1"
  },
  {
    "votes": 1189,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းပန်ဖ",
        "en": "U SAI PAN PHA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016008-1"
  },
  {
    "votes": 224,
    "candidate": {
      "name": {
        "my": "ဦးအေးမောင်",
        "en": "U AYE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016008-1"
  },
  {
    "votes": 1889,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ခမ်း",
        "en": "U SAI AUNG KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016008-2"
  },
  {
    "votes": 2051,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဝင်းမြတ်ဦး",
        "en": "U SAI WIN MYAT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016008-2"
  },
  {
    "votes": 1169,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းကိန်ခမ်း",
        "en": "DAW NAN KEIN KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016003-1"
  },
  {
    "votes": 861,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းနွံ",
        "en": "U SAI NUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016003-1"
  },
  {
    "votes": 1700,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆမ်",
        "en": "U SAI SAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016003-2"
  },
  {
    "votes": 211,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရှဲန်ဝမ်း",
        "en": "U SAI SHEN WON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016003-2"
  },
  {
    "votes": 8421,
    "candidate": {
      "name": {
        "my": "စိုင်းအင်",
        "en": "Sai Inn"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016011-1"
  },
  {
    "votes": 4460,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစိုးမြင့်",
        "en": "U SAI SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016011-1"
  },
  {
    "votes": 1820,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်မောင်",
        "en": "U MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016011-1"
  },
  {
    "votes": 1298,
    "candidate": {
      "name": {
        "my": "နန်းမြမြလွင်",
        "en": "Nan Mya Mya Lwin"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016011-2"
  },
  {
    "votes": 639,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းလူး",
        "en": "U SAI KHAM LOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016011-2"
  },
  {
    "votes": 116,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းပင်း",
        "en": "U SAI PING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016011-2"
  },
  {
    "votes": 13888,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်မေထွန်း",
        "en": "DAW TIN MAY TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016009-1"
  },
  {
    "votes": 5056,
    "candidate": {
      "name": {
        "my": "ဦးဘထွန်း",
        "en": "U BA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016009-1"
  },
  {
    "votes": 7003,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအေး",
        "en": "U SAI AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016009-1"
  },
  {
    "votes": 643,
    "candidate": {
      "name": {
        "my": "ဦးအာဗရှာလုံ",
        "en": "U ARBASHA LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016009-1"
  },
  {
    "votes": 10864,
    "candidate": {
      "name": {
        "my": "ဦးတူးမောင်",
        "en": "U TU MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016009-2"
  },
  {
    "votes": 2601,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016009-2"
  },
  {
    "votes": 8778,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဌေးလွင်စိုး",
        "en": "U SAI HTAY LWIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016009-2"
  },
  {
    "votes": 2154,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ဆံခမ်း(ခ)ဂျွန်လီ",
        "en": "U AIK SAM KHAM (a) JOHN LEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016009-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ထွန်းလူ(ခ)ဦးထွန်းလူ",
        "en": "U KHUN TUN LU (a) U TUN LU"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015021-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှဖေ",
        "en": "U SAI HLA PE"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015021-2"
  },
  {
    "votes": 5706,
    "candidate": {
      "name": {
        "my": "စိုင်းဝင်းမြင့်",
        "en": "Sai Win Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015002-1"
  },
  {
    "votes": 7131,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစံအေး",
        "en": "U SAI SAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015002-1"
  },
  {
    "votes": 395,
    "candidate": {
      "name": {
        "my": "ဒေါ်ပြုံး",
        "en": "DAW PYONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015002-1"
  },
  {
    "votes": 4948,
    "candidate": {
      "name": {
        "my": "စိုင်းအိုက်ပေါင်း",
        "en": "Sai Aik Paung"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015002-2"
  },
  {
    "votes": 10968,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစံတင့်",
        "en": "U SAI SAN TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015014-1"
  },
  {
    "votes": 7516,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရေွှသိန်း",
        "en": "U SAI SHWE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015014-2"
  },
  {
    "votes": 21591,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစိန်ဝင်း",
        "en": "U SAI SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015014-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဝေရေှာက်ရင်",
        "en": "U WAI SHAUK YIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015023-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျောက်တယ်ချန်",
        "en": "U KYAUK TAE CHAN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015023-2"
  },
  {
    "votes": 10081,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမျိုးထွန်း",
        "en": "DR. MYO TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015020-1"
  },
  {
    "votes": 2591,
    "candidate": {
      "name": {
        "my": "ဦးဆိုင်ခွက်",
        "en": "U SAING KHWET"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015020-1"
  },
  {
    "votes": 5299,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရင်(ခ)လောက်ဝူး",
        "en": "U TUN YIN (a) LAO WU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015020-2"
  },
  {
    "votes": 2987,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ဇယ်(ခ)ဦးခွန်ကြွယ်",
        "en": "U KHUN ZAE (a) U KHUN KHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015020-2"
  },
  {
    "votes": 1673,
    "candidate": {
      "name": {
        "my": "စိုင်းဖေအေး",
        "en": "Sai Pae Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015011-1"
  },
  {
    "votes": 14341,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆာလူး",
        "en": "U SAI SAE LU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015011-1"
  },
  {
    "votes": 4723,
    "candidate": {
      "name": {
        "my": "ဦးဘရန်နန်",
        "en": "U BRENG NAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015011-1"
  },
  {
    "votes": 10858,
    "candidate": {
      "name": {
        "my": "ဦးကြာထွန်း",
        "en": "U KYAR TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015011-1"
  },
  {
    "votes": 2638,
    "candidate": {
      "name": {
        "my": "စိုင်းအိုက်ပူး",
        "en": "Sai Aik Pu"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015011-2"
  },
  {
    "votes": 24938,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လွင်(ခ)ဦးဝမ်ကွယ်တာ",
        "en": "U MYINT LWIN (a) U WON KWAY TAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015011-2"
  },
  {
    "votes": 6814,
    "candidate": {
      "name": {
        "my": "ဦးပျည်းလိမ်ကြာ",
        "en": "U PYI LEIN KYAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015011-2"
  },
  {
    "votes": 11387,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ကြည်",
        "en": "U SAI AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015012-1"
  },
  {
    "votes": 22418,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းကျော်",
        "en": "U SAI KHAM KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015012-1"
  },
  {
    "votes": 8530,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015012-2"
  },
  {
    "votes": 17837,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသန်းမောင်",
        "en": "U SAI THAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015012-2"
  },
  {
    "votes": 12969,
    "candidate": {
      "name": {
        "my": "ဦးလှကျော်",
        "en": "U HLA KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015012-2"
  },
  {
    "votes": 15161,
    "candidate": {
      "name": {
        "my": "စိုင်းစံမင်း",
        "en": "Sai San Min"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015001-1"
  },
  {
    "votes": 20926,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကြူ",
        "en": "U SAI KYU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015001-1"
  },
  {
    "votes": 3702,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိန်",
        "en": "U THAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015001-1"
  },
  {
    "votes": 7909,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လိှုင်",
        "en": "U AUNG HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015001-1"
  },
  {
    "votes": 13491,
    "candidate": {
      "name": {
        "my": "စိုင်းကျော်ဌေး",
        "en": "Sai Kyaw Htay"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015001-2"
  },
  {
    "votes": 22491,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းမှတ်",
        "en": "U SAI KHAM HMAT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015001-2"
  },
  {
    "votes": 4715,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရေွှထူး",
        "en": "U SAI SHWE HTOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015001-2"
  },
  {
    "votes": 7786,
    "candidate": {
      "name": {
        "my": "ဦးလောင်ဝူး",
        "en": "U LAU WOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015001-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးပယ်စောက်ချိန်(ခ)ဦးဘေဆော်",
        "en": "U PAE SAO CHEIN (a) U BAE SAW"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015022-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမြင်ရေှာ်ချန်း(ခ)ဦးမြီရေှာက်ချမ်း",
        "en": "U MYIN SHAW CHAN (a) U MYI SHAUK CHAN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015022-2"
  },
  {
    "votes": 5165,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015018-1"
  },
  {
    "votes": 4197,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဆေွ",
        "en": "U MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015018-1"
  },
  {
    "votes": 6341,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်ဦး",
        "en": "U THANT ZIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015018-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမိုင်းအုဏ်းခိုင်",
        "en": "U MAING OHN KHAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015019-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ခ",
        "en": "U AIK KHA"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015019-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးလုပ်ပရေွး(ခ)အောင်မင်းစိန်",
        "en": "U LOTE PA YWE(a) AUNG MIN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015008-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျက်အောင်ခွန်းဆမ်(ခ)ဦးကျစ်",
        "en": "U KYET AUNG KHOON SAM (a) U KYIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015008-2"
  },
  {
    "votes": 5404,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "Sai Soe Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015017-1"
  },
  {
    "votes": 9498,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်ဝင်း",
        "en": "U KHIN ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015017-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစိုးမြင့်",
        "en": "U SAI SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015017-1"
  },
  {
    "votes": 6631,
    "candidate": {
      "name": {
        "my": "စိုင်းဆန်ကွယ်",
        "en": "Sai San Kwal"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015017-2"
  },
  {
    "votes": 3018,
    "candidate": {
      "name": {
        "my": "ဦးတိုးမောင်",
        "en": "U TOE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015017-2"
  },
  {
    "votes": 2346,
    "candidate": {
      "name": {
        "my": "ဦးလှဖေ",
        "en": "U HLA PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015017-2"
  },
  {
    "votes": 5228,
    "candidate": {
      "name": {
        "my": "စိုင်းစံဖေး",
        "en": "Sai San Pae"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015003-1"
  },
  {
    "votes": 2872,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဝင်းခိုင်",
        "en": "U SAI WIN KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015003-1"
  },
  {
    "votes": 4594,
    "candidate": {
      "name": {
        "my": "စိုင်းလောင်(၀်)ခမ်း",
        "en": "Sai Lao Kham"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015003-2"
  },
  {
    "votes": 2032,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းကော်",
        "en": "U SAI KHAM KAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015003-2"
  },
  {
    "votes": 17242,
    "candidate": {
      "name": {
        "my": "နန်းငေွငေွ",
        "en": "Nan Ngwe Ngwe"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015009-1"
  },
  {
    "votes": 5462,
    "candidate": {
      "name": {
        "my": "ဦးစံပူး",
        "en": "U SAN POO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015009-1"
  },
  {
    "votes": 2222,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရွက်ဆိုင်",
        "en": "U SAI YWET SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015009-1"
  },
  {
    "votes": 13761,
    "candidate": {
      "name": {
        "my": "ဦးခန်မိုင်",
        "en": "U KENGMAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015009-2"
  },
  {
    "votes": 5186,
    "candidate": {
      "name": {
        "my": "စိုင်းအောင်မြင့်",
        "en": "Sai Aung Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015009-2"
  },
  {
    "votes": 5588,
    "candidate": {
      "name": {
        "my": "ဦးလွမ်းခေါင်",
        "en": "U LWANN KHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015009-2"
  },
  {
    "votes": 931,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသန်းရှိန်(ခ)စိုင်းထွန်းအောင်",
        "en": "U SAI THAN SHEIN (a) SAI TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR015009-2"
  },
  {
    "votes": 5074,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းစင်",
        "en": "SAI KAM SIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015016-1"
  },
  {
    "votes": 8875,
    "candidate": {
      "name": {
        "my": "စိုင်းလိတ်",
        "en": "SAI LEIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015016-1"
  },
  {
    "votes": 3220,
    "candidate": {
      "name": {
        "my": "ဦးဆရာယိုင်း",
        "en": "U SAYAR YAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015016-1"
  },
  {
    "votes": 1938,
    "candidate": {
      "name": {
        "my": "ဦးခွန်မောင်လွန်း",
        "en": "U KUN MAUNG LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015016-2"
  },
  {
    "votes": 5657,
    "candidate": {
      "name": {
        "my": "ဦးခွန်အေးကျော်",
        "en": "KUN AYE KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015016-2"
  },
  {
    "votes": 6385,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသူရိန်ဦး",
        "en": "SAI THURAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015016-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းပန်းအောင်(ခ)ဦးပုည",
        "en": "U SAI PANN AUNG (a) U PONNYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015006-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလူ",
        "en": "U SAI LU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015006-2"
  },
  {
    "votes": 3029,
    "candidate": {
      "name": {
        "my": "စိုင်းဘဉာဏ်",
        "en": "Sai Ba Nyan"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015015-1"
  },
  {
    "votes": 5841,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဦး",
        "en": "U ZAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015015-1"
  },
  {
    "votes": 6994,
    "candidate": {
      "name": {
        "my": "နန်းခမ်းအေး",
        "en": "Nan Kham Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015015-2"
  },
  {
    "votes": 3191,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆိုင်မိန်း(ခ)ဦးစိုးဝင်းဖေ",
        "en": "U SAI SAING MEIN (a) U SOE WIN PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015015-2"
  },
  {
    "votes": 3604,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းမောင်",
        "en": "U SAI TUN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015010-1"
  },
  {
    "votes": 12080,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတင်ဦး",
        "en": "U SAI TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015010-1"
  },
  {
    "votes": 8808,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကျော်",
        "en": "U TUN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015010-1"
  },
  {
    "votes": 9775,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစောလှ",
        "en": "U SAI SAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015010-2"
  },
  {
    "votes": 6857,
    "candidate": {
      "name": {
        "my": "ဦးညီစိန်",
        "en": "U NYI SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015010-2"
  },
  {
    "votes": 10765,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015013-1"
  },
  {
    "votes": 15446,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြ",
        "en": "U KYAW MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015013-1"
  },
  {
    "votes": 3322,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတင့်ချို",
        "en": "U SAI TINT CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015013-1"
  },
  {
    "votes": 9350,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015013-2"
  },
  {
    "votes": 6861,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဘထွန်း",
        "en": "U SAI BA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015013-2"
  },
  {
    "votes": 12991,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဘထွန်း",
        "en": "U SAI BA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015013-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးရန်ကျော်",
        "en": "U YAN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015005-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်းလတ်(ခ)ဦးညီလောင်",
        "en": "U KYAW MIN LATT (a)NYI NYI LAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015005-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ခွန်း",
        "en": "U AIK KHOON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015007-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျက်အောင်",
        "en": "U KYET AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015007-2"
  },
  {
    "votes": 8813,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆန်စိန်",
        "en": "U SAII SAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015004-1"
  },
  {
    "votes": 7204,
    "candidate": {
      "name": {
        "my": "စိုင်းမောင်ပွင့်(ခ)စိုင်းအောင်ပွင့်",
        "en": "U SAI MAUNG PWINT (a) SAI AUNG PWINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015004-1"
  },
  {
    "votes": 2029,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်တောင်း",
        "en": "U AIK TAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015004-1"
  },
  {
    "votes": 4862,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လိှုင်",
        "en": "U KYAW HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015004-2"
  },
  {
    "votes": 8075,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းထွန်းလွင်",
        "en": "U SAI TUN TUN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015004-2"
  },
  {
    "votes": 799,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမောင်စိန်",
        "en": "U SAI MAUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015004-2"
  },
  {
    "votes": 1257,
    "candidate": {
      "name": {
        "my": "ဦးကျတော့",
        "en": "U KYA TORT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR015004-2"
  },
  {
    "votes": 8673,
    "candidate": {
      "name": {
        "my": "ဒေါ်သန်းသန်းရေွှ",
        "en": "DAW THAN THAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014003-1"
  },
  {
    "votes": 19705,
    "candidate": {
      "name": {
        "my": "ဦးခွန်အေးမောင်",
        "en": "U KHUN AYE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014003-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစံလွင်",
        "en": "U SAN LWIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014003-2"
  },
  {
    "votes": 8266,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတင်ရေွှ",
        "en": "U SAI TIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014004-1"
  },
  {
    "votes": 26420,
    "candidate": {
      "name": {
        "my": "ဦးခွန်လှသိန်း",
        "en": "U KHUN HLA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014004-1"
  },
  {
    "votes": 7767,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခေါင်",
        "en": "U SAI KHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014004-2"
  },
  {
    "votes": 26760,
    "candidate": {
      "name": {
        "my": "ဦးခွန်မောင်ပယ်",
        "en": "U KHUN MAUNG PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014004-2"
  },
  {
    "votes": 25615,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": "U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014005-1"
  },
  {
    "votes": 15420,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးသန်းဦး(ခ)ဦးဂျော့",
        "en": "U MYO THAN OO (a) U GEORGE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014005-1"
  },
  {
    "votes": 24980,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်စိုး",
        "en": "U MYAT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014005-2"
  },
  {
    "votes": 14788,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ချစ်",
        "en": "U AUNG CHIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014005-2"
  },
  {
    "votes": 4378,
    "candidate": {
      "name": {
        "my": "စိုင်းမွန်း",
        "en": "Sai Mon"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014014-1"
  },
  {
    "votes": 2033,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုင်းခမ်းလိှု်င်",
        "en": "DR. SAI KHAM HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014014-1"
  },
  {
    "votes": 3761,
    "candidate": {
      "name": {
        "my": "စိုင်းထွန်းကြည့်",
        "en": "Sai Htun Kyi"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014014-2"
  },
  {
    "votes": 3768,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းနု",
        "en": "U SAI NU"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014014-2"
  },
  {
    "votes": 1176,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရေွှမှုံ",
        "en": "U SAI SHWE HMON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014015-1"
  },
  {
    "votes": 8386,
    "candidate": {
      "name": {
        "my": "ဦးဆန်လိှုင်း",
        "en": "U SANN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014015-1"
  },
  {
    "votes": 3045,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ကျော်ခင်",
        "en": "U SAI AUNG KYAW KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014015-2"
  },
  {
    "votes": 4533,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမွမ်းလိှုင်း",
        "en": "U SAI MOHN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014015-2"
  },
  {
    "votes": 4717,
    "candidate": {
      "name": {
        "my": "စိုင်းကျော်ဇေယျ",
        "en": "Sai Kyaw Zay Ya"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014012-1"
  },
  {
    "votes": 1212,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဆေွ",
        "en": "U CHIT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014012-1"
  },
  {
    "votes": 1224,
    "candidate": {
      "name": {
        "my": "ဦးလပန်း",
        "en": "U LA PANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014012-1"
  },
  {
    "votes": 5207,
    "candidate": {
      "name": {
        "my": "နန်းအက်စ်ကီးမိုး",
        "en": "Nan Eskimo"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014012-2"
  },
  {
    "votes": 1164,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလုံ",
        "en": "U SAI LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014012-2"
  },
  {
    "votes": 349,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတွီ",
        "en": "U SAI TWI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014012-2"
  },
  {
    "votes": 2837,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်ဌေး",
        "en": "U SAI KYAW HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014018-1"
  },
  {
    "votes": 5324,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအိုက်အေး",
        "en": "U SAI AIK AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014018-1"
  },
  {
    "votes": 4089,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်လှ",
        "en": "U SAI KYAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014018-2"
  },
  {
    "votes": 4193,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလွင်",
        "en": "U SOE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014018-2"
  },
  {
    "votes": 25500,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ညွန့်",
        "en": "U AUNG KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014008-1"
  },
  {
    "votes": 9937,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဖေ",
        "en": "U THAN PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014008-1"
  },
  {
    "votes": 20697,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုးနိုင်",
        "en": "U MYINT SOE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014008-2"
  },
  {
    "votes": 7539,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းနွယ်",
        "en": "U THEIN NWAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014008-2"
  },
  {
    "votes": 10255,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ဒီဒေါ်",
        "en": "U KHUN DEE DAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014011-1"
  },
  {
    "votes": 2125,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညွန့်",
        "en": "U KHIN MAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014011-1"
  },
  {
    "votes": 5623,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမျိုးမြင့်",
        "en": "U SAI MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014011-1"
  },
  {
    "votes": 7433,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဘဦး",
        "en": "U SAI BA OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014011-2"
  },
  {
    "votes": 4867,
    "candidate": {
      "name": {
        "my": "ဦးခွန်မြင့်ခွင်",
        "en": "U KHUN MYINT KHWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014011-2"
  },
  {
    "votes": 3530,
    "candidate": {
      "name": {
        "my": "စိုင်းစိုးထိုက်",
        "en": "Sai Soe Htike"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014020-1"
  },
  {
    "votes": 1324,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဦးဆိုင်",
        "en": "U SAI OO SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014020-1"
  },
  {
    "votes": 723,
    "candidate": {
      "name": {
        "my": "ခွန်အောင်စိုး",
        "en": "Khun aung Soe"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014020-2"
  },
  {
    "votes": 3971,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ဟန်သိန်းဦး",
        "en": "U KHUN HAN THEIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014020-2"
  },
  {
    "votes": 5405,
    "candidate": {
      "name": {
        "my": "ိစိုင်းထွန်းမြင့်",
        "en": "Sai Htun Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014017-1"
  },
  {
    "votes": 5235,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမင်းအောင်",
        "en": "U MYO MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014017-1"
  },
  {
    "votes": 2175,
    "candidate": {
      "name": {
        "my": "ိစိုင်းမျိုးထွန်းလွင် (ခ) စိုင်းလင်းခမ်း",
        "en": "Sai Myo Htun Lwin (a) Sai Lynn Kam"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014017-2"
  },
  {
    "votes": 3373,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကံ",
        "en": "U SAI KAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014017-2"
  },
  {
    "votes": 5080,
    "candidate": {
      "name": {
        "my": "စိုင်းပန်",
        "en": "?Sai Pan"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014016-1"
  },
  {
    "votes": 1140,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမီ",
        "en": "U SAI MI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014016-1"
  },
  {
    "votes": 2318,
    "candidate": {
      "name": {
        "my": "နန်းခမ်းပိုင်း",
        "en": "Nan Kham Pai"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014016-2"
  },
  {
    "votes": 1049,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသိန်းအောင်",
        "en": "U SAI THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014016-2"
  },
  {
    "votes": 1766,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းလေး",
        "en": "DAW NAN LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014019-1"
  },
  {
    "votes": 4637,
    "candidate": {
      "name": {
        "my": "ဦးစောလုံ",
        "en": "U SAW LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014019-1"
  },
  {
    "votes": 2292,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဆန်းအေး",
        "en": "U SAI SAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014019-2"
  },
  {
    "votes": 3711,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမောင်ကြီး",
        "en": "U SAI MAUNG GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014019-2"
  },
  {
    "votes": 2294,
    "candidate": {
      "name": {
        "my": "စိုင်းမိုးကြည်",
        "en": "Sai Moe Kyi"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014021-1"
  },
  {
    "votes": 1479,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလောင်ဖ",
        "en": "U SAI LAO PHA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014021-1"
  },
  {
    "votes": 1662,
    "candidate": {
      "name": {
        "my": "စိုင်းလှအောင်",
        "en": "Sai Hla Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014021-2"
  },
  {
    "votes": 1833,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ခမ်း",
        "en": "U SAI AUNG KHAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014021-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်(ခ)ဦးထွန်းကျော်",
        "en": "U MAUNG TUN KYAW (a) U TUN KYAW"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR014013-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွဏ်း",
        "en": "U AUNG TUN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR014013-2"
  },
  {
    "votes": 13090,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014002-1"
  },
  {
    "votes": 3631,
    "candidate": {
      "name": {
        "my": "ဦးသောင်း",
        "en": "U THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014002-1"
  },
  {
    "votes": 27733,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာထွန်းအောင်",
        "en": "DR. TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "INDP",
    "constituency": "MMR014002-1"
  },
  {
    "votes": 15364,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်(ခ)ဥိးကိုနီ",
        "en": "U AUNG MYINT (a) U KO NI"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014002-2"
  },
  {
    "votes": 3844,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014002-2"
  },
  {
    "votes": 5358,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှမောင်",
        "en": "U SAI HLA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014002-2"
  },
  {
    "votes": 21678,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြည်ဝင်း",
        "en": "U AUNG KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "INDP",
    "constituency": "MMR014002-2"
  },
  {
    "votes": 9056,
    "candidate": {
      "name": {
        "my": "ဒေါ်အေးချိုစိန်",
        "en": "DAW AYE CHO SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014010-1"
  },
  {
    "votes": 4217,
    "candidate": {
      "name": {
        "my": "ဦးခူးပေါင်း",
        "en": "U KHU PAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014010-1"
  },
  {
    "votes": 4870,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်သန်း",
        "en": "U MAUNG KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR014010-1"
  },
  {
    "votes": 6054,
    "candidate": {
      "name": {
        "my": "ဦးပီယို့တင်််ထွန်း",
        "en": "U P.ORR TIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014010-2"
  },
  {
    "votes": 3496,
    "candidate": {
      "name": {
        "my": "ဦးသောမငိုက်",
        "en": "U THAWMA NGAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014010-2"
  },
  {
    "votes": 8214,
    "candidate": {
      "name": {
        "my": "ဦးအာမြာဟန်",
        "en": "U ARMYA HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR014010-2"
  },
  {
    "votes": 13965,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြတ်",
        "en": "U AUNG MYAT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014006-1"
  },
  {
    "votes": 5855,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအေး",
        "en": "U TUN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014006-1"
  },
  {
    "votes": 11756,
    "candidate": {
      "name": {
        "my": "ဦးထူးကိုကို",
        "en": "U HTOO KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014006-2"
  },
  {
    "votes": 6264,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ပွ",
        "en": "U AUNG PWA"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014006-2"
  },
  {
    "votes": 9379,
    "candidate": {
      "name": {
        "my": "ဦးပွန်း",
        "en": "U POON"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014009-1"
  },
  {
    "votes": 41270,
    "candidate": {
      "name": {
        "my": "ဦးခွန်လှဆန်း",
        "en": "U KHUN HLA SANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014009-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခွန်အောင်နိုင်ဦး",
        "en": "U KHUN AUNG NAING OO"
      }
    },
    "acclaim": "Y",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014009-2"
  },
  {
    "votes": 45696,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းရင်",
        "en": "U SAI TUN YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014001-1"
  },
  {
    "votes": 22224,
    "candidate": {
      "name": {
        "my": "ဦးဘသန်း",
        "en": "U BA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014001-1"
  },
  {
    "votes": 9365,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှရဲထွဋ်",
        "en": "U SAI HLA YE HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014001-1"
  },
  {
    "votes": 5573,
    "candidate": {
      "name": {
        "my": "ဦးသန်းရေွှ",
        "en": "U THAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014001-1"
  },
  {
    "votes": 55138,
    "candidate": {
      "name": {
        "my": "ဦးခွန်သိန်းမောင်",
        "en": "U KHUN THEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014001-2"
  },
  {
    "votes": 11436,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်နောင်",
        "en": "U MYO MYINT NAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014001-2"
  },
  {
    "votes": 14805,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစံညွန့်",
        "en": "U SAI SAN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014001-2"
  },
  {
    "votes": 5102,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဦးအေးဖေ",
        "en": "DR. U AYE PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014007-1"
  },
  {
    "votes": 962,
    "candidate": {
      "name": {
        "my": "ဦးရဲအောင်",
        "en": "U YE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014007-1"
  },
  {
    "votes": 12019,
    "candidate": {
      "name": {
        "my": "ဦးကိုကို",
        "en": "U KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014007-1"
  },
  {
    "votes": 4285,
    "candidate": {
      "name": {
        "my": "ဦးမိုးထွန်း",
        "en": "U MOE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014007-2"
  },
  {
    "votes": 1880,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးအောင်",
        "en": "U MYO AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014007-2"
  },
  {
    "votes": 12150,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014007-2"
  },
  {
    "ethnic_seat": true,
    "votes": 18189,
    "acclaim": "N",
    "ethnicity": "Aka",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးပီတာသောင်းစိန်",
        "en": "U PETER THAUNG SEIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 9226,
    "acclaim": "N",
    "ethnicity": "Aka",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဥိးအားလယ်",
        "en": "U AR LAE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 229296,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ဝင်း",
        "en": "U NAING WIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 60663,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးညွန့်မောင်",
        "en": "U NYUNT MAUNG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 34004,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်းထွန်း",
        "en": "U ZAW WIN TUN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 19949,
    "acclaim": "N",
    "ethnicity": "Burman",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဝီလှျံခွန်ကြည်",
        "en": "U WILLIAM KHUN KYI"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 12990,
    "acclaim": "N",
    "ethnicity": "Inn Tha",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဘသန်း",
        "en": "U BA THAN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 5388,
    "acclaim": "N",
    "ethnicity": "Inn Tha",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးသာအောင်",
        "en": "U THAR AUNG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 53605,
    "acclaim": "N",
    "ethnicity": "Inn Tha",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "INDP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 22164,
    "acclaim": "N",
    "ethnicity": "Kachin",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဒူဝါးဖုတ်ဒေါင်",
        "en": "U DUWA PHOAT DAUNG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 18188,
    "acclaim": "N",
    "ethnicity": "Kachin",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးခေါဘောင်း",
        "en": "U KHAW BAWMG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 11850,
    "acclaim": "N",
    "ethnicity": "Kayan (a) Padaung",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဥိးထွန်းဟန်",
        "en": "U TUN HAN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 6107,
    "acclaim": "N",
    "ethnicity": "Kayan (a) Padaung",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးတိုးပေါလ်",
        "en": "U TOE PAUL"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 13379,
    "acclaim": "N",
    "ethnicity": "Kayan (a) Padaung",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးလောရင်း",
        "en": "U LAWRENCE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 36599,
    "acclaim": "N",
    "ethnicity": "Lahu",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးရှာမေွလရှဲန",
        "en": "U SHAR MWAY LA SHEIN"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 20517,
    "acclaim": "N",
    "ethnicity": "Lahu",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဒယ်နီယာထီး",
        "en": "U DANIEL YAHTEE"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 8421,
    "acclaim": "N",
    "ethnicity": "Lisu",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဝုန်ဆန်(ခ)ဦးယောဝိ",
        "en": "U WONE SAN (a) U YAWI"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 3480,
    "acclaim": "N",
    "ethnicity": "Lisu",
    "parliament_code": "SHAN-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014",
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဖန်(ခ)လီလေး",
        "en": "U ZAW PHAN (a) LI LAY"
      }
    }
  },
  {
    "votes": 10150,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအေး",
        "en": "U TUN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006010-1"
  },
  {
    "votes": 4625,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဖေ",
        "en": "U THAN PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006010-1"
  },
  {
    "votes": 5765,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဉာဏ်",
        "en": "U THAIN NYAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006010-2"
  },
  {
    "votes": 10820,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ကျော်ဦး",
        "en": "U AUNG KYAW KYAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006010-2"
  },
  {
    "votes": 12302,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006001-1"
  },
  {
    "votes": 11804,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး(ခ)ဦးသန်းအေး",
        "en": "U TIN OO (A) U THAN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006001-1"
  },
  {
    "votes": 17605,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်ကို",
        "en": "U MYAUNT KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006001-2"
  },
  {
    "votes": 12371,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလွင်",
        "en": "U TUN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006001-2"
  },
  {
    "votes": 11647,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006009-1"
  },
  {
    "votes": 8305,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006009-1"
  },
  {
    "votes": 17573,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းအောင်",
        "en": "DR. WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006009-2"
  },
  {
    "votes": 6914,
    "candidate": {
      "name": {
        "my": "ဦးလှရေွှ",
        "en": "U HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006009-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006006-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "Y",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006006-2"
  },
  {
    "votes": 21742,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းအောင်",
        "en": "DR. WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006002-1"
  },
  {
    "votes": 10300,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သူရဝင်း",
        "en": "U AUNG THURA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006002-1"
  },
  {
    "votes": 15132,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ထေွး",
        "en": "U KYAW HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006002-2"
  },
  {
    "votes": 8533,
    "candidate": {
      "name": {
        "my": "ဦးထိန်လင်း",
        "en": "U HTEIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006002-2"
  },
  {
    "votes": 41775,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006005-1"
  },
  {
    "votes": 19283,
    "candidate": {
      "name": {
        "my": "ဦးကံထွန်း",
        "en": "U KAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006005-1"
  },
  {
    "votes": 28926,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်ဆန်း",
        "en": "DR. KYAW SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006005-2"
  },
  {
    "votes": 21407,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်နိုင်",
        "en": "U MAUNG MUANG NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006005-2"
  },
  {
    "votes": 763,
    "candidate": {
      "name": {
        "my": "ဦးစောငြိမ်းချမ်း",
        "en": "U SAW NYEIN CHAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR006007-1"
  },
  {
    "votes": 21858,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဆေွ",
        "en": "U WIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006007-1"
  },
  {
    "votes": 5825,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းညွန့်",
        "en": "U SAN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006007-1"
  },
  {
    "votes": 25174,
    "candidate": {
      "name": {
        "my": "ဦးထင်အောင်ကျော်",
        "en": "U HTIN AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006007-2"
  },
  {
    "votes": 3443,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဝင်း",
        "en": "U TIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006007-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်းလွင်",
        "en": "U AUNG SAN LWIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006008-1"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသက်နိုင်",
        "en": "U THET NAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006008-2"
  },
  {
    "votes": 14784,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဝင်း",
        "en": "U HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006003-1"
  },
  {
    "votes": 7554,
    "candidate": {
      "name": {
        "my": "ဦးငေွလွင်",
        "en": "U NGWE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006003-1"
  },
  {
    "votes": 10907,
    "candidate": {
      "name": {
        "my": "ဦးစိုးထေွး",
        "en": "U SOE HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006003-2"
  },
  {
    "votes": 9107,
    "candidate": {
      "name": {
        "my": "ဦးကြည်စိုး",
        "en": "U KYI SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006003-2"
  },
  {
    "votes": 10108,
    "candidate": {
      "name": {
        "my": "ဦးရေွှစန်း",
        "en": "U SHWE SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006004-1"
  },
  {
    "votes": 9694,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းသစ်",
        "en": "U TUN THIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006004-1"
  },
  {
    "votes": 7618,
    "candidate": {
      "name": {
        "my": "ှုဦးတင်မြ",
        "en": "U Tin Mya"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR006004-2"
  },
  {
    "votes": 7672,
    "candidate": {
      "name": {
        "my": "ဦးဖေသန်း",
        "en": "U PAE THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006004-2"
  },
  {
    "votes": 8104,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006004-2"
  },
  {
    "ethnic_seat": true,
    "votes": 23323,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006",
    "candidate": {
      "name": {
        "my": "ဦးစောဟာဗိ",
        "en": "SAW HARBI"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 15569,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "TANINTHARYI-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006",
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ကြွယ်",
        "en": "KHIN MAUNG KYAWL"
      }
    }
  },
  {
    "votes": 6254,
    "candidate": {
      "name": {
        "my": "ဦးမိုးဝင်းကျော်",
        "en": "U MOE WIN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013037-1"
  },
  {
    "votes": 2384,
    "candidate": {
      "name": {
        "my": "ဦးလှသောင်း",
        "en": "U HLA THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013037-1"
  },
  {
    "votes": 5838,
    "candidate": {
      "name": {
        "my": "ဦးကိုကို",
        "en": "U KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013037-1"
  },
  {
    "votes": 2988,
    "candidate": {
      "name": {
        "my": "ဦးဟုတ်စွမ်မော်",
        "en": "U HOKE SWAN MAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013037-2"
  },
  {
    "votes": 4997,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သူရ",
        "en": "U KYAW THURA"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013037-2"
  },
  {
    "votes": 3606,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်းနိုင်",
        "en": "U KYAW WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013037-2"
  },
  {
    "votes": 10984,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ကြည်",
        "en": "U MYINT KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013044-1"
  },
  {
    "votes": 2485,
    "candidate": {
      "name": {
        "my": "ဒေါ်မြင့်မြင့် (ခ) ဒေါ်အုန်းမြင့်",
        "en": "DAW MYINT MYTIN (A) DAW OHN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013044-1"
  },
  {
    "votes": 8590,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာ ဘရေွှ",
        "en": "DR. BA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013044-1"
  },
  {
    "votes": 7965,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာညိုညိုသင်း",
        "en": "DR.NYO NYO THINN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013044-2"
  },
  {
    "votes": 7704,
    "candidate": {
      "name": {
        "my": "ဦးရဲအောင်",
        "en": "U YE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013044-2"
  },
  {
    "votes": 3032,
    "candidate": {
      "name": {
        "my": "ဦးသက်ဇင်ဌေး",
        "en": "U THET ZIN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013044-2"
  },
  {
    "votes": 3773,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ဝင်း",
        "en": "U MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013017-1"
  },
  {
    "votes": 7920,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဦး",
        "en": "U THEIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013017-1"
  },
  {
    "votes": 3450,
    "candidate": {
      "name": {
        "my": "ဦးတင်ညွန့်",
        "en": "U TIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013017-2"
  },
  {
    "votes": 4535,
    "candidate": {
      "name": {
        "my": "ဦးမင်းလွင်",
        "en": "U MIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013017-2"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": "U AUNG NAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013032-1"
  },
  {
    "votes": 425,
    "candidate": {
      "name": {
        "my": "ဦးကြည်",
        "en": "U KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013032-2"
  },
  {
    "votes": 348,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဦး",
        "en": "U TIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013032-2"
  },
  {
    "votes": 2336,
    "candidate": {
      "name": {
        "my": "ဒေါ်မေသန်းနွယ်",
        "en": "DAW MAY THAN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013043-1"
  },
  {
    "votes": 1708,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်း",
        "en": "U KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013043-1"
  },
  {
    "votes": 4824,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ထွန်းဦး",
        "en": "U NYAN TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013043-2"
  },
  {
    "votes": 1692,
    "candidate": {
      "name": {
        "my": "ဦးသာဝင်း",
        "en": "U THA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013043-2"
  },
  {
    "votes": 15860,
    "candidate": {
      "name": {
        "my": "ဦးစောတာကပေါ",
        "en": "SAW TAKABAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013020-1"
  },
  {
    "votes": 11513,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လိှုင်",
        "en": "U KYAW KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013020-1"
  },
  {
    "votes": 16349,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထူး (ခ) ဦးကိုကို",
        "en": "U KHIN MUANG HTOO (A) U KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013020-2"
  },
  {
    "votes": 11415,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013020-2"
  },
  {
    "votes": 18478,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်စိုး",
        "en": "U MYAT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013019-1"
  },
  {
    "votes": 20974,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလွင်",
        "en": "U TUN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013019-1"
  },
  {
    "votes": 16958,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သိန်း",
        "en": "U KYAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013019-2"
  },
  {
    "votes": 19877,
    "candidate": {
      "name": {
        "my": "ဦးဇော်လင်း",
        "en": "U ZAW LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013019-2"
  },
  {
    "votes": 9154,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းကျော်အောင်",
        "en": "U THEIN KYAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013021-1"
  },
  {
    "votes": 4783,
    "candidate": {
      "name": {
        "my": "ဦးမန်ဆောင်ပေါင်",
        "en": "U MAN SAUNG PAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013021-1"
  },
  {
    "votes": 13199,
    "candidate": {
      "name": {
        "my": "ဦးတင်လိှုင်",
        "en": "U TIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013021-2"
  },
  {
    "votes": 8135,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဆေွ",
        "en": "U CHIT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013021-2"
  },
  {
    "votes": 40460,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013018-1"
  },
  {
    "votes": 22749,
    "candidate": {
      "name": {
        "my": "ဒေါ်ယဉ်မေ",
        "en": "DAW YIN MAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013018-1"
  },
  {
    "votes": 25619,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရေွှ",
        "en": "U TUN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013018-2"
  },
  {
    "votes": 37624,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန်းစန်းဝင်း",
        "en": "DAW SAN SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013018-2"
  },
  {
    "votes": 8004,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းချို",
        "en": "U Win Kyo"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013030-1"
  },
  {
    "votes": 6656,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်",
        "en": "U THANT ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013030-1"
  },
  {
    "votes": 15870,
    "candidate": {
      "name": {
        "my": "ဦးသန်းလိှုင်",
        "en": "U THAN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013030-1"
  },
  {
    "votes": 9044,
    "candidate": {
      "name": {
        "my": "ဦးထင်ပေါ်",
        "en": "U TIN PAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013030-2"
  },
  {
    "votes": 22403,
    "candidate": {
      "name": {
        "my": "ဦးဌေးရှိန်",
        "en": "U HTAY SHAINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013030-2"
  },
  {
    "votes": 7561,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းစိန်",
        "en": "U THAUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013014-1"
  },
  {
    "votes": 7163,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်အောင်",
        "en": "U NAING AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013014-1"
  },
  {
    "votes": 11271,
    "candidate": {
      "name": {
        "my": "ဦးဝေဇင်",
        "en": "U WAI ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013014-2"
  },
  {
    "votes": 7411,
    "candidate": {
      "name": {
        "my": "ဦးအေးကြူ",
        "en": "U AYE KYAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013014-2"
  },
  {
    "votes": 16311,
    "candidate": {
      "name": {
        "my": "ဦးမင်းလွင်",
        "en": "U MIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013040-1"
  },
  {
    "votes": 12935,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မိုး",
        "en": "U ZAW MOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013040-1"
  },
  {
    "votes": 14512,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းနိုင်",
        "en": "U THEIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013040-2"
  },
  {
    "votes": 13538,
    "candidate": {
      "name": {
        "my": "ဦးရဲမင်းထွန်း",
        "en": "U YEG MIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013040-2"
  },
  {
    "votes": 30000,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မျိုး",
        "en": "U KYAW MYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013008-1"
  },
  {
    "votes": 5255,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်မောင်",
        "en": "U NYUNT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR013008-1"
  },
  {
    "votes": 15650,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013008-1"
  },
  {
    "votes": 26912,
    "candidate": {
      "name": {
        "my": "ဦးကျော်တင့်",
        "en": "U KYAW TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013008-1"
  },
  {
    "votes": 43208,
    "candidate": {
      "name": {
        "my": "ဦးနေလင်း",
        "en": "U NAY LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013008-2"
  },
  {
    "votes": 37235,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013008-2"
  },
  {
    "votes": 11221,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းနိုင်",
        "en": "U THEIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013004-1"
  },
  {
    "votes": 41035,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ဝင်း",
        "en": "U NAING WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013004-1"
  },
  {
    "votes": 10129,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013004-2"
  },
  {
    "votes": 40997,
    "candidate": {
      "name": {
        "my": "ဦးလှသန်း",
        "en": "U HLA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013004-2"
  },
  {
    "votes": 6032,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမိုးဦး",
        "en": "U SOE MOE OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013003-1"
  },
  {
    "votes": 3048,
    "candidate": {
      "name": {
        "my": "ဦးစောမောင်တုတ်",
        "en": "SAW MAUNG TOT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013003-1"
  },
  {
    "votes": 6735,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013003-1"
  },
  {
    "votes": 24340,
    "candidate": {
      "name": {
        "my": "ဦးဗိုလ်ကြည်",
        "en": "U BO KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013003-1"
  },
  {
    "votes": 4604,
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်ဇေယျ",
        "en": "U SAW AUNG ZEYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013003-2"
  },
  {
    "votes": 12681,
    "candidate": {
      "name": {
        "my": "ဦးခင်မြင့်",
        "en": "U KHIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013003-2"
  },
  {
    "votes": 34326,
    "candidate": {
      "name": {
        "my": "ဦးလှဦး",
        "en": "U HLA OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013003-2"
  },
  {
    "votes": 18498,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမြင့်",
        "en": "U THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013006-1"
  },
  {
    "votes": 3483,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဟန်",
        "en": "U SEIN HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013006-1"
  },
  {
    "votes": 7760,
    "candidate": {
      "name": {
        "my": "ဦးသာနို့အောင်",
        "en": "U THA NOE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013006-1"
  },
  {
    "votes": 9429,
    "candidate": {
      "name": {
        "my": "ဦးစောလိှုင်",
        "en": "U SAW HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013006-2"
  },
  {
    "votes": 21566,
    "candidate": {
      "name": {
        "my": "ဦးတိုင်းရ",
        "en": "U TIME RAH"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013006-2"
  },
  {
    "votes": 34285,
    "candidate": {
      "name": {
        "my": "ဦးစောဆန်နီချန်း",
        "en": "SAW SANNY CHAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013001-1"
  },
  {
    "votes": 25072,
    "candidate": {
      "name": {
        "my": "ဦးမန်းအေးဝင်း",
        "en": "MAHN AYE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013001-1"
  },
  {
    "votes": 33008,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်းမြင့်",
        "en": "U AUNG SAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013001-2"
  },
  {
    "votes": 24979,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိုး",
        "en": "U AUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013001-2"
  },
  {
    "votes": 7003,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အောင်",
        "en": "U KYAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013041-1"
  },
  {
    "votes": 8077,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ခင်",
        "en": "U AUNG KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013041-1"
  },
  {
    "votes": 5803,
    "candidate": {
      "name": {
        "my": "ဦးမြငေွ",
        "en": "U MYA NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013041-2"
  },
  {
    "votes": 4792,
    "candidate": {
      "name": {
        "my": "ဦးကျော်နု",
        "en": "U KYAW NU"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013041-2"
  },
  {
    "votes": 1379,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013028-1"
  },
  {
    "votes": 7343,
    "candidate": {
      "name": {
        "my": "ဦးစိုးလိှုင်",
        "en": "U SOE HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013028-1"
  },
  {
    "votes": 21697,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထိန်",
        "en": "U WIN HTEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013028-1"
  },
  {
    "votes": 2368,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": "U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013028-2"
  },
  {
    "votes": 9290,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ကြည်",
        "en": "U NYUNT KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013028-2"
  },
  {
    "votes": 20768,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထွန်းနိုင်",
        "en": "U WIN TUN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013028-2"
  },
  {
    "votes": 10767,
    "candidate": {
      "name": {
        "my": "ဦးမြအေး",
        "en": "U MYA AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013026-1"
  },
  {
    "votes": 34485,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်အောင်",
        "en": "U KYAW MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013026-1"
  },
  {
    "votes": 29343,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဦး",
        "en": "U MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013026-2"
  },
  {
    "votes": 12651,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မြင့်",
        "en": "U MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013026-2"
  },
  {
    "votes": 5638,
    "candidate": {
      "name": {
        "my": "ဦးစကျော်စိန်",
        "en": "U SA KYAW SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013029-1"
  },
  {
    "votes": 27840,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်စိုး",
        "en": "U KHIN MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013029-1"
  },
  {
    "votes": 3438,
    "candidate": {
      "name": {
        "my": "ဦးသက်လိှုင်",
        "en": "U THET HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013029-2"
  },
  {
    "votes": 26853,
    "candidate": {
      "name": {
        "my": "ဦးစိန်တင်ဝင်း",
        "en": "U SEIN TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013029-2"
  },
  {
    "votes": 2214,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ရည်",
        "en": "U MAUNG MAUNG YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013033-1"
  },
  {
    "votes": 3652,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးသိန်း (ခ) မြသိန်း",
        "en": "U MYO THAIN (A) MYA THAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013033-1"
  },
  {
    "votes": 2561,
    "candidate": {
      "name": {
        "my": "ဦးညီလင်းဖြိုး",
        "en": "U NYI LIN PHYOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013033-2"
  },
  {
    "votes": 2808,
    "candidate": {
      "name": {
        "my": "ဦးမြသိန်း",
        "en": "U MYA THAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013033-2"
  },
  {
    "votes": 12410,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဆေွ",
        "en": "U CHIT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013024-1"
  },
  {
    "votes": 21902,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလှရေွှ",
        "en": "U TUN HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013024-1"
  },
  {
    "votes": 9158,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဦး",
        "en": "U Tun Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013024-2"
  },
  {
    "votes": 10762,
    "candidate": {
      "name": {
        "my": "ဦးကြည်လွင်",
        "en": "U KYI LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013024-2"
  },
  {
    "votes": 15986,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013024-2"
  },
  {
    "votes": 10650,
    "candidate": {
      "name": {
        "my": "ဒေါ်ကြည်ကြည်မာ",
        "en": "DAW KYI KYI MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013038-1"
  },
  {
    "votes": 9172,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်သက်မာ",
        "en": "DAW KHIN THEH MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013038-1"
  },
  {
    "votes": 12828,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လွင်",
        "en": "U MYINT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013038-2"
  },
  {
    "votes": 8935,
    "candidate": {
      "name": {
        "my": "ဦးဦးကြည်",
        "en": "U OO KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013038-2"
  },
  {
    "votes": 3368,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်(ခ)ဦးဝင်းနိုင်ဦး",
        "en": "U WIN NAING (A) U WIN NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013035-1"
  },
  {
    "votes": 3262,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန္ဒာမြင့်",
        "en": "DAW SANDA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013035-1"
  },
  {
    "votes": 3159,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ချို",
        "en": "U MYINT CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013035-2"
  },
  {
    "votes": 3588,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013035-2"
  },
  {
    "votes": 2951,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013036-1"
  },
  {
    "votes": 2194,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်အောင်",
        "en": "U NYUNT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013036-1"
  },
  {
    "votes": 2094,
    "candidate": {
      "name": {
        "my": "ဦးနန်းညွန့်ဝင်းမော်",
        "en": "U NAN NYUNT WIN MAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013036-2"
  },
  {
    "votes": 1868,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဦး",
        "en": "U KYAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013036-2"
  },
  {
    "votes": 19840,
    "candidate": {
      "name": {
        "my": "ဦးမင်းသိမ်းထွန်း",
        "en": "U MIN THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013042-1"
  },
  {
    "votes": 15686,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013042-1"
  },
  {
    "votes": 27218,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ညွန့်",
        "en": "U AUNG KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013042-2"
  },
  {
    "votes": 15065,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013042-2"
  },
  {
    "votes": 8226,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းသောင်း",
        "en": "U OHN THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR013002-1"
  },
  {
    "votes": 29853,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသက်တင်",
        "en": "DR. THET TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013002-1"
  },
  {
    "votes": 14185,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်မြင့်",
        "en": "DAW TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013002-1"
  },
  {
    "votes": 9776,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုးမြင့်",
        "en": "U KYAW SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR013002-2"
  },
  {
    "votes": 42405,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမင်း",
        "en": "U SOE MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013002-2"
  },
  {
    "votes": 14879,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ခိုင်",
        "en": "U CHIT KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013002-2"
  },
  {
    "votes": 3302,
    "candidate": {
      "name": {
        "my": "ဦးလှသိန်း",
        "en": "Hla Thein (U"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR013022-1"
  },
  {
    "votes": 13466,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာ စောလှထွန်း",
        "en": "DR. SAW HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013022-1"
  },
  {
    "votes": 2307,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ညွန့်",
        "en": "U SEIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013022-1"
  },
  {
    "votes": 11001,
    "candidate": {
      "name": {
        "my": "ဦးလှဌေး",
        "en": "U HLA HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013022-1"
  },
  {
    "votes": 15518,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုး",
        "en": "U KYAW SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013022-2"
  },
  {
    "votes": 9169,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သင်း",
        "en": "U KYAW THIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR013022-2"
  },
  {
    "votes": 5089,
    "candidate": {
      "name": {
        "my": "ဦးဟံဆေွ",
        "en": "U HAN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013022-2"
  },
  {
    "votes": 30556,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013012-1"
  },
  {
    "votes": 8007,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်တင်",
        "en": "U NYUNT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013012-1"
  },
  {
    "votes": 24713,
    "candidate": {
      "name": {
        "my": "ဦးစန်းဝင်း",
        "en": "U SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013012-1"
  },
  {
    "votes": 7931,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဦးသိန်း",
        "en": "U Kyi Oo Thein"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013012-2"
  },
  {
    "votes": 32363,
    "candidate": {
      "name": {
        "my": "ဦးသက်ထွန်းမောင်",
        "en": "U THET TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013012-2"
  },
  {
    "votes": 6715,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမြင့်",
        "en": "U HTAY MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013012-2"
  },
  {
    "votes": 23511,
    "candidate": {
      "name": {
        "my": "ှုဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013012-2"
  },
  {
    "votes": 4115,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013034-1"
  },
  {
    "votes": 3105,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ငြိမ်း",
        "en": "U KYAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013034-1"
  },
  {
    "votes": 2134,
    "candidate": {
      "name": {
        "my": "ဦးရဲမင်းဇော်",
        "en": "U YE MIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR013034-2"
  },
  {
    "votes": 3083,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကိုဝင်းလှ",
        "en": "DR. KO WIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013034-2"
  },
  {
    "votes": 1143,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013034-2"
  },
  {
    "votes": 4628,
    "candidate": {
      "name": {
        "my": "ဦးတင်ချိုအေး",
        "en": "U TIN CHO AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013016-1"
  },
  {
    "votes": 3254,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထွဋ်",
        "en": "U WIN HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013016-1"
  },
  {
    "votes": 4231,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်ကြည်",
        "en": "U TUN AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013016-2"
  },
  {
    "votes": 4063,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013016-2"
  },
  {
    "votes": 7795,
    "candidate": {
      "name": {
        "my": "ဦးဌေးအောင်",
        "en": "U HTAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013039-1"
  },
  {
    "votes": 6926,
    "candidate": {
      "name": {
        "my": "ဦးခင်စိုး",
        "en": "U KHIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013039-1"
  },
  {
    "votes": 8312,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းအောင်ကျော်",
        "en": "U WIN AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013039-2"
  },
  {
    "votes": 9072,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမင်းအောင်",
        "en": "U MYO MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013039-2"
  },
  {
    "votes": 4441,
    "candidate": {
      "name": {
        "my": "ဦးမင်းသိမ်း",
        "en": "U MIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013031-1"
  },
  {
    "votes": 2387,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013031-1"
  },
  {
    "votes": 5558,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဆေွ",
        "en": "U MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013031-2"
  },
  {
    "votes": 1279,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်အေးရီ",
        "en": "DAW KHIN AYE YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013031-2"
  },
  {
    "votes": 323,
    "candidate": {
      "name": {
        "my": "ဦးစံကျော်",
        "en": "U SAN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013045-1"
  },
  {
    "votes": 69,
    "candidate": {
      "name": {
        "my": "ဦးလှမြိုင်",
        "en": "U HLA MYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013045-1"
  },
  {
    "votes": 842,
    "candidate": {
      "name": {
        "my": "ဦးနေမျိုးအောင်",
        "en": "U NAY MYO AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013045-2"
  },
  {
    "votes": 302,
    "candidate": {
      "name": {
        "my": "ဦးသန်းရေွှ",
        "en": "U THAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013045-2"
  },
  {
    "votes": 26093,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ဝင်း",
        "en": "U MAUNG MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013007-1"
  },
  {
    "votes": 20404,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သိန်း",
        "en": "U KHIN MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013007-1"
  },
  {
    "votes": 32687,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန်းစန်းနွယ်",
        "en": "DAW SAN SAN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013007-2"
  },
  {
    "votes": 23320,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်စောလွင်",
        "en": "DAW KHIN SAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013007-2"
  },
  {
    "votes": 16844,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစောနိုင်",
        "en": "Dr. Saw Naing"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013011-1"
  },
  {
    "votes": 1473,
    "candidate": {
      "name": {
        "my": "ဦးစောမြတ်နိုင်",
        "en": "U SAW MYAT KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013011-1"
  },
  {
    "votes": 16959,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်မိုး",
        "en": "U AUNG KYAW MOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013011-1"
  },
  {
    "votes": 3985,
    "candidate": {
      "name": {
        "my": "ဦးလှရေွှ",
        "en": "U HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013011-1"
  },
  {
    "votes": 25090,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမင်းအောင်",
        "en": "U MYO MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013011-2"
  },
  {
    "votes": 14174,
    "candidate": {
      "name": {
        "my": "ဦးလှသောင်း",
        "en": "U HLA THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013011-2"
  },
  {
    "votes": 25680,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ဝင်း",
        "en": "U NYAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013005-1"
  },
  {
    "votes": 39462,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်းမောင်",
        "en": "U SOE WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013005-1"
  },
  {
    "votes": 35397,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013005-2"
  },
  {
    "votes": 25925,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိန်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013005-2"
  },
  {
    "votes": 15999,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်",
        "en": "U KHIN MAUGN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013015-1"
  },
  {
    "votes": 18130,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထိန်",
        "en": "U WIN HTEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013015-1"
  },
  {
    "votes": 15121,
    "candidate": {
      "name": {
        "my": "ဦးလေးမြင့်",
        "en": "U LAY MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013015-2"
  },
  {
    "votes": 17873,
    "candidate": {
      "name": {
        "my": "ဦးအေးသိန်း",
        "en": "U AYE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013015-2"
  },
  {
    "votes": 25613,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ဖေ",
        "en": "U NYUNT PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013013-1"
  },
  {
    "votes": 20174,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013013-1"
  },
  {
    "votes": 29426,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်းနိုင်",
        "en": "U ZAW WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013013-2"
  },
  {
    "votes": 23335,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013013-2"
  },
  {
    "votes": 1961,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကိုကိုဝင်း",
        "en": "U Win Ko Ko Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013023-1"
  },
  {
    "votes": 17379,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်ဦး",
        "en": "U KHIN MAUNG LWIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013023-1"
  },
  {
    "votes": 7181,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013023-1"
  },
  {
    "votes": 30897,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းဦး",
        "en": "U AUNG THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013023-1"
  },
  {
    "votes": 16717,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်အေး",
        "en": "U MAUNG MAUNG AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013023-2"
  },
  {
    "votes": 33877,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်း",
        "en": "U KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013023-2"
  },
  {
    "votes": 22635,
    "candidate": {
      "name": {
        "my": "ဦးကျော်",
        "en": "U KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013009-1"
  },
  {
    "votes": 3508,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ကြီး",
        "en": "U MAUNG MAUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013009-1"
  },
  {
    "votes": 14562,
    "candidate": {
      "name": {
        "my": "ဦးစောသိန်း",
        "en": "U SAW THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013009-1"
  },
  {
    "votes": 31363,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန်းစန်းမြင့်",
        "en": "DAW SAN SAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013009-2"
  },
  {
    "votes": 23349,
    "candidate": {
      "name": {
        "my": "ဦးရေွှအိ",
        "en": "U SHWE EKK"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013009-2"
  },
  {
    "votes": 13350,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U NAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013025-1"
  },
  {
    "votes": 25950,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်သိန်း",
        "en": "DR. MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013025-1"
  },
  {
    "votes": 12879,
    "candidate": {
      "name": {
        "my": "ဦးသစ်လွင်",
        "en": "U THIT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013025-2"
  },
  {
    "votes": 24849,
    "candidate": {
      "name": {
        "my": "ဦးမြသိန်း",
        "en": "U MYA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013025-2"
  },
  {
    "votes": 6851,
    "candidate": {
      "name": {
        "my": "ဦးစောကျော်ထူးဝင်း",
        "en": "U SAW KYAW HTOO WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013027-1"
  },
  {
    "votes": 9401,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျင်(ခ)ဦးစောတာပိုး",
        "en": "SAW MAUNG KYIN (A) SAW TARPO"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013027-1"
  },
  {
    "votes": 2699,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမြင့်",
        "en": "U THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013027-1"
  },
  {
    "votes": 4447,
    "candidate": {
      "name": {
        "my": "ဦးစောရိန်းဘိုး",
        "en": "U SAW RAINBOW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013027-2"
  },
  {
    "votes": 10445,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအေး",
        "en": "U TUN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013027-2"
  },
  {
    "votes": 37172,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာ အုန်းကြွယ်",
        "en": "DR. OHN KYEW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013027-2"
  },
  {
    "votes": 7040,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းကျော်",
        "en": "U THAUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013010-1"
  },
  {
    "votes": 5562,
    "candidate": {
      "name": {
        "my": "ဦးဇင်မောင်မောင်ဌေး",
        "en": "U ZIN MAUNG MAUNG TAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013010-1"
  },
  {
    "votes": 6301,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မိုးထွဋ်",
        "en": "U AUNG MOE HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013010-2"
  },
  {
    "votes": 9739,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးခင်",
        "en": "U MYO KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013010-2"
  },
  {
    "ethnic_seat": true,
    "votes": 61045,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013",
    "candidate": {
      "name": {
        "my": "စောထွန်းအောင်မြင့်",
        "en": "U SAW TUN AUNG MYINT"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 43075,
    "acclaim": "N",
    "ethnicity": "Kayin",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013",
    "candidate": {
      "name": {
        "my": "စောတာကပေါ",
        "en": "SAW TAKABAW"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 8713,
    "acclaim": "N",
    "ethnicity": "Rakhine",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013",
    "candidate": {
      "name": {
        "my": "ဒေါက်တာ ထွန်းဇံအောင်",
        "en": "DR. TUN ZAN AUNG"
      }
    }
  },
  {
    "ethnic_seat": true,
    "votes": 26251,
    "acclaim": "N",
    "ethnicity": "Rakhine",
    "parliament_code": "YANGON-RGH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR013",
    "candidate": {
      "name": {
        "my": "ဦးဇော်အေးမောင်",
        "en": "U ZAW AYE MAUNG"
      }
    }
  },
  {
    "votes": 123033,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဟန်လင်း",
        "en": "U SOE HAN LIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 54113,
    "candidate": {
      "name": {
        "my": "ဦးကိုတင်",
        "en": "U KO TIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 44845,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": " U AUNG NAING"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 31291,
    "candidate": {
      "name": {
        "my": "ဦးမန်းစိန်လှဝင်း",
        "en": "U MAHN SEIN HLA WIN"
      }
    },
    "acclaim": "N",
    "party": "KPP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 124100,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": " U SOE WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 83801,
    "candidate": {
      "name": {
        "my": "ဦးစောဂျဲလ်ဆင်",
        "en": " U SAW JACKSON"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 134610,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်လင်း",
        "en": " U NYAN LINN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 83376,
    "candidate": {
      "name": {
        "my": "ဦးအေးကျော်",
        "en": " U AYE KYAW"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 210604,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်လှိုင်",
        "en": "U NYUNT HLAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 56933,
    "candidate": {
      "name": {
        "my": " ဦးလှရှိန်",
        "en": "U HLA SHEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 147403,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဌေးဝင်း",
        "en": " DR. HTAT WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 32541,
    "candidate": {
      "name": {
        "my": "ဦးစောဟင်နရီ",
        "en": "U SAW HENRY"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 105647,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရီ",
        "en": "U KHIN MAUNG YEE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 51289,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သောင်း",
        "en": " U KYAW THAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 40909,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုးလှိုင်",
        "en": "U Aung Myo Hlaing"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 197835,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဌေးနိုင်",
        "en": " DR. HTAY NAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 125397,
    "candidate": {
      "name": {
        "my": " ဦးမန်းထွန်းဝင်း",
        "en": " U MANN TUN WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 175721,
    "candidate": {
      "name": {
        "my": "ဦးသက်လင်း",
        "en": "U Thet Lin"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 18505,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "party": "MPP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 65350,
    "candidate": {
      "name": {
        "my": "ဒေါ်လှငေွ",
        "en": "DAW HLA NGWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 119260,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသိန်း",
        "en": " U WIN THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 94505,
    "candidate": {
      "name": {
        "my": "ဦးမန်းထိန်ဝင်းစိန်",
        "en": " U MANN HTEIN WIN SEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 17632,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းရေွှ",
        "en": "U THEIN SHWE"
      }
    },
    "acclaim": "N",
    "party": "MPP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 156485,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြတ်မြတ်အုန်းခင်",
        "en": " DR. MYAT MYAT OHN KHIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 45827,
    "candidate": {
      "name": {
        "my": "ဦးအေးမြင့်မောင်",
        "en": " U AYE MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 14402,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းလွင်",
        "en": "U OHN LWIN"
      }
    },
    "acclaim": "N",
    "party": "NPAL",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 12377,
    "candidate": {
      "name": {
        "my": "ဦးဘဝင်း",
        "en": "U BA WIN"
      }
    },
    "acclaim": "N",
    "party": " Peace and Diversity Party",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 11894,
    "candidate": {
      "name": {
        "my": "ဒေါ်ရီရီစံ",
        "en": "DAW YI YI SAN"
      }
    },
    "acclaim": "N",
    "party": "MPP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 141047,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်အေး",
        "en": " U MAUNG MAUNG AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 60363,
    "candidate": {
      "name": {
        "my": "ဦးမန်းဖေဝင်း",
        "en": " U MANN PE WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 176215,
    "candidate": {
      "name": {
        "my": "ဦးလိှုင်ဦး",
        "en": " U HLAING OO)"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 116296,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြ",
        "en": " U KYAW MYA"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 74059,
    "candidate": {
      "name": {
        "my": "ဦးခင်လိှုင်",
        "en": " U KHIN HLAING"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Ayeyarwaddy-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 26492,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဌေး",
        "en": "Thein Htay (U)"
      }
    },
    "acclaim": "N",
    "party": "UDP2",
    "year": 2010,
    "constituency ": "Bago-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 88406,
    "candidate": {
      "name": {
        "my": "ဦးစောမော်ထွန်း",
        "en": " U SAW MAW TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 40084,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဆေွ",
        "en": "U AUNG NAING SWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 99358,
    "candidate": {
      "name": {
        "my": " ဦးဖိုးဇော်",
        "en": "U PHOE ZAW"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 54934,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဇံ",
        "en": "U THEIN ZAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 114022,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": " U TIN MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 72005,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သိန်း",
        "en": " U AUNG THEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 107242,
    "candidate": {
      "name": {
        "my": "ဦးရဲမြင့်",
        "en": " U YE MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 81520,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြည်ဝင်း",
        "en": " U AUNG KYI WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 8349,
    "candidate": {
      "name": {
        "my": "စောစေးဝါးညွန့်",
        "en": "U SAW SAY WAR NYUNT"
      }
    },
    "acclaim": "N",
    "party": "KPP",
    "year": 2010,
    "constituency ": "Bago-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 17431,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထွန်း",
        "en": "U Win Htun"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Bago-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 128046,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်ရှိန်",
        "en": " U WIN NAING SHEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 53027,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": " U SOE MYINT )"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 96338,
    "candidate": {
      "name": {
        "my": "ဦးသက်နိုင်ဦး",
        "en": "U THET NAING OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 68863,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုး",
        "en": " U KYAW SOE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 81058,
    "candidate": {
      "name": {
        "my": "ဒေါ်ချိုနွယ်ဦး",
        "en": " DAW CHO NWE OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 40830,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမြင့်",
        "en": " U SEIN MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 49475,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်အောင်",
        "en": "U KYAW KYAW AUNG"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Bago-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 136324,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်မောင်လေး",
        "en": "U MAUNG MAUNG LAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 62500,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဆေွ",
        "en": "U TIN MAUNG SWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 48389,
    "candidate": {
      "name": {
        "my": "ဦးအေးလွင်",
        "en": "U AYE LWIN"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Bago-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 110250,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြဦး",
        "en": " DR. MYA OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 44687,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": " U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 83288,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ထွန်း",
        "en": " U MYINT TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 57979,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ရင်",
        "en": " U NYUNT YIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 36156,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဟန်",
        "en": "U Kyi Han"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Bago-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 92771,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ရင်",
        "en": " U NYUNT YIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 50953,
    "candidate": {
      "name": {
        "my": "ဦးဝင်း",
        "en": " U WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 40574,
    "candidate": {
      "name": {
        "my": "ဦးဇေယျာလိှူင်",
        "en": "U ZEYA HLAING"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Bago-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 97477,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဇော်(ခ)ကိုပေါက်",
        "en": " U TUN ZAW (a)KO PAUK"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Bago-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 45604,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်တင်ထေွး",
        "en": " DAW TIN TIN HTWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Bago-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 3848,
    "candidate": {
      "name": {
        "my": "ဦးရုလ်ဇမ်",
        "en": "U RUL ZAM"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 916,
    "candidate": {
      "name": {
        "my": "ဦးစမာန်",
        "en": "U CA MANG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 1269,
    "candidate": {
      "name": {
        "my": "ဒေါ်လဲန်ဗဲလ်",
        "en": "DAW LEN VEL"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 4785,
    "candidate": {
      "name": {
        "my": "ဦးကျွန်ခဲန်း",
        "en": "U COM KHEN"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 4058,
    "candidate": {
      "name": {
        "my": "ဒေါ်ရေွှထေွး",
        "en": "DAW SHWE HTWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 766,
    "candidate": {
      "name": {
        "my": "ဦးဗန်လွန်",
        "en": "U VAN LUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 5230,
    "candidate": {
      "name": {
        "my": "ဦးဇုန်လှယ်ထန်း",
        "en": "U ZUNG HLEI THANG"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 8072,
    "candidate": {
      "name": {
        "my": "ဦးမှှုှံကီယို",
        "en": "U MHON KEE RO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 12370,
    "candidate": {
      "name": {
        "my": "ဦးပေါလ်ထန်ျထိုင်း",
        "en": "U POL HTANG HTAING"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 1347,
    "candidate": {
      "name": {
        "my": "ဦးဘေွထန်းကျင်ဇ",
        "en": "U BWE HTANG CIN ZA"
      }
    },
    "acclaim": "N",
    "party": "UDP1",
    "year": 2010,
    "constituency ": "Chin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 4650,
    "candidate": {
      "name": {
        "my": "ဦးယောဟန်",
        "en": "U YAW HAN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 1830,
    "candidate": {
      "name": {
        "my": "ဦးဘရက်ခံ",
        "en": "U BYET KHAM"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 10435,
    "candidate": {
      "name": {
        "my": "ဦးစတီဗင်ထာဘိတ်",
        "en": "U STEPHEN TARBEIT"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 6434,
    "candidate": {
      "name": {
        "my": "ဦးခမ်ဆွင်းမုန်",
        "en": "U KHAM SUIN MHON"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 5147,
    "candidate": {
      "name": {
        "my": "ဦးကိုတုတ်ချင်းထန်",
        "en": "U KOTE CHIN HTANG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 4486,
    "candidate": {
      "name": {
        "my": "ဦးခုပ်လျန်း",
        "en": "U KHU LIAN"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 1241,
    "candidate": {
      "name": {
        "my": "ဦးဝင့်ဟဲ့လျန်း",
        "en": "U WINT HEI LIANG"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 6104,
    "candidate": {
      "name": {
        "my": "ဦးဆွန်ဒုတ်ကျင့်",
        "en": "U SUN DOKE KYIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 3680,
    "candidate": {
      "name": {
        "my": "ဦးထန်စွမ်းပေါင်",
        "en": "U HTAUNT SAWM PAU"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 5646,
    "candidate": {
      "name": {
        "my": "ဦးလန်းဇမန်",
        "en": "U LAN ZAMAN"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 1761,
    "candidate": {
      "name": {
        "my": "ဦးအက်စ်ဘီခန့်ဇမ်းဝုန်",
        "en": "U M.B. CAM ZAWON"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 5579,
    "candidate": {
      "name": {
        "my": "ဦးနိန်းခင်းပေါင်",
        "en": "U NEIN KIM PAU"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 1891,
    "candidate": {
      "name": {
        "my": "ဦးလိုခိုဟန်",
        "en": "U LO KHO HAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 4696,
    "candidate": {
      "name": {
        "my": "ဦးခမ်ဟောက်လျန်း",
        "en": "U CAM HAU LIANG"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 6882,
    "candidate": {
      "name": {
        "my": "ဦးဟာရှိန်းဘေွ",
        "en": "U HAR SHEIN BWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 1179,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လိန်းဖေ",
        "en": "U AUNG LEIN PE"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Chin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 3968,
    "candidate": {
      "name": {
        "my": "ဦးဖီလင်းမောင်",
        "en": "U PE LEIN MAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 2107,
    "candidate": {
      "name": {
        "my": "ဦးထန်းကီး",
        "en": "U TAN KI"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 2392,
    "candidate": {
      "name": {
        "my": "ဦးလိန်းအွမ်",
        "en": "U LING UNG"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 5261,
    "candidate": {
      "name": {
        "my": "ဦးအာမီ",
        "en": "U AR MI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 6385,
    "candidate": {
      "name": {
        "my": "ဦးပေါ်လှျံလွင်",
        "en": "U PAUL LIA LWIN"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 6005,
    "candidate": {
      "name": {
        "my": "ဦးဆန်လေှးမန်",
        "en": " U SANG HLEI MANG"
      }
    },
    "acclaim": "N",
    "party": "ENDP",
    "year": 2010,
    "constituency ": "Chin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 4983,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဌေးဝင်း",
        "en": " DR. HTAY WIN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Chin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 2899,
    "candidate": {
      "name": {
        "my": "ဦးမနားနိုင်း",
        "en": "U MANA NAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 1310,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်",
        "en": "U HLA MAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 1137,
    "candidate": {
      "name": {
        "my": "ဦးလိန်းပိုင်း",
        "en": "U LEIN PAING"
      }
    },
    "acclaim": "N",
    "party": "Chin National Party",
    "year": 2010,
    "constituency ": "Chin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 2715,
    "candidate": {
      "name": {
        "my": "ဦးမနားရှင်း",
        "en": "U MANA SHING"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 3469,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 8407,
    "candidate": {
      "name": {
        "my": "ဦးဝေှ့ယိန်း",
        "en": "U WAI YING"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 2111,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်ဖြူ",
        "en": " U KYAW KYAW PHYU"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Chin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 4304,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": " U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "party": "UDP1",
    "year": 2010,
    "constituency ": "Chin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 6075,
    "candidate": {
      "name": {
        "my": "ဦးဆလိုင်းခွဲယန်",
        "en": "U SALAI KWE YAN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Chin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 1565,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စံအေး",
        "en": "U MAUNG SAN AYE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Chin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 5758,
    "candidate": {
      "name": {
        "my": "ဦးဘူးလွင်",
        "en": "U BU LWIN"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Chin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 2546,
    "candidate": {
      "name": {
        "my": "ဦးသန်းကျော်အောင်",
        "en": "U THAN KYAW AUNG"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Chin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 1367,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်မောင်",
        "en": " U MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "party": "UDP1",
    "year": 2010,
    "constituency ": "Chin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 52391,
    "candidate": {
      "name": {
        "my": "ဦးခက်ထိန်နန်",
        "en": "U KET HTEIN NAN"
      }
    },
    "acclaim": "N",
    "party": "UDPKS",
    "year": 2010,
    "constituency ": "Kachin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 28510,
    "candidate": {
      "name": {
        "my": "ဦးမာလာလေး",
        "en": "U Malar Lay"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 9864,
    "candidate": {
      "name": {
        "my": "ဦးမင်းကြည်",
        "en": "U Min Kyi"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 9848,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဝင်းထင်",
        "en": "U Sai Win Tin"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 14294,
    "candidate": {
      "name": {
        "my": "ဦးဂမ်ဆိုင်း",
        "en": "U GAN SENG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 5260,
    "candidate": {
      "name": {
        "my": "ဒေါ်ရှဒေါင်းခွမ်တောင်",
        "en": "DAW SHADAUNG KWAN TAUNG"
      }
    },
    "acclaim": "N",
    "party": "UDPKS",
    "year": 2010,
    "constituency ": "Kachin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 7196,
    "candidate": {
      "name": {
        "my": "ဦးဘရန်ရေှာင်",
        "en": "U BRENG SHAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 4047,
    "candidate": {
      "name": {
        "my": "ဦးမဂျီဇော်တောင်",
        "en": "U MA GYI ZAW TAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 6953,
    "candidate": {
      "name": {
        "my": "ဦးစခုန်တိန့်ယိန်း",
        "en": "U ZAHKUNG TING YING"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Kachin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 8550,
    "candidate": {
      "name": {
        "my": "ဦးစောဝင်းထွန်း",
        "en": "U Saw Win Tun"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 16172,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတင်အောင်",
        "en": "U SAI TIN AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 8419,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုဦး",
        "en": "U KO KO OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 15176,
    "candidate": {
      "name": {
        "my": "ဦးဖေသောင်း",
        "en": "U PE THAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 6437,
    "candidate": {
      "name": {
        "my": "ဦးမောင်နွဲ့",
        "en": "U MAUNG NWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 9242,
    "candidate": {
      "name": {
        "my": "ဦးစံသိန်း",
        "en": "U SAN THEIN"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 13056,
    "candidate": {
      "name": {
        "my": "ဦးစောသန်းမြင့်",
        "en": "U Saw Than Myint"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 31549,
    "candidate": {
      "name": {
        "my": "ဦးစံပြည့်",
        "en": " U SAN PYEI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 15382,
    "candidate": {
      "name": {
        "my": "စိုင်းသန်းရေွှ",
        "en": "Sai Than Shwe"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 37606,
    "candidate": {
      "name": {
        "my": "ဦးမြအုန်း",
        "en": " U MYA OHN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 13880,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလွင်",
        "en": " U TUN LWIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 8323,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းငေွ",
        "en": " U OHN NGWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 4222,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဦး",
        "en": "U KYAW OO"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 13001,
    "candidate": {
      "name": {
        "my": "ဦးဖော်လားကမ်ဖန်",
        "en": "U PHAW LAR KAM PHAN"
      }
    },
    "acclaim": "N",
    "party": "UDPKS",
    "year": 2010,
    "constituency ": "Kachin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 15250,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမြမောင်",
        "en": "U SAIN MYA MAUNG"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Kachin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 26474,
    "candidate": {
      "name": {
        "my": "ဦးစံထွန်း",
        "en": "U SAN TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 26013,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ရေွှ",
        "en": "U MYINT SHWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 19553,
    "candidate": {
      "name": {
        "my": "ဦးရယ်ဒန်းတန်",
        "en": " U REH DIN TANG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kachin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 25107,
    "candidate": {
      "name": {
        "my": "ဦးဂျေယောဝူ",
        "en": "U JAE YAW WU"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kachin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 23249,
    "candidate": {
      "name": {
        "my": "ဦးပီမောင်စိုး",
        "en": " U P. MAUNG SOE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 5995,
    "candidate": {
      "name": {
        "my": "ဦးအယ်လိယာ",
        "en": "U L.LIYAR"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 20852,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဒင်(ခ)ဦးထေးရယ်",
        "en": " U KYAW DIN (a) U HTAYAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 9231,
    "candidate": {
      "name": {
        "my": "ဦးစောချယ်ရီဘတ်",
        "en": "U SAW CHERRYBAT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 11662,
    "candidate": {
      "name": {
        "my": "ဦးဘိုရယ်",
        "en": "U BO YAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 1387,
    "candidate": {
      "name": {
        "my": "ဦးငါးရယ်ကျော်ဟန်",
        "en": "U NGARYAY KYAW HAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 1656,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": " U KYAW WIN"
      }
    },
    "acclaim": "N",
    "party": "KNP2",
    "year": 2010,
    "constituency ": "Kayah-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 15255,
    "candidate": {
      "name": {
        "my": "ဒေါ်မေးရီယန်",
        "en": "DAW MERRY YAN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 1152,
    "candidate": {
      "name": {
        "my": "ဦးဒိုနေးရှာ",
        "en": "SAW DOH NAY SHAR"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 3020,
    "candidate": {
      "name": {
        "my": "ဦးခလိုက်",
        "en": "U KA LAING"
      }
    },
    "acclaim": "N",
    "party": "KNP2",
    "year": 2010,
    "constituency ": "Kayah-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 3360,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းရယ်",
        "en": " U SAN YAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 1641,
    "candidate": {
      "name": {
        "my": "ဦးဖရေးရယ်",
        "en": "U PHAREYAY"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 4250,
    "candidate": {
      "name": {
        "my": "ဦးဆားရယ်",
        "en": " U SARYAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 918,
    "candidate": {
      "name": {
        "my": "ဦးစတင်ဖါနို",
        "en": " U STEFANO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 135,
    "candidate": {
      "name": {
        "my": "ဦးဌေးရယ်",
        "en": " U HTYAYAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 7,
    "candidate": {
      "name": {
        "my": "ဒေါ်ယဉ်ထေွးငယ်",
        "en": "DAW YIN HTWE NGE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 1387,
    "candidate": {
      "name": {
        "my": "ဦးပိုးရယ်အောင်သိန်း",
        "en": " U POYAY AUNG THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 220,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအုန်းမြင့်",
        "en": " U SAI OHN MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 4616,
    "candidate": {
      "name": {
        "my": "ဦးကော်ရိုင်",
        "en": " U KAWYAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 421,
    "candidate": {
      "name": {
        "my": "ဦးနီးကာ",
        "en": "U NIKA"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 3353,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသာစိန်",
        "en": " U SAI THA SEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 205,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရေွှ",
        "en": " U TUN SHWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 3003,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်",
        "en": "U HLA MAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 83,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": " U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 2547,
    "candidate": {
      "name": {
        "my": "ဦးနှင်းဝေ",
        "en": "U HNIN WAI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayah-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 291,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရင်",
        "en": " U TUN YIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayah-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 25687,
    "candidate": {
      "name": {
        "my": "စောအောင်ကျော်နိုင် (ခ)ရှားဘူးဖော့",
        "en": "Saw Aung Kyaw Naing (a) SHAR BUPHAWT"
      }
    },
    "acclaim": "N",
    "party": "PDP2",
    "year": 2010,
    "constituency ": "Kayin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 20442,
    "candidate": {
      "name": {
        "my": "ဦးစောခင်ကျော်ရှိန်",
        "en": " U SAW KHIN KYAW SHEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 14125,
    "candidate": {
      "name": {
        "my": "ဦးစောဗသိန်းနိုင်",
        "en": "U SAW BA THEIN NAING"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 23181,
    "candidate": {
      "name": {
        "my": "စောမြထွန်း",
        "en": "Saw Mya Htun"
      }
    },
    "acclaim": "N",
    "party": "PDP2",
    "year": 2010,
    "constituency ": "Kayin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 23181,
    "candidate": {
      "name": {
        "my": "ဦးကံညွန့်",
        "en": "U KAN NYUNT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 12070,
    "candidate": {
      "name": {
        "my": " ဦးစိန်အောင်",
        "en": " U SEIN AUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 16705,
    "candidate": {
      "name": {
        "my": "စောငြိမ်းသင်",
        "en": "Saw Nyein Thin"
      }
    },
    "acclaim": "N",
    "party": "PDP2",
    "year": 2010,
    "constituency ": "Kayin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 12031,
    "candidate": {
      "name": {
        "my": "ဦးစောလင်းခင်",
        "en": " U SAW LIN KHIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 6500,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": " U THAN TUN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 13381,
    "candidate": {
      "name": {
        "my": "စောသန်းထွန်း",
        "en": "Saw Than Tun"
      }
    },
    "acclaim": "N",
    "party": "PDP2",
    "year": 2010,
    "constituency ": "Kayin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 4846,
    "candidate": {
      "name": {
        "my": "ဦးစောအောင်ငေွ",
        "en": " U SAW AUNG NGWE"
      }
    },
    "acclaim": "N",
    "party": "NLFD",
    "year": 2010,
    "constituency ": "Kayin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 3290,
    "candidate": {
      "name": {
        "my": "ဦးမန်းတင်ရီ",
        "en": " U MANN TIN YI"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 2752,
    "candidate": {
      "name": {
        "my": "ဦးစောထွန်းမြအောင်",
        "en": "U SAW TUN MYA AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 775,
    "candidate": {
      "name": {
        "my": " ဦးစောမောင်ချစ်",
        "en": "U SAW MAUNG CHIT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 2397,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းနီနီအေး",
        "en": " DAW NAN NI NI AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 2197,
    "candidate": {
      "name": {
        "my": "ဦးစောတင်မောင်လွင်",
        "en": "U SAW TIN MAUNG LWIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 12374,
    "candidate": {
      "name": {
        "my": "ဦးစောတော်ပုလဲ",
        "en": "U SAW TAW PALE"
      }
    },
    "acclaim": "N",
    "party": "KPP",
    "year": 2010,
    "constituency ": "Kayin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 7140,
    "candidate": {
      "name": {
        "my": "ဦးစောမာဘယ်",
        "en": "U SAW MARBEL"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစောအေးမြိုင်",
        "en": " U SAW AYE MYAING"
      }
    },
    "acclaim": "Y",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 15360,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်သိန်း",
        "en": " U AUNG MYINT THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 12957,
    "candidate": {
      "name": {
        "my": "ဦးစောကျော်လွင်",
        "en": " U SAW KYAW LWIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 10913,
    "candidate": {
      "name": {
        "my": "မင်းမျိုးတင့်လွင်",
        "en": "Min Myo Tint Lwin"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Kayin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 7361,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်",
        "en": " U WIN NAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 5440,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြိုင်",
        "en": " U AUNG MYAING"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Kayin-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစောမြဝင်း",
        "en": " U SAW MYA WIN"
      }
    },
    "acclaim": "Y",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Kayin-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမန်းအောင်တင်မြင့်",
        "en": "U MANN AUNG TIN MYINT"
      }
    },
    "acclaim": "Y",
    "party": "NLFD",
    "year": 2010,
    "constituency ": "Kayin-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 15968,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ကြည်(ခ)ဦးကျော်စိန်ဟန်-",
        "en": "U KHIN MAUNG KYI (a) U KYAW SEIN HAN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Magway-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 98808,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ထွန်း",
        "en": " U KYI TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 11403,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": " U TIN WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 8968,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လင်းဦး",
        "en": " U AUNG LIN OO"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 103788,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ငြိမ်း",
        "en": "U AUNG NYEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 49433,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ငေွ",
        "en": "U Myint Ngwe"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Magway-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 30897,
    "candidate": {
      "name": {
        "my": "ဦးဘသန်း",
        "en": "U BA THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 32833,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ကိုကို",
        "en": "U Chit Ko Ko"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Magway-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 11150,
    "candidate": {
      "name": {
        "my": "ဦးလှသိန်း",
        "en": " U HLA THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 39788,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်း",
        "en": " U KYAW THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 3672,
    "candidate": {
      "name": {
        "my": "ဦးဌေးငေွ",
        "en": "U HTAY NGWE"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 2826,
    "candidate": {
      "name": {
        "my": "ဥ၊ီးအောင်သန်းတင်",
        "en": "U AUNG THAN TINT"
      }
    },
    "acclaim": "N",
    "party": "NPAL",
    "year": 2010,
    "constituency ": "Magway-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 112455,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U Soe Myint"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Magway-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 79142,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": " U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 27396,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ခန့်",
        "en": " U AUNG KHANT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 132038,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သူဦး",
        "en": " U KYAW THU OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 52737,
    "candidate": {
      "name": {
        "my": "ဦးခင်လှုိင်",
        "en": " U KHIN HLAING"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 112455,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 64053,
    "candidate": {
      "name": {
        "my": "ဦးဌေးအောင်",
        "en": " U HTAY AUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 155693,
    "candidate": {
      "name": {
        "my": " ဦးသန်းဆေွ",
        "en": " U THAN SWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 74254,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": " U KYAW WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 139344,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": " U TIN WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 57391,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": " U KYAW WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 26781,
    "candidate": {
      "name": {
        "my": "ဦးကြင်ဝမ်း",
        "en": "U KYIN WUN"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 122734,
    "candidate": {
      "name": {
        "my": "ဦးသော်ဇင်ဦး",
        "en": " U THAW ZIN OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 56558,
    "candidate": {
      "name": {
        "my": "ဦးရဲညွန့်",
        "en": " U YE NYUNT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 17844,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 96855,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": " U AUNG THAN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 18012,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်",
        "en": " U MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 24115,
    "candidate": {
      "name": {
        "my": "ဦးဌေးနိုင်",
        "en": "U HTAY NAING"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 162959,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်း",
        "en": " U AUNG TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 28059,
    "candidate": {
      "name": {
        "my": "ဦးလှညွန့်",
        "en": "U HLA NYUNT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 20077,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Magway-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 29343,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်သန်း",
        "en": "U Soe Myint Than"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Magway-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 63900,
    "candidate": {
      "name": {
        "my": "ဦးလှဆေွ",
        "en": "U HLA SWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Magway-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 28609,
    "candidate": {
      "name": {
        "my": "ဦးအံ့မောင်",
        "en": " U ANT MAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Magway-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 17623,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာတင်မောင်ထွန်း",
        "en": "DR. TIN MAUNG TUN"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Mandalay-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 120418,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးထွန်း",
        "en": "Dr. Soe Tun"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 82815,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်အောင်",
        "en": "U TIN AUNG AUNG"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 37482,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိန်း",
        "en": "U Myint Thein"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 14008,
    "candidate": {
      "name": {
        "my": "ဒေါ်သန်းသန်းနု",
        "en": "DAW THAN THAN NU"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Mandalay-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 26805,
    "candidate": {
      "name": {
        "my": "ဦးအေသွင်(ခ)ဦးအောင်ခိုင်စိုး",
        "en": "U AYE THWIN (a) U AUNG KHINE SOE"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Mandalay-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 56089,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်မေစီ",
        "en": "Daw Khin May Si"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 136337,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U Win Maung"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 94356,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာလှစိုးညွန့်",
        "en": "DR. HLA SOE NYUNT"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 131225,
    "candidate": {
      "name": {
        "my": "ဦးကမ်ကိုကြီး",
        "en": "U KHAM KO GYEE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 237035,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဝင်း",
        "en": "U TIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 33686,
    "candidate": {
      "name": {
        "my": "ဦးတင်ညွန့်",
        "en": "U TIN NYUNT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 307722,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 58680,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 30573,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": "U AUNG THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 16362,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Mandalay-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 221611,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မြင့်ဖေ",
        "en": "U ZAW MYINT PE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 23667,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ငေွ",
        "en": "U AUNG NGWE"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 43939,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်",
        "en": "U HTAY LWIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 235404,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 21820,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဟိန်း",
        "en": "U KYAW HEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 70711,
    "candidate": {
      "name": {
        "my": "ဦးအေး",
        "en": "U AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 24177,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်လှသောင်း",
        "en": "U MAUNG MAUNG HLA THAUNG"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 34278,
    "candidate": {
      "name": {
        "my": "ဦးလှအောင်",
        "en": " U HLA AUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 238397,
    "candidate": {
      "name": {
        "my": "ဦးခင်အောင်မြင့်",
        "en": "U KHIN AUNG MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 17905,
    "candidate": {
      "name": {
        "my": " ဦးနေမင်းကျော်",
        "en": "U NAY MIN KYAW"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 155561,
    "candidate": {
      "name": {
        "my": "ဦးမှတ်ကြီး",
        "en": "U HMAT KYEE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 19284,
    "candidate": {
      "name": {
        "my": "ဦးရန်အောင်မင်း",
        "en": "U YAN AUNG MIN"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Mandalay-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 46650,
    "candidate": {
      "name": {
        "my": "ဦးစိုးတင့်အောင်",
        "en": "U SOE TINT AUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 307279,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဌေး",
        "en": "U KHIN MAUNG HTAY"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 12465,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇော်ဦး",
        "en": "U AUNG ZAW OO"
      }
    },
    "acclaim": "N",
    "party": "TUOMFONP",
    "year": 2010,
    "constituency ": "Mandalay-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 31053,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဦး",
        "en": "U MYINT OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 177639,
    "candidate": {
      "name": {
        "my": "ဦးစိုးအောင်",
        "en": "U SOE AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 29183,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 27102,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 148593,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမောင်",
        "en": "U HTAY MAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mandalay-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 41816,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": "U KYAW AYE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mandalay-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 36906,
    "candidate": {
      "name": {
        "my": "ဦးညီပု",
        "en": "U NYI PU"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Mandalay-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 13954,
    "candidate": {
      "name": {
        "my": "နိုင်အောင်ချမ်း",
        "en": "Naing Aung Chan"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 21584,
    "candidate": {
      "name": {
        "my": "ဒေါ်ရီရီမြင့်",
        "en": " DAW YI YI MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 10942,
    "candidate": {
      "name": {
        "my": "ဦးဟန်ရင်",
        "en": " U HAN YIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 11820,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်",
        "en": "U Khin Maung"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 25326,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးဝင်း",
        "en": " DR. SOE WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 11451,
    "candidate": {
      "name": {
        "my": "ဦးမြဟန်",
        "en": " U MYA HAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 17555,
    "candidate": {
      "name": {
        "my": "နိုင်လှအောင်",
        "en": "Nai Hla Aung"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 5222,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းဖေ",
        "en": "U OHN PE"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Mon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 20264,
    "candidate": {
      "name": {
        "my": "ဦးမင်းလွန်းအောင်",
        "en": " U MIN LUN AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 20264,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမြင့်(၂)",
        "en": " U THEIN MYINT (2)"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 23750,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သိန်း",
        "en": "U Kyaw Thein"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 18984,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဉာဏ်ဇော်",
        "en": "DR. NYAN ZAW"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 6935,
    "candidate": {
      "name": {
        "my": "ဦးစံသောင်း",
        "en": "U SAN THAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 13439,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ထွန်းအုံ",
        "en": "U NAING TUN OHN"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 10187,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 7818,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": " U TIN TUN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 26066,
    "candidate": {
      "name": {
        "my": "နိုင်ကွန်ချမ်း",
        "en": "Naing Kung Chan"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 29018,
    "candidate": {
      "name": {
        "my": "\\- ဦးလှသိန်း(ခ)ဒေါက်တာလှသိန်း",
        "en": "U HLA THEIN (a) DR. HLA THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 5651,
    "candidate": {
      "name": {
        "my": "ဦးဘရှိန်",
        "en": " U BA SHEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 33963,
    "candidate": {
      "name": {
        "my": "နိုင်ဗညားအောင်မိုး",
        "en": "(Dr.) Banya Aung Moe"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 15349,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": " U TUN MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 7034,
    "candidate": {
      "name": {
        "my": " ဦးဖေသောင်း",
        "en": " U PE THAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 14735,
    "candidate": {
      "name": {
        "my": "ဦးခွန်တင်ဝင်း",
        "en": "U KHUN TIN WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 16627,
    "candidate": {
      "name": {
        "my": "ဦးစောသောင်းဖေ",
        "en": "U SAW THAUNG PE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 19702,
    "candidate": {
      "name": {
        "my": "ဦးစောအုန်း",
        "en": "U SAW OHN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 19177,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": " U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 21346,
    "candidate": {
      "name": {
        "my": "နိုင်ခင်မောင်",
        "en": "Naing Khin Maung"
      }
    },
    "acclaim": "N",
    "party": "AMRDP",
    "year": 2010,
    "constituency ": "Mon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 24897,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": " U ZAW WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 23533,
    "candidate": {
      "name": {
        "my": "ဦးအေးလိှုင်",
        "en": " U AYE HLAING"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 1927,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှ",
        "en": "U TIN HLA"
      }
    },
    "acclaim": "N",
    "party": "KPP",
    "year": 2010,
    "constituency ": "Mon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 27302,
    "candidate": {
      "name": {
        "my": "ဦးညီညီထွန်း",
        "en": " U NYI NYI TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 21321,
    "candidate": {
      "name": {
        "my": "ဦးအေးကျော်",
        "en": "U AYE KYAW"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 25600,
    "candidate": {
      "name": {
        "my": "ဦးဇော်နိုင်ဦး",
        "en": "U ZAW NAING OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Mon-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 23552,
    "candidate": {
      "name": {
        "my": "ဦးခင်မြင့်",
        "en": " U KHIN MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Mon-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 24583,
    "candidate": {
      "name": {
        "my": "ုဦးမောင်မောင်တင်",
        "en": "U MAUNG MAUNG TIN"
      }
    },
    "acclaim": "N",
    "party": "NDPD",
    "year": 2010,
    "constituency ": "Rakhine-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 3969,
    "candidate": {
      "name": {
        "my": "ဦးလှတိုး",
        "en": "U HLA TOE"
      }
    },
    "acclaim": "N",
    "party": " Kaman National Progressive Party",
    "year": 2010,
    "constituency ": "Rakhine-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 40164,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဦးအေးမောင်",
        "en": "Dr. U AYE MAUNG"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 21757,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာချမ်းသာ",
        "en": "DR. CHAN THA"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 2896,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဦးကျော်",
        "en": "U TUN OO KYAW"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 16350,
    "candidate": {
      "name": {
        "my": "ဦးလှသာထွန်း",
        "en": " U HLA THA TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 7657,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်းညို",
        "en": " U AUNG TUN NYO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 45002,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်",
        "en": "U KYAW KYAW"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 6769,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လှကျော်",
        "en": " U SEIN HLA KYAW"
      }
    },
    "acclaim": "N",
    "party": "KMNDP2",
    "year": 2010,
    "constituency ": "Rakhine-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 2757,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဇောလှ",
        "en": "U KYAW ZAW HLA"
      }
    },
    "acclaim": "N",
    "party": "Rakhine State National Force Of Myanmar",
    "year": 2010,
    "constituency ": "Rakhine-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 14978,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်လေး",
        "en": "U MAUNG MAUNG LAY"
      }
    },
    "acclaim": "N",
    "party": "NDPD",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 12757,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ရင်",
        "en": " U KYAW YIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 9718,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလင်း",
        "en": " U TUN LINN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 41057,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်(ခ)အောင်ကျော်ဦး",
        "en": " U KHIN MAUNG (a) AUNG KYAW OO"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 3794,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဇော်",
        "en": "U MYINT ZAW"
      }
    },
    "acclaim": "N",
    "party": "Rakhine State National Force Of Myanmar",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 9995,
    "candidate": {
      "name": {
        "my": "ဦးစံသာအောင်",
        "en": " U SAN THAR AUNG"
      }
    },
    "party": "Mro Or Khami National Solidarity Organization",
    "year": 2010,
    "constituency ": "Rakhine-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 29806,
    "candidate": {
      "name": {
        "my": "ဦးစောဖြူ",
        "en": "U SAW PYU"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 16742,
    "candidate": {
      "name": {
        "my": " ဦးဦးလှမြင့်(ခ)ဦးဦးလှအေး",
        "en": " U OO HLA MYINT (a) U OO HLA AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 8999,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သိ်န်းကျော်",
        "en": " U MAUNG THEIN KYAW"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 44736,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ထွန်းအောင်",
        "en": "U KYAW TUN AUNG"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 22282,
    "candidate": {
      "name": {
        "my": "ဦးဦးစောလှ",
        "en": "U OO SAW HLA"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 10458,
    "candidate": {
      "name": {
        "my": "ဦးစောလှဖြူ",
        "en": "U SAW HLA PRU"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 57405,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လတ်",
        "en": "U KHIN MAUNG LATT"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 26661,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇံဝေ",
        "en": " U AUNG ZAN WAI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 14149,
    "candidate": {
      "name": {
        "my": "ဦးတလိုင်းရေှ",
        "en": " U TALAI SHAY"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 3710,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သာထွန်း",
        "en": " U MAUNG THAR TUN"
      }
    },
    "acclaim": "N",
    "party": "KMNDP2",
    "year": 2010,
    "constituency ": "Rakhine-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 38699,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "party": "NDPD",
    "year": 2010,
    "constituency ": "Rakhine-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 72177,
    "candidate": {
      "name": {
        "my": "ဦးဇာယာဒ်ရော်မံ(ခ)ဦးဌေးဝင်း",
        "en": " U HTAY WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 2079,
    "candidate": {
      "name": {
        "my": "ဦးဘသိန်း",
        "en": "U BA THEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 30887,
    "candidate": {
      "name": {
        "my": "ဦးတောယုဘေဒန်",
        "en": "U TAW YU BAYDEIN"
      }
    },
    "acclaim": "N",
    "party": "NDPD",
    "year": 2010,
    "constituency ": "Rakhine-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 64564,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သာခင်",
        "en": "U MAUNG THAR KHIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 1396,
    "candidate": {
      "name": {
        "my": "ဦးဖေါယေါ်ဇူလ်ဟောက်",
        "en": "U FARUL ZULHAK"
      }
    },
    "acclaim": "N",
    "party": "United Democratic Party - Kachin State",
    "year": 2010,
    "constituency ": "Rakhine-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 44126,
    "candidate": {
      "name": {
        "my": "ဥိးမူစတာဖာ ခါမီ",
        "en": "U MUSTAPHA KAMIL"
      }
    },
    "acclaim": "N",
    "party": "NDPD",
    "year": 2010,
    "constituency ": "Rakhine-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 48120,
    "candidate": {
      "name": {
        "my": "ဦးမောင်အေးထွန်း",
        "en": " U MAUNG AYE TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 10922,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကျော်ခိုင်",
        "en": " U MAUNG KYAW KHINE"
      }
    },
    "acclaim": "N",
    "party": "KMNDP2",
    "year": 2010,
    "constituency ": "Rakhine-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 2740,
    "candidate": {
      "name": {
        "my": "ဦးအလီဟူစိန်",
        "en": " U ALI HUSSEIN"
      }
    },
    "acclaim": "N",
    "party": "United Democratic Party - Kachin State",
    "year": 2010,
    "constituency ": "Rakhine-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 25640,
    "candidate": {
      "name": {
        "my": " ဦးထွန်းမင်း(ခ)ဦးအမ်တီယာစ်",
        "en": "U TUN MIN (a) U AMTIYIAS"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Rakhine-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 28648,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းတင်",
        "en": "U OHN TIN"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 24639,
    "candidate": {
      "name": {
        "my": "ဦးအောင်တင်သိန်း",
        "en": " U AUNG TIN THEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 13653,
    "candidate": {
      "name": {
        "my": "ဦးဘိုမြဦး",
        "en": "U BO MYA OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 62610,
    "candidate": {
      "name": {
        "my": "ဦးညိုထွန်း",
        "en": " U NYO TUN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 7752,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ငြိမ်း",
        "en": " U AUNG NYEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 17024,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဘစိန်",
        "en": " U AUNG BA SEIN"
      }
    },
    "acclaim": "N",
    "party": "Rakhine Nationals Development Party",
    "year": 2010,
    "constituency ": "Rakhine-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 54067,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လှူိင်",
        "en": "U MYINT HLAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Rakhine-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 43894,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းစိုး",
        "en": " U THEIN SOE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Rakhine-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 26850,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထိုက်",
        "en": "U AUNG HTEIT"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Sagaing-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 146934,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းတင့်",
        "en": " U WIN TINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 46267,
    "candidate": {
      "name": {
        "my": "ဦးလှသန်း",
        "en": "U HLA THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 263959,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဘိုကြီး(ခ)ဦးအောင်ငေွ",
        "en": " DR.BO GYEE (a) U AUNG NGWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 66615,
    "candidate": {
      "name": {
        "my": "ဦးပေါသောင်",
        "en": "U PAW THAUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 212718,
    "candidate": {
      "name": {
        "my": "၀ဏ္ဏကျော်ထင်ဦးဝင်းမြင့်",
        "en": "WANA KYAW TINT U WIN MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 65925,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ဝင်း",
        "en": "U Maung Maung Win"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 181381,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းမြင့်အောင်",
        "en": " DR. WIN MYINT AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 69631,
    "candidate": {
      "name": {
        "my": "ဒေါ်အုံးခင်",
        "en": "DAW OHN KHIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 24674,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ငြိမ်း",
        "en": "U Kyaw Nyein"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Sagaing-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 239023,
    "candidate": {
      "name": {
        "my": "ဦးကံညွန့်",
        "en": "U KAN NYUNT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 42733,
    "candidate": {
      "name": {
        "my": "ဦးကြည်",
        "en": "U KYI"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 73344,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": " U TUN MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 65325,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်လှ",
        "en": "U TIN MAUNG HLA"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 103991,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်အေး",
        "en": "U KHIN MAUNG AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 68537,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": " U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 102020,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလှိုင်",
        "en": "U THEIN HLAING"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 63003,
    "candidate": {
      "name": {
        "my": "ဦးမြဖေ",
        "en": "U MYA Pae"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 30695,
    "candidate": {
      "name": {
        "my": "ဦးဖူးခင်မောင်ကြီး",
        "en": "U PU KHIN MAUNG YI"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Sagaing-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 22247,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဝင်း",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 8846,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသန်း",
        "en": "U SOE THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 10497,
    "candidate": {
      "name": {
        "my": "ဦးထန်ခိုဟောက်",
        "en": " U HTAN KHO HAUK"
      }
    },
    "acclaim": "N",
    "party": "CPP",
    "year": 2010,
    "constituency ": "Sagaing-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 34072,
    "candidate": {
      "name": {
        "my": "ဦးရဲဝင်းဦး",
        "en": " U YE WIN OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 35660,
    "candidate": {
      "name": {
        "my": "\\- ဦးမောင်မောင်ကြီး",
        "en": " U MAUNG MAUNG GYEE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 31092,
    "candidate": {
      "name": {
        "my": " ဦးအောင်ချစ်လွင်",
        "en": " U AUNG CHIT LWIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 22485,
    "candidate": {
      "name": {
        "my": "ဒေါ်သောင်းရီ",
        "en": " DAW THAUNG YEE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 39410,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်ကို",
        "en": "U MYAT KO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Sagaing-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 8630,
    "candidate": {
      "name": {
        "my": "ဦးလာသော်ကဲ",
        "en": "U LA THAW KE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Sagaing-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 244251,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်လင်း",
        "en": " U NYAN LINN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 103959,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ကျော်",
        "en": " U KHUN KYAW"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 75234,
    "candidate": {
      "name": {
        "my": "ဦးစန်းယု",
        "en": "U SAN YU"
      }
    },
    "acclaim": "N",
    "party": "INDP",
    "year": 2010,
    "constituency ": "Shan-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 17801,
    "candidate": {
      "name": {
        "my": "ဦးမိှုင်း",
        "en": " U HMAING"
      }
    },
    "acclaim": "N",
    "party": "KNP2",
    "year": 2010,
    "constituency ": "Shan-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 94071,
    "candidate": {
      "name": {
        "my": "စိုင်းကျော်ဇောသန်း",
        "en": "U Sai Kyaw Zaw Than"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 73586,
    "candidate": {
      "name": {
        "my": "ဦးဆိုင်းဆိုင်",
        "en": " U SAING SAI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 15897,
    "candidate": {
      "name": {
        "my": "ဦးကောင်စစ်",
        "en": "U KAUNG SIT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 68120,
    "candidate": {
      "name": {
        "my": "စိုင်းခမ်းလိုင်",
        "en": "Sai Kham Hlaing"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 81105,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုင်းမောက်ခမ်း",
        "en": " DR.SAI MAUK KHAM"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 14270,
    "candidate": {
      "name": {
        "my": "ဦးပေါ်ဦး",
        "en": "U PAW OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 26950,
    "candidate": {
      "name": {
        "my": "ဦးလော်ဆင်ကွမ်း",
        "en": " U LAW SIN KWAN"
      }
    },
    "acclaim": "N",
    "party": "KDAUP",
    "year": 2010,
    "constituency ": "Shan-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 123458,
    "candidate": {
      "name": {
        "my": "စိုင်းသန့်ဇင်",
        "en": "Sai Thant Zin"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 89318,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ပွင့်",
        "en": " U KHUN PWINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 41252,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းဦး",
        "en": "U AUNG THAN OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 47876,
    "candidate": {
      "name": {
        "my": "စိုင်းဖိုးမြတ်",
        "en": "Sai Phoe Myat"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 65720,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းဦး",
        "en": " U AUNG THAN OO"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 15361,
    "candidate": {
      "name": {
        "my": "ဦးယောရှု",
        "en": " U YAW SHU"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 37501,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": " U KYAW AYE"
      }
    },
    "acclaim": "N",
    "party": "TNP",
    "year": 2010,
    "constituency ": "Shan-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 5842,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ဆော(ခ)ဦးစောနန္ဒီ",
        "en": " U AIK SAW (a) U SAWNANDI"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Shan-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 17230,
    "candidate": {
      "name": {
        "my": "စိုင်းကံညွန့်",
        "en": "Sai Kan Nyunt"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 55188,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုင်းဆိုင်ကျောက်ဆမ်",
        "en": "DR. SAI SAING KYAUK SAM"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 3224,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမုန့်ရွက်",
        "en": " U SAI MON YWET"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 41224,
    "candidate": {
      "name": {
        "my": "နန်းငေွှမ",
        "en": "Nan Ngwe Mya"
      }
    },
    "acclaim": "N",
    "party": "SNDP",
    "year": 2010,
    "constituency ": "Shan-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 79494,
    "candidate": {
      "name": {
        "my": "ဦးဝီလ်ဆင်မိုး",
        "en": " U WILSON MOE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 14814,
    "candidate": {
      "name": {
        "my": "ဦးစပ်သင်စိုင်",
        "en": " U SAO THIN SAI"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 32276,
    "candidate": {
      "name": {
        "my": "ဦးရှုမောင်",
        "en": " U SHU MAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 11924,
    "candidate": {
      "name": {
        "my": "ဦးဖေဝင်း",
        "en": " U PE WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Shan-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 30221,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": " U MYA THAN"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Shan-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်းထွန်း",
        "en": " U NAY WIN TUN"
      }
    },
    "acclaim": "Y",
    "party": "PNO",
    "year": 2010,
    "constituency ": "Shan-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကျော်",
        "en": " U TUN KYAW"
      }
    },
    "acclaim": "Y",
    "party": "TNP",
    "year": 2010,
    "constituency ": "Shan-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးလျူကွက်ရိှ",
        "en": " U LU KWET SHU"
      }
    },
    "acclaim": "Y",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Shan-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 1703,
    "candidate": {
      "name": {
        "my": "ဦးညီပလုပ်",
        "en": "U NYI PALOKE"
      }
    },
    "acclaim": "N",
    "party": "WNUP",
    "year": 2010,
    "constituency ": "Shan-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 23542,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်ပေါင်နပ်",
        "en": " U SAI PAO NAT"
      }
    },
    "acclaim": "N",
    "party": "WDP",
    "year": 2010,
    "constituency ": "Shan-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 6334,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းပို",
        "en": "U Tun Po"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Tanintharyi-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 11209,
    "candidate": {
      "name": {
        "my": " ဦးစိုးသက်",
        "en": "U SOE THET"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 6886,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ရေွှ",
        "en": "U NYAN SHWE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 7865,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဌေး",
        "en": "U Aung Htay"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Tanintharyi-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 15858,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဆေွ",
        "en": " U THAN SWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 7865,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမြင့်",
        "en": " U THEIN MYINT"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 37098,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်မြင့်",
        "en": " U TIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 18443,
    "candidate": {
      "name": {
        "my": "ဦးလှသန်း",
        "en": " U HLA THAN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 24830,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": " U AUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 16462,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထိန်",
        "en": "U WIN HTEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 943,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သိမ်း",
        "en": " U AUNG THEIN"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Tanintharyi-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 10848,
    "candidate": {
      "name": {
        "my": "ဦးနေဖူးဗဆေွ",
        "en": "U NAY PHU BA SWE"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Tanintharyi-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 16829,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စိန်",
        "en": "U MAUNG SEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 15470,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်ဦး",
        "en": " U KYAW KYAW OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 44032,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရန်",
        "en": " U TIN YAN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 17410,
    "candidate": {
      "name": {
        "my": "ဦးတန်စ",
        "en": "U TAN SA"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 28943,
    "candidate": {
      "name": {
        "my": "ဦးမိုးမြင့်",
        "en": " U MOE MYINT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 21317,
    "candidate": {
      "name": {
        "my": "ဦးဟန်တင်",
        "en": " U HAN TIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျားမောင်(ခ)ဦးမောင်ကျား",
        "en": "U KYAR MAUNG (a) U MAUNG KYAR"
      }
    },
    "acclaim": "Y",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 46541,
    "candidate": {
      "name": {
        "my": "ဦးလှစိုး",
        "en": " U HLA SOE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 10273,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်တင်အေး",
        "en": " U NYUNT TIN AYE"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမြင့်",
        "en": "U THAN MYINT"
      }
    },
    "acclaim": "Y",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 29125,
    "candidate": {
      "name": {
        "my": "ဦးလှအေး",
        "en": "U HLA AYE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 15206,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": " U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 21133,
    "candidate": {
      "name": {
        "my": "ဦးတိုးဝင်း",
        "en": "U TOE WIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Tanintharyi-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 10253,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးလွင်",
        "en": " U MYO LWIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Tanintharyi-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 12278,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်မင်း",
        "en": "U Kyaw Kyaw Min"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Yangon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 106074,
    "candidate": {
      "name": {
        "my": "ဦးတင်ခ",
        "en": "U TIN KHA"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 30406,
    "candidate": {
      "name": {
        "my": "ဒေါ်စောစောဦး",
        "en": "DAW SAW SAW OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 114548,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်ဝိုင်းကြည်",
        "en": "DAW KHIN WAING KYI"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 5728,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမင်းလတ်",
        "en": "U TUN MIN LATT"
      }
    },
    "acclaim": "N",
    "party": "DAPP",
    "year": 2010,
    "constituency ": "Yangon-1",
    "parliament_code": "AMH"
  },
  {
    "votes": 27265,
    "candidate": {
      "name": {
        "my": "ဦးလေးစိုး",
        "en": "U LAY SOE"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Yangon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 122086,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်တင်",
        "en": "U NYUNT TIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 37329,
    "candidate": {
      "name": {
        "my": "ဦးဆေွသန့်ကို",
        "en": "U SWE THANT KO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 90333,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်",
        "en": "U TUN AUNG"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-2",
    "parliament_code": "AMH"
  },
  {
    "votes": 17906,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြတ်ဦး",
        "en": "U KYAW MYAT OO"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 86713,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမောင်သင်း",
        "en": "DR. MAUNG THINN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 11535,
    "candidate": {
      "name": {
        "my": "ဦးဇော်အေးအောင်",
        "en": "U ZAW AYE AUNG"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Yangon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 92338,
    "candidate": {
      "name": {
        "my": "ဦးဖုန်းမြင့်အောင်",
        "en": "U PHONE MYINT AUNG"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-3",
    "parliament_code": "AMH"
  },
  {
    "votes": 20344,
    "candidate": {
      "name": {
        "my": "ဦးသီဟမင်းအောင်",
        "en": "U THIHA MIN AUNG"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 78714,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်သူ (ခ)ကျော်ဇင်ညွန့်",
        "en": "U MYAT THU (a) KYAW ZIN NYUNT"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 27354,
    "candidate": {
      "name": {
        "my": "ဦးတင်နိုင်",
        "en": "U TIN NAING"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Yangon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 83558,
    "candidate": {
      "name": {
        "my": "Dr. မြတ်ဉာဏစိုး",
        "en": "DR. MYAT NYAN SOE"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-4",
    "parliament_code": "AMH"
  },
  {
    "votes": 24683,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွန်း",
        "en": "U YE TUN"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Yangon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 23709,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သိန်း",
        "en": "U KYAW THEIN"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 81950,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနောင်",
        "en": "U WIN NAUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 69407,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းထွန်း",
        "en": "U THAUNG TUN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-5",
    "parliament_code": "AMH"
  },
  {
    "votes": 18087,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U Win Myint"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 7313,
    "candidate": {
      "name": {
        "my": "ဦးစန်းမြင့်",
        "en": "U San Myint"
      }
    },
    "acclaim": "N",
    "party": "Independent Candidates",
    "year": 2010,
    "constituency ": "Yangon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 17815,
    "candidate": {
      "name": {
        "my": "ဦးသော်လင်းထွန်း",
        "en": "U THAW LIN TUN"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Yangon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 70317,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမျိုးသန်းတင်",
        "en": "DR. MYO THAN TIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 77541,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာတင်ရေွှ",
        "en": "DR. TIN SHWE"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-6",
    "parliament_code": "AMH"
  },
  {
    "votes": 106069,
    "candidate": {
      "name": {
        "my": "ဦးမြငြိမ်း",
        "en": "U MYA NYEIN"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 27592,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဇော်ညွှန့်",
        "en": "U THEIN ZAW NYUNT"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 32678,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်း",
        "en": "U Aung Htun"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-7",
    "parliament_code": "AMH"
  },
  {
    "votes": 137628,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်ကြည်",
        "en": "DR. MYINT KYI"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 85689,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာလှမျိုး",
        "en": "DR. HLA MYO"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 38669,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမြင့်မောင်",
        "en": "U Htay Myint Aung"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-8",
    "parliament_code": "AMH"
  },
  {
    "votes": 173208,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်ဆေွ",
        "en": "DR. KHIN SHWE"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 15461,
    "candidate": {
      "name": {
        "my": "ဦးမန်းအုန်းမြင့်သိန်း",
        "en": "Mahn Ohn Myint Thein"
      }
    },
    "acclaim": "N",
    "party": "KPP",
    "year": 2010,
    "constituency ": "Yangon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 7332,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဝင်း",
        "en": "U TIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "party": "MPP",
    "year": 2010,
    "constituency ": "Yangon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 38830,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U Kyaw Myint"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-9",
    "parliament_code": "AMH"
  },
  {
    "votes": 51903,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာ ကျော်မင်း",
        "en": "Dr. Kyaw Min"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 116682,
    "candidate": {
      "name": {
        "my": "ဦးနု",
        "en": "U NU"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 39018,
    "candidate": {
      "name": {
        "my": "ဦးသိုက်ထွန်း",
        "en": "U THAIK TUN"
      }
    },
    "acclaim": "N",
    "party": "DP",
    "year": 2010,
    "constituency ": "Yangon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 17448,
    "candidate": {
      "name": {
        "my": " ဦးတင်ထွန်းအောင်",
        "en": "U TIN TUN AUNG"
      }
    },
    "acclaim": "N",
    "party": "NPAL",
    "year": 2010,
    "constituency ": "Yangon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 8532,
    "candidate": {
      "name": {
        "my": "ဦးနေမျိုးဝေ",
        "en": "U NAY MYO WAI"
      }
    },
    "acclaim": "N",
    "party": " Peace and Diversity Party",
    "year": 2010,
    "constituency ": "Yangon-10",
    "parliament_code": "AMH"
  },
  {
    "votes": 68761,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်စိုး",
        "en": "U AUNG KYAW SOE"
      }
    },
    "acclaim": "N",
    "party": "T8GSYOM",
    "year": 2010,
    "constituency ": "Yangon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 60025,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဆေွ",
        "en": "U Tint Swe"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 146234,
    "candidate": {
      "name": {
        "my": "ဦးတင်ယု",
        "en": "U TIN YU"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 61101,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်",
        "en": "U THANT ZIN"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-11",
    "parliament_code": "AMH"
  },
  {
    "votes": 213443,
    "candidate": {
      "name": {
        "my": "ဦးဆေွအောင်",
        "en": "U SWE AUNG"
      }
    },
    "acclaim": "N",
    "party": "USADP",
    "year": 2010,
    "constituency ": "Yangon-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 71805,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်အောင်",
        "en": "U Nyunt Aung"
      }
    },
    "acclaim": "N",
    "party": "NUP",
    "year": 2010,
    "constituency ": "Yangon-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 44590,
    "candidate": {
      "name": {
        "my": "ဦးသီဟစိုး",
        "en": "U THIHA SOE"
      }
    },
    "acclaim": "N",
    "party": "NDF",
    "year": 2010,
    "constituency ": "Yangon-12",
    "parliament_code": "AMH"
  },
  {
    "votes": 5559,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန္ဒာဦး",
        "en": "DAW SANDAR OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": " Peace and Diversity Party",
    "constituency": "MMR017024"
  },
  {
    "votes": 4814,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုး",
        "en": "U AUNG MYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017024"
  },
  {
    "votes": 13063,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U Khin Maung Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017024"
  },
  {
    "votes": 80972,
    "candidate": {
      "name": {
        "my": "ဦးမြသိန်း",
        "en": "U MYA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017024"
  },
  {
    "votes": 37900,
    "candidate": {
      "name": {
        "my": "ဦးဌေးညွန့်",
        "en": "U HTAY NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017024"
  },
  {
    "votes": 67142,
    "candidate": {
      "name": {
        "my": "ဦးကြည်တင့်",
        "en": "U KYI TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017022"
  },
  {
    "votes": 27781,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမောင်",
        "en": "U SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017022"
  },
  {
    "votes": 9361,
    "candidate": {
      "name": {
        "my": "ဦးလှငေွ",
        "en": "U HLA NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017026"
  },
  {
    "votes": 62940,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017026"
  },
  {
    "votes": 27237,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကြည",
        "en": "U TUN KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017026"
  },
  {
    "votes": 54641,
    "candidate": {
      "name": {
        "my": "ဦးစောဂျိန်း",
        "en": "U SAW JAMES"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017015"
  },
  {
    "votes": 43888,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းညွန့်",
        "en": "U THAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017015"
  },
  {
    "votes": 142704,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဦး",
        "en": "U HTAY OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017008"
  },
  {
    "votes": 33284,
    "candidate": {
      "name": {
        "my": "ဦးစောထွန်း",
        "en": "U SAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017008"
  },
  {
    "votes": 107348,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်စိုး",
        "en": "U MAUNG MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017013"
  },
  {
    "votes": 17013,
    "candidate": {
      "name": {
        "my": "ဦးတင်အောင်",
        "en": "U TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017013"
  },
  {
    "votes": 46115,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဝင်း",
        "en": "U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017002"
  },
  {
    "votes": 33085,
    "candidate": {
      "name": {
        "my": "ဦးသန်းတင်",
        "en": "U THAN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017002"
  },
  {
    "votes": 4665,
    "candidate": {
      "name": {
        "my": "စောကဲနက်ဝေ",
        "en": "U SAW KENNETH WAI THAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017025"
  },
  {
    "votes": 10721,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR017025"
  },
  {
    "votes": 5382,
    "candidate": {
      "name": {
        "my": "ဦးအောင်အောင်",
        "en": "U AUNG AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017025"
  },
  {
    "votes": 56749,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဝင်း",
        "en": "U THAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017025"
  },
  {
    "votes": 14636,
    "candidate": {
      "name": {
        "my": "ဦးသန်းညွန့်",
        "en": "U THAN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017025"
  },
  {
    "votes": 5437,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်း",
        "en": "U KYAW SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017025"
  },
  {
    "votes": 25184,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာတင်မောင်ကြိုင",
        "en": "DR.TIN MAUNG KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017012"
  },
  {
    "votes": 24617,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017012"
  },
  {
    "votes": 50392,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017007"
  },
  {
    "votes": 31555,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းမာန",
        "en": "U OHN MARN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017007"
  },
  {
    "votes": 18247,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဝင်း",
        "en": "U Tint Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017005"
  },
  {
    "votes": 71994,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးသူရ",
        "en": "DR.SOE THUYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017005"
  },
  {
    "votes": 31064,
    "candidate": {
      "name": {
        "my": "ဦးမင်းလှရှိန််",
        "en": "U MIN HLA SHEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017005"
  },
  {
    "votes": 10124,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းလှုိင်",
        "en": "U OHN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR017016"
  },
  {
    "votes": 96951,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ထွန်း",
        "en": "U ZAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017016"
  },
  {
    "votes": 63245,
    "candidate": {
      "name": {
        "my": "ဦးအောင်အောင",
        "en": "U AUNG AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017016"
  },
  {
    "votes": 41527,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်ဦး",
        "en": "U MYAT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017016"
  },
  {
    "votes": 34288,
    "candidate": {
      "name": {
        "my": "ဦးဘဌေး",
        "en": "U BA HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017010"
  },
  {
    "votes": 1067,
    "candidate": {
      "name": {
        "my": "ဦးရေွှသောင်း",
        "en": "U SHWE THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017010"
  },
  {
    "votes": 21916,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးလွင",
        "en": "DR. SOE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR017010"
  },
  {
    "votes": 123824,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းညွန့်",
        "en": "U THEIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017019"
  },
  {
    "votes": 4877,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR017019"
  },
  {
    "votes": 5089,
    "candidate": {
      "name": {
        "my": "ဦးကျော်လင်း",
        "en": "U KYAW LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017019"
  },
  {
    "votes": 22514,
    "candidate": {
      "name": {
        "my": "ဦးပိုက်",
        "en": "U PAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017019"
  },
  {
    "votes": 19856,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR017018"
  },
  {
    "votes": 97692,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုလွင်",
        "en": "U KO KO LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017018"
  },
  {
    "votes": 36712,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြ",
        "en": "U TIN MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017018"
  },
  {
    "votes": 81996,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဌေး",
        "en": "U THAN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017011"
  },
  {
    "votes": 24501,
    "candidate": {
      "name": {
        "my": "ဦးလှကြိုင",
        "en": "U HLA KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017011"
  },
  {
    "votes": 3899,
    "candidate": {
      "name": {
        "my": "ဦးဌေးအောင်",
        "en": "U HTAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR017014"
  },
  {
    "votes": 18309,
    "candidate": {
      "name": {
        "my": "ဦးမန်းသောင်းကြွယ်",
        "en": "U MAHN THAUNG KYWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017014"
  },
  {
    "votes": 18469,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြင်",
        "en": "U Aung Kyin"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017014"
  },
  {
    "votes": 74865,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဆန်း",
        "en": "U TINT SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017014"
  },
  {
    "votes": 14404,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိမ်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017014"
  },
  {
    "votes": 13544,
    "candidate": {
      "name": {
        "my": "ဦးမန်းထွန်းတင်",
        "en": "U MAHN TUN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR017004"
  },
  {
    "votes": 8921,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသူစိုး",
        "en": "U Win Thu Soe"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017004"
  },
  {
    "votes": 26397,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်သန်းမြင",
        "en": "DAW KHIN THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017004"
  },
  {
    "votes": 12659,
    "candidate": {
      "name": {
        "my": "ဦးဘသော",
        "en": "U BA THAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017004"
  },
  {
    "votes": 10623,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်",
        "en": "U MAUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR017021"
  },
  {
    "votes": 50054,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြသန်း",
        "en": "U AUNG MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017021"
  },
  {
    "votes": 39919,
    "candidate": {
      "name": {
        "my": "ဦးဘငြိမ်း",
        "en": "U BA NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017021"
  },
  {
    "votes": 57054,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိ်န်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017020"
  },
  {
    "votes": 61992,
    "candidate": {
      "name": {
        "my": "ဦးမန်းမောင်မောင်ဉာဏ",
        "en": "U MAHN MAUNG MAUNG NYAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017020"
  },
  {
    "votes": 32652,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017001"
  },
  {
    "votes": 90676,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017001"
  },
  {
    "votes": 34558,
    "candidate": {
      "name": {
        "my": "ဦးမြတ်အောင်််",
        "en": "U MYAT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017001"
  },
  {
    "votes": 97669,
    "candidate": {
      "name": {
        "my": "ဦးစိုးနိုင်",
        "en": "U SOE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017023"
  },
  {
    "votes": 34101,
    "candidate": {
      "name": {
        "my": "ဦးစောကျော်ထွန််း",
        "en": "U SAW KYAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017023"
  },
  {
    "votes": 11027,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးကျော်",
        "en": "U Myo Kyaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017003"
  },
  {
    "votes": 42217,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသန်း",
        "en": "U WIN THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017003"
  },
  {
    "votes": 42217,
    "candidate": {
      "name": {
        "my": "ဦးငြိမ်းမောင",
        "en": "U NYEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017003"
  },
  {
    "votes": 74046,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်အေး",
        "en": "U KHIN MAUNG AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017017"
  },
  {
    "votes": 60548,
    "candidate": {
      "name": {
        "my": "ဦးသန်းအောင်",
        "en": "U THAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017017"
  },
  {
    "votes": 5480,
    "candidate": {
      "name": {
        "my": "ဦးသက်နိုင်ဦး",
        "en": "U Thet Naing Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR017006"
  },
  {
    "votes": 53864,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်တင",
        "en": "U MAUNG MAUNG TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017006"
  },
  {
    "votes": 37823,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017006"
  },
  {
    "votes": 77908,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွတ",
        "en": "U TIN HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR017009"
  },
  {
    "votes": 14555,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR017009"
  },
  {
    "votes": 88265,
    "candidate": {
      "name": {
        "my": "ဦးကြင်သိန်း",
        "en": "U KYIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007001"
  },
  {
    "votes": 30966,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုးဝင်း",
        "en": "U Aung Myo Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007001"
  },
  {
    "votes": 36906,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007001"
  },
  {
    "votes": 44773,
    "candidate": {
      "name": {
        "my": "ဦးမင်းဆေွွ",
        "en": "U MIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007007"
  },
  {
    "votes": 23462,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ခ",
        "en": "U KYI KHA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007007"
  },
  {
    "votes": 8835,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007007"
  },
  {
    "votes": 9665,
    "candidate": {
      "name": {
        "my": "ဦးစန်းကျော်",
        "en": "U San Kyaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007003"
  },
  {
    "votes": 62199,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းစိန",
        "en": "U WIN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007003"
  },
  {
    "votes": 27278,
    "candidate": {
      "name": {
        "my": "ဦးစိန်သောင်း",
        "en": "U SEIN THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007003"
  },
  {
    "votes": 13706,
    "candidate": {
      "name": {
        "my": "ဦးသူရမျိုးခင်",
        "en": "U THUYA MYO KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007011"
  },
  {
    "votes": 28467,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ဆေွဝင်း",
        "en": "U NYAN SWE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007011"
  },
  {
    "votes": 11381,
    "candidate": {
      "name": {
        "my": "ဦးမင်းမင်းဇော်",
        "en": "U Min Min Zaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007006"
  },
  {
    "votes": 58089,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာခင်မောင်ဆေ",
        "en": "?DR.KHIN MAUNG SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007006"
  },
  {
    "votes": 43013,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရီ",
        "en": "U TUN YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007006"
  },
  {
    "votes": 8352,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇော်ထွဋ်",
        "en": "U Aung Zaw Htut"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007005"
  },
  {
    "votes": 42956,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007005"
  },
  {
    "votes": 23113,
    "candidate": {
      "name": {
        "my": "ဦးသန်းကျော်",
        "en": "U THAN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007005"
  },
  {
    "votes": 2380,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းအောင်ကြည်",
        "en": "U SAN AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR007005"
  },
  {
    "votes": 48792,
    "candidate": {
      "name": {
        "my": "ဦးညီညီမြင့်််",
        "en": "U NYI NYI MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007013"
  },
  {
    "votes": 19267,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007013"
  },
  {
    "votes": 3379,
    "candidate": {
      "name": {
        "my": "ဦးမင်းသိန်းဇော်",
        "en": "U MIN THEIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR007013"
  },
  {
    "votes": 6482,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိုး",
        "en": "U TIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR007012"
  },
  {
    "votes": 83536,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်လှုိင",
        "en": "U NYUNT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007012"
  },
  {
    "votes": 27842,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဌေး",
        "en": "U THAN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007012"
  },
  {
    "votes": 2394,
    "candidate": {
      "name": {
        "my": "စောဆင်းဘက်",
        "en": "Saw Sin Bak"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007008"
  },
  {
    "votes": 15091,
    "candidate": {
      "name": {
        "my": "ဦးတင်ရေွှ",
        "en": "U TIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007008"
  },
  {
    "votes": 17745,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်း",
        "en": "U TIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007008"
  },
  {
    "votes": 6342,
    "candidate": {
      "name": {
        "my": "ဦးစောမင်ရွယ်",
        "en": "U SAW MIN YWEI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR007014"
  },
  {
    "votes": 31879,
    "candidate": {
      "name": {
        "my": "ဦးအံ့ကြီး",
        "en": "U ANT GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007014"
  },
  {
    "votes": 6016,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007014"
  },
  {
    "votes": 85932,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မင်း",
        "en": "U AUNG MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007009"
  },
  {
    "votes": 35509,
    "candidate": {
      "name": {
        "my": "ဦးငြိမ်းအောင်",
        "en": "U NYEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007009"
  },
  {
    "votes": 6016,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007002"
  },
  {
    "votes": 17757,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး",
        "en": "U TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007002"
  },
  {
    "votes": 11380,
    "candidate": {
      "name": {
        "my": "ဦးနေလင်းအောင်",
        "en": "U Nay Lin Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007004"
  },
  {
    "votes": 43562,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ရေွှ",
        "en": "U MYINT SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007004"
  },
  {
    "votes": 17833,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းလှုိင်(ခ)ဦးထွန်းထွန်း",
        "en": "U TUN HLAING (a) U TUN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007004"
  },
  {
    "votes": 8554,
    "candidate": {
      "name": {
        "my": "ဦးစိုးကြည်",
        "en": "U Soe Kyi"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR007010"
  },
  {
    "votes": 54180,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးသန့်",
        "en": "U MYO THANT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR007010"
  },
  {
    "votes": 30352,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ခင်",
        "en": " U AUNG KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR007010"
  },
  {
    "votes": 19189,
    "candidate": {
      "name": {
        "my": "ဒေါ်ချိုချိုကျော်ငြိမ်း",
        "en": "DAW CHO CHO KYAW NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008014"
  },
  {
    "votes": 27204,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်",
        "en": "U THANT ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008014"
  },
  {
    "votes": 13285,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုး",
        "en": "U MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008014"
  },
  {
    "votes": 4715,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး",
        "en": "U TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008008"
  },
  {
    "votes": 35915,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းမြင့်",
        "en": "DR. WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008008"
  },
  {
    "votes": 19356,
    "candidate": {
      "name": {
        "my": "ဦးညွှန့်တင််",
        "en": "U NYUNT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008008"
  },
  {
    "votes": 12704,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်စိုး",
        "en": "U KYAW SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR008008"
  },
  {
    "votes": 9507,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရီ",
        "en": "U Khin Maung Ye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR008009"
  },
  {
    "votes": 31875,
    "candidate": {
      "name": {
        "my": "ဦးမောင်တိုး",
        "en": "U KHIN MAUNG TOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008009"
  },
  {
    "votes": 12100,
    "candidate": {
      "name": {
        "my": "ဦးအေးချို",
        "en": "U AYE CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008009"
  },
  {
    "votes": 24861,
    "candidate": {
      "name": {
        "my": "ဦးနန်းဦး",
        "en": "U NAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008013"
  },
  {
    "votes": 13856,
    "candidate": {
      "name": {
        "my": "ဦးဟန်စော",
        "en": "U HAN SAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008013"
  },
  {
    "votes": 8148,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်မြင့််",
        "en": "U AUNG KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR008013"
  },
  {
    "votes": 17936,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လိှုင််",
        "en": "U SEIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR008013"
  },
  {
    "votes": 57455,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွဋ်ဦး",
        "en": "U YE HTUT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008012"
  },
  {
    "votes": 26445,
    "candidate": {
      "name": {
        "my": "ဒေါ်အေးမြင့်",
        "en": "DAW AYE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008012"
  },
  {
    "votes": 11368,
    "candidate": {
      "name": {
        "my": "နန်းကြူသန်းဝင်း",
        "en": "Nan Khyu Than Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR008010"
  },
  {
    "votes": 36970,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထေွး",
        "en": "U TIN HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008010"
  },
  {
    "votes": 12936,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှ",
        "en": "U TIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008010"
  },
  {
    "votes": 16397,
    "candidate": {
      "name": {
        "my": "ဒေါ်စန်းစန်းမော်",
        "en": "DAW SAN SAN MAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008003"
  },
  {
    "votes": 51116,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဝင်း",
        "en": "U KHIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008003"
  },
  {
    "votes": 13654,
    "candidate": {
      "name": {
        "my": "ဦးညွှန့်ဝေ",
        "en": "U NYUNT WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008003"
  },
  {
    "votes": 8212,
    "candidate": {
      "name": {
        "my": "ဒေါ်အေးအေးဝင်း(ခ)မစု",
        "en": "DAW AYE AYE WIN (a) MA SU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008002"
  },
  {
    "votes": 25742,
    "candidate": {
      "name": {
        "my": "ဦးအေးဟန်",
        "en": "U AYE HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008002"
  },
  {
    "votes": 20981,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008002"
  },
  {
    "votes": 13761,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်းအောင်",
        "en": "U NAY WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008004"
  },
  {
    "votes": 41869,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်သိန်း",
        "en": "U AUNG MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008004"
  },
  {
    "votes": 17676,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှုိင်",
        "en": "U TIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008004"
  },
  {
    "votes": 21985,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ဦး",
        "en": "U AUNG NAING OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008001"
  },
  {
    "votes": 60405,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008001"
  },
  {
    "votes": 28086,
    "candidate": {
      "name": {
        "my": "ဦးသာထူး",
        "en": "U THAR HTOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008001"
  },
  {
    "votes": 17943,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသန်း",
        "en": "U SOE THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008006"
  },
  {
    "votes": 34980,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဒေါ်အေးမြင့်",
        "en": "DR. DAW AYE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008006"
  },
  {
    "votes": 10927,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုး",
        "en": "U MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008006"
  },
  {
    "votes": 42471,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်(ခ)ခင်မောင်ဝင်း",
        "en": "U MAUNG MAUNG (a) U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008007"
  },
  {
    "votes": 26082,
    "candidate": {
      "name": {
        "my": "ဦးစန်းသွင်",
        "en": "U SAN THWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008007"
  },
  {
    "votes": 18580,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဇော်လတ်(ခ)ဦးခင်မောင်ချင်း",
        "en": "U ZAW ZAW LATT (a) U KHIN MAUNG CHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR008005"
  },
  {
    "votes": 31504,
    "candidate": {
      "name": {
        "my": "ဦးစိုးအောင့််",
        "en": "U SOE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008005"
  },
  {
    "votes": 15184,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008005"
  },
  {
    "votes": 9305,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သိန်း",
        "en": "U KHIN MAUNG THEIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR008011"
  },
  {
    "votes": 22347,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR008011"
  },
  {
    "votes": 6420,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ထွန်း",
        "en": "U CHIT TUN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR008011"
  },
  {
    "votes": 7122,
    "candidate": {
      "name": {
        "my": "ဦးဆိုင်းလျန်ကြူး",
        "en": "U SAING LANG KYEU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004001"
  },
  {
    "votes": 1015,
    "candidate": {
      "name": {
        "my": "ဦးဆန်ထန်း",
        "en": "U SAN TAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004001"
  },
  {
    "votes": 8975,
    "candidate": {
      "name": {
        "my": "ဦးခွန်လိန်း",
        "en": "U Khun Lain"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004001"
  },
  {
    "votes": 4807,
    "candidate": {
      "name": {
        "my": "ဦးထန်ဟဲ",
        "en": "U TIALHNIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004002"
  },
  {
    "votes": 10400,
    "candidate": {
      "name": {
        "my": "ဦးငွန်မောင်း",
        "en": "U NGUN MAO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004002"
  },
  {
    "votes": 1501,
    "candidate": {
      "name": {
        "my": "ဦးနလေွ",
        "en": "U NA LWEI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004002"
  },
  {
    "votes": 1476,
    "candidate": {
      "name": {
        "my": "ဦးလီယန်တေး",
        "en": "U LIAN TIL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004003"
  },
  {
    "votes": 12075,
    "candidate": {
      "name": {
        "my": " ဒေါ်ဇာတလဲမ်း",
        "en": "DAW ZADA LAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004003"
  },
  {
    "votes": 8295,
    "candidate": {
      "name": {
        "my": "ဦးငွန်ဟရမ်း",
        "en": "U NGUN HRAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004003"
  },
  {
    "votes": 3288,
    "candidate": {
      "name": {
        "my": "သူရအောင်ကို",
        "en": "THURA AUNG KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004008"
  },
  {
    "votes": 1130,
    "candidate": {
      "name": {
        "my": "ဦးမနထန် (ခ)ကျော်ဝင်း",
        "en": "U MANA HTANG(a) U KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004008"
  },
  {
    "votes": 1274,
    "candidate": {
      "name": {
        "my": "ဦးလိန်းကီး",
        "en": "U LEIN KEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004008"
  },
  {
    "votes": 2360,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ယဉ်",
        "en": "U KHIN MAUNG YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004008"
  },
  {
    "votes": 8436,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာရိုအုပ်",
        "en": "DR.RO OKE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004007"
  },
  {
    "votes": 7910,
    "candidate": {
      "name": {
        "my": "ဦးလှျံကြယ်",
        "en": "U LIAN CE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004007"
  },
  {
    "votes": 6198,
    "candidate": {
      "name": {
        "my": "ဦးရေွှထန်း",
        "en": "U SHWE HTANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR004007"
  },
  {
    "votes": 6877,
    "candidate": {
      "name": {
        "my": "ဦးထန်းလိန်း",
        "en": "U HTAN LEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004006"
  },
  {
    "votes": 1056,
    "candidate": {
      "name": {
        "my": "ဦးထန်းမာ",
        "en": "U HTAN MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR004006"
  },
  {
    "votes": 4074,
    "candidate": {
      "name": {
        "my": "ဦးလင်းဟုန်း",
        "en": "U LEIN HONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004006"
  },
  {
    "votes": 2029,
    "candidate": {
      "name": {
        "my": "ဦးဖေွးအား",
        "en": "U PHWE AA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004006"
  },
  {
    "votes": 2420,
    "candidate": {
      "name": {
        "my": "ဦးရှိန်းထွန်း",
        "en": "U SHING THUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004006"
  },
  {
    "votes": 4292,
    "candidate": {
      "name": {
        "my": "ဦးစိန်သာအောင်",
        "en": "U SEIN THAR AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Mro Or Khami National Solidarity Organization (MKNSO)",
    "constituency": "MMR004009"
  },
  {
    "votes": 6129,
    "candidate": {
      "name": {
        "my": "ဦးပြည်ယိုး",
        "en": "U PYI YOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004009"
  },
  {
    "votes": 2345,
    "candidate": {
      "name": {
        "my": "ဦးထန့်ဆေွ",
        "en": "U HTAUNT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004009"
  },
  {
    "votes": 12629,
    "candidate": {
      "name": {
        "my": "ဦးပိုက်လင်း",
        "en": "U PAIK LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004009"
  },
  {
    "votes": 5644,
    "candidate": {
      "name": {
        "my": "ဦးဗန်နေ",
        "en": "U VAN NAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP1",
    "constituency": "MMR004009"
  },
  {
    "votes": 4650,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ထွန်း",
        "en": "U KYAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR004009"
  },
  {
    "votes": 12446,
    "candidate": {
      "name": {
        "my": "ဦးပုမ့်ခန့်အင်",
        "en": "U PUN KHAM INN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004004"
  },
  {
    "votes": 9192,
    "candidate": {
      "name": {
        "my": "ဦးဝမ်လီနာသန်",
        "en": "U WAN LINA THANG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004004"
  },
  {
    "votes": 10320,
    "candidate": {
      "name": {
        "my": "ပါမောက္ခဒေါက်တာ ကမ်ကျင်ဒါးလ်",
        "en": "DR. KHAM CINDAL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Chin National Party",
    "constituency": "MMR004004"
  },
  {
    "votes": 2292,
    "candidate": {
      "name": {
        "my": "ဦးပီအက်စ်ခမ်ဒို့နန်",
        "en": "U P.S.CAM DO NAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR004004"
  },
  {
    "votes": 6948,
    "candidate": {
      "name": {
        "my": "ဦးဟောက်ခန့်မန်",
        "en": "U HAU KAM MANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR004005"
  },
  {
    "votes": 5055,
    "candidate": {
      "name": {
        "my": "ဦးမန်ချင်ပေါ",
        "en": "U MANG CIN PAU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR004005"
  },
  {
    "votes": 23883,
    "candidate": {
      "name": {
        "my": "ဦးလွန်းမောင်",
        "en": "U LUN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001010"
  },
  {
    "votes": 7777,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုလေး(ခ)ဦးဖိုးအေး",
        "en": "U Ko Ko Lay (a) U Poe Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001010"
  },
  {
    "votes": 17565,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ညို",
        "en": "U Chit Nyo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001010"
  },
  {
    "votes": 4957,
    "candidate": {
      "name": {
        "my": "ဦးဇုံးတိန့်",
        "en": "U Zon Taint"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001005"
  },
  {
    "votes": 3181,
    "candidate": {
      "name": {
        "my": "ဦးနုဘုတ်ထောခေါင်",
        "en": "U Na Bot Taw Kaung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001005"
  },
  {
    "votes": 5983,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်အေး",
        "en": "U Maung Maung Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001009"
  },
  {
    "votes": 29426,
    "candidate": {
      "name": {
        "my": "ဦအုန်းမြင့်",
        "en": "U OHN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001009"
  },
  {
    "votes": 21633,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဘောက်ဂျာ",
        "en": "DAW BAWK JA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR001009"
  },
  {
    "votes": 507,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဒွဲဘူ",
        "en": "DAW DWAIR BU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001003"
  },
  {
    "votes": 350,
    "candidate": {
      "name": {
        "my": "ဦးအင်ဆင်းနော်အောင်",
        "en": "U In Sin Naw Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001003"
  },
  {
    "votes": 3308,
    "candidate": {
      "name": {
        "my": "ဦးစီခန်ရမ်",
        "en": "U Si Kan Yan"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008"
  },
  {
    "votes": 3177,
    "candidate": {
      "name": {
        "my": "ဦးချောဖါဝူ",
        "en": "U Chaw Par Wu"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001008"
  },
  {
    "votes": 2066,
    "candidate": {
      "name": {
        "my": "ဦးကွမ်ငါးနော်အောင်",
        "en": "U Kon Ngar Naw Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001006"
  },
  {
    "votes": 2512,
    "candidate": {
      "name": {
        "my": "ဦးအင်းဖုန်ဆန်း(ခ)ဦးအင်ထုဖုန်ဆန်း",
        "en": "U Inn Pon San (a) U Inn Tut Pon San"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001006"
  },
  {
    "votes": 7595,
    "candidate": {
      "name": {
        "my": " ဦးထွန်းသိန်း(ခ)ဦးထွန်းထွန်း",
        "en": "U Tun Thein (a) U Tun Tun"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001003"
  },
  {
    "votes": 4593,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": "U Aung Than"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001003"
  },
  {
    "votes": 6159,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဟန်ထွန်း",
        "en": "U Myo Han Tun"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001003"
  },
  {
    "votes": 9648,
    "candidate": {
      "name": {
        "my": "ဦးသန်းတင့်အောင်",
        "en": "U Than Tint Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001008"
  },
  {
    "votes": 29080,
    "candidate": {
      "name": {
        "my": "ဦးဘုန်းဆေွ",
        "en": "U PHONE SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001008"
  },
  {
    "votes": 5838,
    "candidate": {
      "name": {
        "my": "ဦးကြီးမြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001008"
  },
  {
    "votes": 16156,
    "candidate": {
      "name": {
        "my": "စိုင်းဝင်းအေး (ခ) စိုင်းထွန်းအေး",
        "en": "?Sai Win Aye (a) Sai Tun Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001007"
  },
  {
    "votes": 34422,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုးလေး",
        "en": "U JYAW SOE LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001007"
  },
  {
    "votes": 13329,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကြန်",
        "en": "U TUN KYAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001007"
  },
  {
    "votes": 10551,
    "candidate": {
      "name": {
        "my": "ဦးရုံးမူ",
        "en": "U YONE MU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001012"
  },
  {
    "votes": 10715,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ထွန်း",
        "en": "U ZAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001012"
  },
  {
    "votes": 7113,
    "candidate": {
      "name": {
        "my": "ဦးဖိုးရင်",
        "en": "U PHOE YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001012"
  },
  {
    "votes": 41920,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဇော်",
        "en": "U THEIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001001"
  },
  {
    "votes": 8610,
    "candidate": {
      "name": {
        "my": "စိုင်းအောင်ကျော်ဦး",
        "en": "Sai Aung Kyaw Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001001"
  },
  {
    "votes": 14585,
    "candidate": {
      "name": {
        "my": "ဦးမာခါး",
        "en": "U MAR KHAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR001001"
  },
  {
    "votes": 16498,
    "candidate": {
      "name": {
        "my": "ဦးလနန်အောက်",
        "en": "U LA NAN AUK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001001"
  },
  {
    "votes": 3153,
    "candidate": {
      "name": {
        "my": "ဦးဂျီဖုန်ဆားရ်",
        "en": "U GYEE PHONE SAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001017"
  },
  {
    "votes": 630,
    "candidate": {
      "name": {
        "my": "ဦးခေါ်ဒုတီးတန်",
        "en": "U KHAW DU TEETAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001017"
  },
  {
    "votes": 20284,
    "candidate": {
      "name": {
        "my": "ဦးယောဒီဒေွ့",
        "en": "U YAW DI DWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001014"
  },
  {
    "votes": 1538,
    "candidate": {
      "name": {
        "my": "ဦးစောနွ့ဲရေွှမောင်",
        "en": "U SAW NWE SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001014"
  },
  {
    "votes": 5671,
    "candidate": {
      "name": {
        "my": "ဦးဆာရ်ဝမ်ဖုန်",
        "en": "U SAR WON PHON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001014"
  },
  {
    "votes": 18631,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်(ခ)ဦးဖြူ",
        "en": "U WIN NAING (a) U PHYU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001011"
  },
  {
    "votes": 16624,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းကြွယ",
        "en": "U THAUNG KYWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001011"
  },
  {
    "votes": 1233,
    "candidate": {
      "name": {
        "my": "ဦးခမိုင်မှုံကွမ်(ခ)ဦးခမိုင်တန",
        "en": "U KAMAING HMON KWAN (a) U KAMAN TAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001015"
  },
  {
    "votes": 858,
    "candidate": {
      "name": {
        "my": "ဦးခရိုလ",
        "en": "U KRO LA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001015"
  },
  {
    "votes": 7260,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဆေွ",
        "en": "U MYO SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001004"
  },
  {
    "votes": 4734,
    "candidate": {
      "name": {
        "my": "ဦးအင်ဗေွထွယ်အောင်",
        "en": "U IN BWE HTAIR AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001004"
  },
  {
    "votes": 569,
    "candidate": {
      "name": {
        "my": "ဦးလင်းလင်းဦး",
        "en": "U LIN LIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR001004"
  },
  {
    "votes": 1083,
    "candidate": {
      "name": {
        "my": "ဦးဇောင်းခေါင်",
        "en": "U ZAUNG KHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR001006"
  },
  {
    "votes": 1037,
    "candidate": {
      "name": {
        "my": "ဦးမုတ်ရင်းဒေါင်ဟောင်း",
        "en": "U MOT YING DAUNG HAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001006"
  },
  {
    "votes": 10663,
    "candidate": {
      "name": {
        "my": "ဦးပေါလု",
        "en": "U PAW LU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDPKS",
    "constituency": "MMR001002"
  },
  {
    "votes": 10636,
    "candidate": {
      "name": {
        "my": "ဦးလမြာသား",
        "en": "U Lar Myar Thaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR001002"
  },
  {
    "votes": 12652,
    "candidate": {
      "name": {
        "my": "ဦးယိန်းဘောမ်",
        "en": "U YEIN BAWM"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR001002"
  },
  {
    "votes": 4524,
    "candidate": {
      "name": {
        "my": "ဦးအေးမောင််",
        "en": "U AYE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002005"
  },
  {
    "votes": 449,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရင််",
        "en": "U TUN YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002005"
  },
  {
    "votes": 26869,
    "candidate": {
      "name": {
        "my": "ဦးစိုးရယ်",
        "en": "U SOE YAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002002"
  },
  {
    "votes": 2873,
    "candidate": {
      "name": {
        "my": "ဦးထော်ရယ်",
        "en": "U TAW YAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002002"
  },
  {
    "votes": 5192,
    "candidate": {
      "name": {
        "my": "ဦးဟူးဘတ်ဘယ်ရီ",
        "en": "U HUBERT BERI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR002002"
  },
  {
    "votes": 6432,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွဋ်တင်",
        "en": "U YE HTUT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002006"
  },
  {
    "votes": 249,
    "candidate": {
      "name": {
        "my": "ဦးစောဒင်းနရာ",
        "en": "U SAW DANIEL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002006"
  },
  {
    "votes": 7612,
    "candidate": {
      "name": {
        "my": "ဦးအူးရယ်(ခ)ဦးဦးရယ်",
        "en": "U EURAL (a) U OO YAL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002003"
  },
  {
    "votes": 2536,
    "candidate": {
      "name": {
        "my": "ဦးလောရယ်",
        "en": "U LAW YAL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002003"
  },
  {
    "votes": 42893,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညို",
        "en": "U KHIN MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002001"
  },
  {
    "votes": 15179,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစိုးမြင့်",
        "en": "U SAI SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002001"
  },
  {
    "votes": 2603,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ခိုင်ဝင်း",
        "en": "U KYAW KHINE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002007"
  },
  {
    "votes": 244,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရေွှထွန်း",
        "en": "U SAI SHWE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002007"
  },
  {
    "votes": 1563,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းနွံ",
        "en": "DAW NAN NUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR002004"
  },
  {
    "votes": 184,
    "candidate": {
      "name": {
        "my": "ဦးမြမောင််",
        "en": "U MYA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR002004"
  },
  {
    "votes": 30540,
    "candidate": {
      "name": {
        "my": "စောသိန်းအောင်",
        "en": "Saw Thein Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003002"
  },
  {
    "votes": 17955,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရင်",
        "en": "U KHIN MAUNG YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003002"
  },
  {
    "votes": 8051,
    "candidate": {
      "name": {
        "my": "ဦးစောဟန်သိန်း",
        "en": "U SAW HAN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003002"
  },
  {
    "votes": 45339,
    "candidate": {
      "name": {
        "my": "နန်းစေအွာ",
        "en": "DAW NAN SAY HWA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003001"
  },
  {
    "votes": 45197,
    "candidate": {
      "name": {
        "my": "ဦးစောထားလုံ",
        "en": "U SAW HTAR LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003001"
  },
  {
    "votes": 27654,
    "candidate": {
      "name": {
        "my": "ဦးစောကျော်ခင်ဝင်း",
        "en": "U SAW KYAW KHIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003001"
  },
  {
    "votes": 5185,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသန်းနိုင်",
        "en": "U SAI THAN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003003"
  },
  {
    "votes": 2959,
    "candidate": {
      "name": {
        "my": "ဦးအေးသိန်း",
        "en": "U AYE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003003"
  },
  {
    "votes": 13357,
    "candidate": {
      "name": {
        "my": "မင်းဝင်းမြင့်",
        "en": "Min Win Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR003006"
  },
  {
    "votes": 23224,
    "candidate": {
      "name": {
        "my": "ဦးသူရိန်ဇော်",
        "en": "U THUREIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003006"
  },
  {
    "votes": 15749,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှ",
        "en": "U TIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003006"
  },
  {
    "votes": 8673,
    "candidate": {
      "name": {
        "my": "ဦးစောထွဋ်ခေါင်လွင်",
        "en": "U SAW HTUT KAUNG LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003007"
  },
  {
    "votes": 3372,
    "candidate": {
      "name": {
        "my": "ဦးစောကြည်အေး",
        "en": "U SAW KYI AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR003007"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဦး",
        "en": "U THAN OO"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003005"
  },
  {
    "votes": 10033,
    "candidate": {
      "name": {
        "my": "ဦးစောနေကော်ထူး",
        "en": "U SAW NAY KAW Htoo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR003004"
  },
  {
    "votes": 1615,
    "candidate": {
      "name": {
        "my": "နန်းလားခူ",
        "en": "NAN LARKU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR003004"
  },
  {
    "votes": 7923,
    "candidate": {
      "name": {
        "my": "ဦးစောဂျူးနစ်",
        "en": "U SAW JUNAS"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR003004"
  },
  {
    "votes": 83229,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်ကြည်",
        "en": "U TUN AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR009016"
  },
  {
    "votes": 83212,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုထွန်း",
        "en": "U KO KO TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009016"
  },
  {
    "votes": 23555,
    "candidate": {
      "name": {
        "my": "ဦးဝမ်ဖေ",
        "en": "U WON PHE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009016"
  },
  {
    "votes": 23080,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သန်း",
        "en": "U Maung Maung Than"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009003"
  },
  {
    "votes": 59081,
    "candidate": {
      "name": {
        "my": "ဦးသိ်န်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009003"
  },
  {
    "votes": 18885,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009003"
  },
  {
    "votes": 2677,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဝင်း",
        "en": "U THAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009023"
  },
  {
    "votes": 23051,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်ငြိမ်း",
        "en": "Dr. Kyaw Nyein"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009023"
  },
  {
    "votes": 32016,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009023"
  },
  {
    "votes": 10590,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သန်း",
        "en": "U MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009023"
  },
  {
    "votes": 14062,
    "candidate": {
      "name": {
        "my": "ဦးအောင်",
        "en": "U Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009015"
  },
  {
    "votes": 28522,
    "candidate": {
      "name": {
        "my": "ဦးပိုက်ထေွး",
        "en": "U PAIK HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009015"
  },
  {
    "votes": 5792,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမောင်",
        "en": "U SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009015"
  },
  {
    "votes": 106900,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မင်း",
        "en": "U ZAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009001"
  },
  {
    "votes": 10368,
    "candidate": {
      "name": {
        "my": "ဦးဗိုလ်ဟိန်း(ခ)ဦးခင်မောင်ဇင်",
        "en": "U BO HEIN (a) U KHIN MAUNG ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009001"
  },
  {
    "votes": 16258,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွတ်",
        "en": "U Ye Htut"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009001"
  },
  {
    "votes": 21694,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009001"
  },
  {
    "votes": 8524,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကိုကိုဌေး",
        "en": "U Aung Ko Ko Htay"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009007"
  },
  {
    "votes": 63350,
    "candidate": {
      "name": {
        "my": "ဦးအေးသူ",
        "en": "U AYE THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009007"
  },
  {
    "votes": 23226,
    "candidate": {
      "name": {
        "my": "ဦးတင့်လွင်",
        "en": "U TINT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009007"
  },
  {
    "votes": 5535,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဝင်း",
        "en": "U Htay Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009014"
  },
  {
    "votes": 25873,
    "candidate": {
      "name": {
        "my": "ဦးစိုးနိုင်",
        "en": "U SOE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009014"
  },
  {
    "votes": 9246,
    "candidate": {
      "name": {
        "my": "ဦးတင်လိှုင်",
        "en": "U TIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009014"
  },
  {
    "votes": 44762,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းတင့်",
        "en": "U WIN TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009013"
  },
  {
    "votes": 31687,
    "candidate": {
      "name": {
        "my": "ဦးရေွှသန်း",
        "en": "U SHWE THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009013"
  },
  {
    "votes": 8942,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဇော်",
        "en": "U WIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009020"
  },
  {
    "votes": 109173,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009020"
  },
  {
    "votes": 8260,
    "candidate": {
      "name": {
        "my": "ဦးမျိုုးမြင့််",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009020"
  },
  {
    "votes": 3619,
    "candidate": {
      "name": {
        "my": "ဦးမိုးမင်းဝင်း",
        "en": "U MOE MIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009005"
  },
  {
    "votes": 12695,
    "candidate": {
      "name": {
        "my": "ဦးသာညီ",
        "en": "U Tha Nyi"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009005"
  },
  {
    "votes": 46986,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သူ",
        "en": "U MYINT THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009005"
  },
  {
    "votes": 14812,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝမ်း",
        "en": "U TIN WUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009005"
  },
  {
    "votes": 16873,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဖေ",
        "en": "U Myint Pae"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009006"
  },
  {
    "votes": 63108,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်စိုး",
        "en": "U AUNG KYAW SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009006"
  },
  {
    "votes": 25099,
    "candidate": {
      "name": {
        "my": "ဦးသိ်န်းညွန့်",
        "en": "U THEIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009006"
  },
  {
    "votes": 18804,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဌေး",
        "en": "U MYINT HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009009"
  },
  {
    "votes": 11302,
    "candidate": {
      "name": {
        "my": "ဦးတင်စိန်",
        "en": "U TIN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009009"
  },
  {
    "votes": 19120,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": " U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009018"
  },
  {
    "votes": 105587,
    "candidate": {
      "name": {
        "my": "ဦးစောလှ",
        "en": "U SAW HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009018"
  },
  {
    "votes": 26817,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009018"
  },
  {
    "votes": 15114,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009021"
  },
  {
    "votes": 57246,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညို",
        "en": "U KHIN MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009021"
  },
  {
    "votes": 15558,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိုးဝင်း",
        "en": "U AUNG SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009021"
  },
  {
    "votes": 5618,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ရှိန်",
        "en": "U AUNG SHEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009008"
  },
  {
    "votes": 62500,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမောင်မောင်ဋ္ဌေး",
        "en": "DR. TIN MAUNG NWAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009008"
  },
  {
    "votes": 21327,
    "candidate": {
      "name": {
        "my": "ဦးတင်နေွး",
        "en": "U TIN NWAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009008"
  },
  {
    "votes": 40072,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စွာစိုး",
        "en": "U KYAW SWA SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR009010"
  },
  {
    "votes": 57350,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်",
        "en": "U KHIN MAUNG LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009010"
  },
  {
    "votes": 26551,
    "candidate": {
      "name": {
        "my": "ဦးစန်းလွင်",
        "en": "U SAN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009010"
  },
  {
    "votes": 11524,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကြိုင်",
        "en": "U MAUNG KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009025"
  },
  {
    "votes": 19894,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမောင်",
        "en": "U HTAY MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009025"
  },
  {
    "votes": 11510,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009025"
  },
  {
    "votes": 24534,
    "candidate": {
      "name": {
        "my": "ဦးနီကုန်း",
        "en": "U NI KONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009022"
  },
  {
    "votes": 29777,
    "candidate": {
      "name": {
        "my": "ဦးရှိန်ကျော်ဝင်း",
        "en": "U SHEIN KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009022"
  },
  {
    "votes": 18846,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009011"
  },
  {
    "votes": 8270,
    "candidate": {
      "name": {
        "my": "ဦးဟန်ထွန်း",
        "en": "U HAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009011"
  },
  {
    "votes": 50825,
    "candidate": {
      "name": {
        "my": "ဦးအေးနိုင်",
        "en": "U AYE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009017"
  },
  {
    "votes": 18907,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ထွဏ်း(ခ)ဆရာထွဏ်း",
        "en": "U MAUNG TUN (a) SAYA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009017"
  },
  {
    "votes": 4449,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုး",
        "en": "U MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009004"
  },
  {
    "votes": 17490,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဦး",
        "en": "U Myint Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009004"
  },
  {
    "votes": 76893,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဝင်း",
        "en": "U THAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009004"
  },
  {
    "votes": 31699,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့့််",
        "en": "U SOE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009004"
  },
  {
    "votes": 50849,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကျော်",
        "en": "U WIN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009012"
  },
  {
    "votes": 14578,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်",
        "en": "U KHIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009012"
  },
  {
    "votes": 2668,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP1",
    "constituency": "MMR009012"
  },
  {
    "votes": 9025,
    "candidate": {
      "name": {
        "my": "ဦးအံ့မော်",
        "en": "U ANT MAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009024"
  },
  {
    "votes": 14545,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဌေး",
        "en": "U KHIN MAUNG HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009024"
  },
  {
    "votes": 6886,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရီ",
        "en": "U TUN YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009024"
  },
  {
    "votes": 45057,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဝင်းမော်ထွန်း",
        "en": "DAW WIN MAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009002"
  },
  {
    "votes": 903,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်မိုးအေး(ခ)မယော",
        "en": "DAW TIN MOE (a) MA YAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009002"
  },
  {
    "votes": 14590,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U Nay Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR009002"
  },
  {
    "votes": 11513,
    "candidate": {
      "name": {
        "my": "ဦးတင်ငေွ",
        "en": "U TIN NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009002"
  },
  {
    "votes": 16830,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဆေွ",
        "en": "U MYO SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR009019"
  },
  {
    "votes": 101146,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမောင်",
        "en": "U SOE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR009019"
  },
  {
    "votes": 13868,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွဋ်",
        "en": "U YE HTUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR009019"
  },
  {
    "votes": 30969,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသန့်ဇင်",
        "en": "DR. THANT ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010006"
  },
  {
    "votes": 50180,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်းဦး",
        "en": "U THEIN TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010006"
  },
  {
    "votes": 19875,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဆေွ",
        "en": "U TINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010006"
  },
  {
    "votes": 3697,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U Tin Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Wunthanu NLD (The Union Of Myanmar)",
    "constituency": "MMR010001"
  },
  {
    "votes": 35486,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အောင်",
        "en": "U KYAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010001"
  },
  {
    "votes": 41697,
    "candidate": {
      "name": {
        "my": "ဦးကိုကြီး",
        "en": "U KO GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010001"
  },
  {
    "votes": 10761,
    "candidate": {
      "name": {
        "my": "ဦးစောလွင်",
        "en": "U SAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010001"
  },
  {
    "votes": 29287,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သက်",
        "en": "U KHIN MAUNG THET"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010002"
  },
  {
    "votes": 30591,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်မြင့်",
        "en": "DR. KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010002"
  },
  {
    "votes": 6558,
    "candidate": {
      "name": {
        "my": "ဦးစံရင်",
        "en": "U SAN YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010002"
  },
  {
    "votes": 6657,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လှ",
        "en": "U SEIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010002"
  },
  {
    "votes": 25672,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်စိုး",
        "en": "U AUNG NAING SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010004"
  },
  {
    "votes": 32381,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်",
        "en": "U TIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010004"
  },
  {
    "votes": 17516,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010004"
  },
  {
    "votes": 11823,
    "candidate": {
      "name": {
        "my": "ဦးစည်သူအောင်",
        "en": "U SITHU AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR010004"
  },
  {
    "votes": 113697,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010020"
  },
  {
    "votes": 34781,
    "candidate": {
      "name": {
        "my": "ဦးရဲမောင်",
        "en": "U YE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010020"
  },
  {
    "votes": 14170,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သန်း",
        "en": "U KHIN MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010013"
  },
  {
    "votes": 116178,
    "candidate": {
      "name": {
        "my": "ဦးသောင်း",
        "en": "U THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010013"
  },
  {
    "votes": 7927,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010013"
  },
  {
    "votes": 11926,
    "candidate": {
      "name": {
        "my": "ူဦးတင်အေး",
        "en": "U Tin Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010027"
  },
  {
    "votes": 86129,
    "candidate": {
      "name": {
        "my": "ဦးဌေးဝင်း",
        "en": "U HTAY WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010027"
  },
  {
    "votes": 27654,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဦး",
        "en": "U MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010027"
  },
  {
    "votes": 18614,
    "candidate": {
      "name": {
        "my": "ဒေါ်ဆေွထေွး",
        "en": "DAW SWE HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010009"
  },
  {
    "votes": 55167,
    "candidate": {
      "name": {
        "my": "ဦးပြည့်မောင်",
        "en": "U PYEI MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010009"
  },
  {
    "votes": 27701,
    "candidate": {
      "name": {
        "my": "ဦးညိုမိှုင်း",
        "en": "U NYO HMAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010009"
  },
  {
    "votes": 7672,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဆေွ",
        "en": "U CHIT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010003"
  },
  {
    "votes": 24868,
    "candidate": {
      "name": {
        "my": "ဦးစောမင်းနိုင်",
        "en": "U SAW MIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010003"
  },
  {
    "votes": 30636,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြအေး",
        "en": "DR. MYA AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010003"
  },
  {
    "votes": 9551,
    "candidate": {
      "name": {
        "my": "ဒေါ်သောင်းသောင်းနု",
        "en": "DAW THAUNG THAUNG NU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010003"
  },
  {
    "votes": 4943,
    "candidate": {
      "name": {
        "my": "ဦးဝေလင်းအောင်",
        "en": "U WAI LIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR010003"
  },
  {
    "votes": 61878,
    "candidate": {
      "name": {
        "my": "ဦးအေးမောက်",
        "en": "U AYE MAUK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010029"
  },
  {
    "votes": 4057,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သန်း",
        "en": "U KHIN MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010029"
  },
  {
    "votes": 6517,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်",
        "en": "U KHIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010029"
  },
  {
    "votes": 8472,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွန်းနိုင်",
        "en": "U YE TUN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010029"
  },
  {
    "votes": 117152,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010028"
  },
  {
    "votes": 23284,
    "candidate": {
      "name": {
        "my": "ဦးကျော်တင်",
        "en": "U KYAW TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010028"
  },
  {
    "votes": 13242,
    "candidate": {
      "name": {
        "my": "ဥိးဝင်းဇော်",
        "en": "U WIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR010028"
  },
  {
    "votes": 17055,
    "candidate": {
      "name": {
        "my": "ဦးတိုးတိုး",
        "en": "U TOE TOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010011"
  },
  {
    "votes": 27289,
    "candidate": {
      "name": {
        "my": "ဦးဘဌေး",
        "en": "U BA HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010011"
  },
  {
    "votes": 7057,
    "candidate": {
      "name": {
        "my": "ဦးအေးဆောင်",
        "en": "U AYE SAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010011"
  },
  {
    "votes": 16599,
    "candidate": {
      "name": {
        "my": "ဦးထက်နိုင်",
        "en": "U HTET NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010017"
  },
  {
    "votes": 92103,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမြင့်",
        "en": "U WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010017"
  },
  {
    "votes": 11360,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရေွှ",
        "en": "U THAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010017"
  },
  {
    "votes": 9759,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး",
        "en": "U KHIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR010017"
  },
  {
    "votes": 6851,
    "candidate": {
      "name": {
        "my": "ဒေါ်အေးသန္တာမိုး",
        "en": "DAW AYE THANDAR MOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010015"
  },
  {
    "votes": 12844,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်းဝင်း",
        "en": "U KYAW SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010015"
  },
  {
    "votes": 80183,
    "candidate": {
      "name": {
        "my": "ဦးကြည်မောင်",
        "en": "U KYI MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010015"
  },
  {
    "votes": 6820,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လိှုုင်",
        "en": "U SEIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010015"
  },
  {
    "votes": 98423,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ထွန်းအောင်",
        "en": "U NYAN TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010019"
  },
  {
    "votes": 7920,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဝင်း",
        "en": "U SEIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010019"
  },
  {
    "votes": 42866,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြဦး",
        "en": "DR. MYA OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010021"
  },
  {
    "votes": 16158,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လွင်",
        "en": "U MYINT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010021"
  },
  {
    "votes": 23508,
    "candidate": {
      "name": {
        "my": "ဦးစိုးနွယ်",
        "en": "U SOE NWAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010022"
  },
  {
    "votes": 70486,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြိုင်",
        "en": "U AUNG KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010022"
  },
  {
    "votes": 21917,
    "candidate": {
      "name": {
        "my": "ဦးသန်းရှိန်",
        "en": "U THAN SHEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010022"
  },
  {
    "votes": 3761,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းရေွှကြာ",
        "en": "Nan Shwe Kyar"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Wunthanu NLD (The Union Of Myanmar)",
    "constituency": "MMR010007"
  },
  {
    "votes": 1940,
    "candidate": {
      "name": {
        "my": "ဦးမင်းအောင်",
        "en": "U MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR010007"
  },
  {
    "votes": 18552,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စိုးထိုက်",
        "en": "U KYAW SOE HTAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010007"
  },
  {
    "votes": 49301,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": "U AUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010007"
  },
  {
    "votes": 17854,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းကြည်",
        "en": "U TUN KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010007"
  },
  {
    "votes": 1976,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လေး",
        "en": "U KHIN MAUNG LAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR010007"
  },
  {
    "votes": 133029,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်စိုး",
        "en": "U MYINT SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010024"
  },
  {
    "votes": 19382,
    "candidate": {
      "name": {
        "my": "ဦးစံထွန်း",
        "en": "U SAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010024"
  },
  {
    "votes": 2179,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသန်းထိုက်ဦး",
        "en": "Dr. Than Thiet Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Wunthanu NLD (The Union Of Myanmar)",
    "constituency": "MMR010005"
  },
  {
    "votes": 13226,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်",
        "en": "U AUNG NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010005"
  },
  {
    "votes": 28392,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကို",
        "en": "U MAUNG KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010005"
  },
  {
    "votes": 10203,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စိုး",
        "en": "U MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010005"
  },
  {
    "votes": 5791,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ခိုင်",
        "en": "U MYINT KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR010005"
  },
  {
    "votes": 73021,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်ဦး",
        "en": "U HLA MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010026"
  },
  {
    "votes": 11707,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ရေွှ",
        "en": "U NYAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010026"
  },
  {
    "votes": 9769,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR010026"
  },
  {
    "votes": 77441,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်အောင်",
        "en": "DR. MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010008"
  },
  {
    "votes": 34626,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ဦး",
        "en": "U MAUNG MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010008"
  },
  {
    "votes": 7445,
    "candidate": {
      "name": {
        "my": "ဦးစိန်အောင်ကျော်",
        "en": "U SEIN AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010010"
  },
  {
    "votes": 34813,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010010"
  },
  {
    "votes": 13893,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဦး",
        "en": "U CHIT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010010"
  },
  {
    "votes": 13102,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းစိန်",
        "en": "U TUN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010014"
  },
  {
    "votes": 52209,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ပြုံး",
        "en": "U KYI PYONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010014"
  },
  {
    "votes": 9731,
    "candidate": {
      "name": {
        "my": "ဦးတော်ကောင်း",
        "en": "U TAW KAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010014"
  },
  {
    "votes": 8752,
    "candidate": {
      "name": {
        "my": "ဦးနန်းထိုက်",
        "en": "U NAN HTAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010016"
  },
  {
    "votes": 69074,
    "candidate": {
      "name": {
        "my": "ဦးတင်အေး",
        "en": "U TIN AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010016"
  },
  {
    "votes": 3180,
    "candidate": {
      "name": {
        "my": "ဦးပြေအေး",
        "en": "U PYI AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010016"
  },
  {
    "votes": 1750,
    "candidate": {
      "name": {
        "my": "ဦးတင်အုန်း",
        "en": "U TIN OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP1",
    "constituency": "MMR010016"
  },
  {
    "votes": 106803,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ဦး",
        "en": "U MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010025"
  },
  {
    "votes": 6717,
    "candidate": {
      "name": {
        "my": "ဦးသန်း",
        "en": "U THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010025"
  },
  {
    "votes": 4416,
    "candidate": {
      "name": {
        "my": "ဦးဝမ်းမောင်",
        "en": "U WUN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR010025"
  },
  {
    "votes": 3106,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်တိုး",
        "en": "U MAUNG MAUNG TOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010018"
  },
  {
    "votes": 122171,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သောင်း",
        "en": "U AUNG THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010018"
  },
  {
    "votes": 6204,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010018"
  },
  {
    "votes": 6033,
    "candidate": {
      "name": {
        "my": "ဦးအောင်အောင်စန်း (ခ) ဦးဇိုးအောင်",
        "en": "U AUNG AUNG SAN (A) U POE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010012"
  },
  {
    "votes": 31079,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညွန့်",
        "en": "U KHIN MAUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010012"
  },
  {
    "votes": 12148,
    "candidate": {
      "name": {
        "my": "ဦးသွင်",
        "en": "U THWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010012"
  },
  {
    "votes": 12305,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဟန်",
        "en": "U TUN HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010030"
  },
  {
    "votes": 18483,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဌေး",
        "en": "U MYINT HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010030"
  },
  {
    "votes": 69939,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိုး",
        "en": "U THAN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010030"
  },
  {
    "votes": 74288,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမြင့်",
        "en": "U THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010031"
  },
  {
    "votes": 29210,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010031"
  },
  {
    "votes": 16251,
    "candidate": {
      "name": {
        "my": "ဦးကိုကို",
        "en": "U KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010031"
  },
  {
    "votes": 9980,
    "candidate": {
      "name": {
        "my": "ဦးထိုက်သူ",
        "en": "U HTAIK THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR010023"
  },
  {
    "votes": 109599,
    "candidate": {
      "name": {
        "my": "ဦးဘိုနီ",
        "en": "U BO NI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR010023"
  },
  {
    "votes": 18562,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဖြူ",
        "en": "U TUN PHYU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR010023"
  },
  {
    "votes": 60503,
    "candidate": {
      "name": {
        "my": "ဦးသူရရေွှမန်း",
        "en": "U Thura SHWE MANN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": ""
  },
  {
    "votes": 2253,
    "candidate": {
      "name": {
        "my": "ဦးချစ်စိန်",
        "en": "U CHIT SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": ""
  },
  {
    "votes": 44305,
    "candidate": {
      "name": {
        "my": "ဦးသီဟသူရတင်အောင်မြင့်ဦး",
        "en": "U Thiha Thura TIN AUNG MYINT OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": ""
  },
  {
    "votes": 4615,
    "candidate": {
      "name": {
        "my": "ဦးလှရေွှ",
        "en": "U HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": ""
  },
  {
    "votes": 26885,
    "candidate": {
      "name": {
        "my": "ဦးကျော်စွာခိုင်",
        "en": "U KYAW SWA KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": ""
  },
  {
    "votes": 6734,
    "candidate": {
      "name": {
        "my": "ဦးကြီးမြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": ""
  },
  {
    "votes": 65620,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းစိန်",
        "en": "U THEIN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": ""
  },
  {
    "votes": 6257,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": "U KYAW AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": ""
  },
  {
    "votes": 13398,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လှူိင်",
        "en": "U MYINT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": ""
  },
  {
    "votes": 1466,
    "candidate": {
      "name": {
        "my": "ဦးမြဟန်",
        "en": "U MYA HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": ""
  },
  {
    "votes": 26771,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးမိုးအောင်",
        "en": "DR. SOE MOE AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011010"
  },
  {
    "votes": 22492,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိုး",
        "en": "U THAN SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011010"
  },
  {
    "votes": 23183,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်",
        "en": "U Hla Maung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011003"
  },
  {
    "votes": 20039,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးမင်းလိှုင်",
        "en": "U MYO MIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011003"
  },
  {
    "votes": 6274,
    "candidate": {
      "name": {
        "my": "ဦးစံပွင့်",
        "en": "U SAN PWINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011003"
  },
  {
    "votes": 17199,
    "candidate": {
      "name": {
        "my": "နိုင်ငေွသိန်း",
        "en": "Nai Ngwe Thein (Nai Janu)"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011002"
  },
  {
    "votes": 4715,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်ဆေွ",
        "en": "DR.MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR011002"
  },
  {
    "votes": 18922,
    "candidate": {
      "name": {
        "my": "ဒေါ်မိယဉ်ချမ်း",
        "en": "DAW MI YIN CHAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011002"
  },
  {
    "votes": 4715,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းမြင့်(၁)",
        "en": "U THEIN MYINT (1)"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011002"
  },
  {
    "votes": 4425,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းတင်တင်နွယ်",
        "en": "DAW NAN TIN TIN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR011002"
  },
  {
    "votes": 26877,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လွင််",
        "en": "U MYINT LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011009"
  },
  {
    "votes": 23035,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်အောင်",
        "en": "U TUN MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011009"
  },
  {
    "votes": 25938,
    "candidate": {
      "name": {
        "my": "ဦးလှခိုင်",
        "en": "U Hla Khaing"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011001"
  },
  {
    "votes": 47141,
    "candidate": {
      "name": {
        "my": "ဦးအေးမြင့်",
        "en": "U AYE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011001"
  },
  {
    "votes": 22604,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဝင်း",
        "en": "U AUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011001"
  },
  {
    "votes": 24867,
    "candidate": {
      "name": {
        "my": "ဦးအေးမောင်",
        "en": "U Aye Maung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011005"
  },
  {
    "votes": 29900,
    "candidate": {
      "name": {
        "my": "ဦးမြသိန်း",
        "en": "U MYA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011005"
  },
  {
    "votes": 4173,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်မြင့်",
        "en": "U MAUNG MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011005"
  },
  {
    "votes": 24867,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဆိုင်",
        "en": "U TIN SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR011005"
  },
  {
    "votes": 30347,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆေွ",
        "en": "U Kyaw Swe"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011008"
  },
  {
    "votes": 35961,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့််",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011008"
  },
  {
    "votes": 11789,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011004"
  },
  {
    "votes": 11585,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး",
        "en": "U KHIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011004"
  },
  {
    "votes": 5623,
    "candidate": {
      "name": {
        "my": "ဦးအေးဖေ",
        "en": "U AYE PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011004"
  },
  {
    "votes": 2562,
    "candidate": {
      "name": {
        "my": "ဦးစိန်မြမောင်",
        "en": "U SEIN MYA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR011004"
  },
  {
    "votes": 35253,
    "candidate": {
      "name": {
        "my": "ဦးစောဘသိန်း",
        "en": "U SAW BA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011007"
  },
  {
    "votes": 34877,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ချစ်",
        "en": "U AUNG CHIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011007"
  },
  {
    "votes": 34605,
    "candidate": {
      "name": {
        "my": "ဒေါ်မြင့်မြင့်သန်း",
        "en": "Daw Myint Myaint Than"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "AMRDP",
    "constituency": "MMR011006"
  },
  {
    "votes": 15983,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမင်းကျော်ထွန်း",
        "en": "DR. MIN KYAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR011006"
  },
  {
    "votes": 5494,
    "candidate": {
      "name": {
        "my": "ဦးလှမောင်",
        "en": "U HLA MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR011006"
  },
  {
    "votes": 44313,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းဆေွ",
        "en": "U THEIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012014"
  },
  {
    "votes": 5735,
    "candidate": {
      "name": {
        "my": "ဦးညီလွယ်",
        "en": "U NYI LWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012014"
  },
  {
    "votes": 47802,
    "candidate": {
      "name": {
        "my": "ဦးအာဒူတာဟီး",
        "en": "U ABDUL TAHIL"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012010"
  },
  {
    "votes": 15564,
    "candidate": {
      "name": {
        "my": "ဦးဘသိန်း",
        "en": "U BA THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012010"
  },
  {
    "votes": 51985,
    "candidate": {
      "name": {
        "my": "ဦးရေွှမောင်(ခ)အဒူရော်ဇက်",
        "en": "U SHWE MAUNG (A) ADUL RAZAT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012010"
  },
  {
    "votes": 14558,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လှ",
        "en": "U AUNG HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR012010"
  },
  {
    "votes": 13219,
    "candidate": {
      "name": {
        "my": "ဦးညီလေး(ခ)ဦးကြည်သာ",
        "en": "U NYI LAY (A) U KYI THA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012017"
  },
  {
    "votes": 11183,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကြွင်း",
        "en": "U MAUNG KYUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012017"
  },
  {
    "votes": 22975,
    "candidate": {
      "name": {
        "my": "ဦးဘရှင်း",
        "en": "U BA SHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012011"
  },
  {
    "votes": 14626,
    "candidate": {
      "name": {
        "my": "ဦးဝေခိုင်",
        "en": "U WAI KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012011"
  },
  {
    "votes": 5103,
    "candidate": {
      "name": {
        "my": "ဦးစံကျော်ရေွှ",
        "en": "U SAN KYAW SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012011"
  },
  {
    "votes": 14967,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စိန်",
        "en": "U MAUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012004"
  },
  {
    "votes": 44104,
    "candidate": {
      "name": {
        "my": "ဦးသာစိန်",
        "en": "U THA SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012004"
  },
  {
    "votes": 4037,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်းဦး",
        "en": "U HLA TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012004"
  },
  {
    "votes": 44104,
    "candidate": {
      "name": {
        "my": "ဦးမောင်သာဇံ",
        "en": "U MAUNG THARZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012004"
  },
  {
    "votes": 68523,
    "candidate": {
      "name": {
        "my": "ဦးနူရူဟောက်",
        "en": "U NURUL HAUK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012009"
  },
  {
    "votes": 5114,
    "candidate": {
      "name": {
        "my": "ဦးကျော်အေး",
        "en": "U KYAW AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012009"
  },
  {
    "votes": 137691,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ကိရ်အာမက်(ခ)အောင်ဇော်ဝင်း",
        "en": "U ZAWKI ARMAD (A) AUNG ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012009"
  },
  {
    "votes": 30772,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိန်သာ",
        "en": "U AUNG SEIN THAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012005"
  },
  {
    "votes": 8352,
    "candidate": {
      "name": {
        "my": "ဦးမောင်နုချေ",
        "en": "U MAUNG NU CHAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012005"
  },
  {
    "votes": 15817,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်း",
        "en": "U KYAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012005"
  },
  {
    "votes": 20139,
    "candidate": {
      "name": {
        "my": "ဦးစံမောင်",
        "en": "U SAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012003"
  },
  {
    "votes": 9556,
    "candidate": {
      "name": {
        "my": "ဦးစောရေွှ",
        "en": "U SAW SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012003"
  },
  {
    "votes": 47223,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ထွန်းသာ",
        "en": "U AUNG TUN THA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012003"
  },
  {
    "votes": 3606,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR012012"
  },
  {
    "votes": 4502,
    "candidate": {
      "name": {
        "my": " ဦးစောညွန် ့",
        "en": "U SAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012012"
  },
  {
    "votes": 6320,
    "candidate": {
      "name": {
        "my": "ဦးအောင်စိန်",
        "en": "U AUNG SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012012"
  },
  {
    "votes": 6160,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမောင်",
        "en": "U THAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR012012"
  },
  {
    "votes": 3640,
    "candidate": {
      "name": {
        "my": "ဦးတင်နု",
        "en": "U TIN NU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR012012"
  },
  {
    "votes": 15187,
    "candidate": {
      "name": {
        "my": "ဦးဖေသန်း",
        "en": "U PE THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012006"
  },
  {
    "votes": 4367,
    "candidate": {
      "name": {
        "my": "ဦးမောင်လှ",
        "en": "U MAUNG HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012006"
  },
  {
    "votes": 10005,
    "candidate": {
      "name": {
        "my": " ဦးရေွှမန်းတင်",
        "en": "U SHWE MAN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012006"
  },
  {
    "votes": 30550,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ဇံ",
        "en": "U AUNG KYAW ZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012007"
  },
  {
    "votes": 5883,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းမြင့်",
        "en": "U TUN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012007"
  },
  {
    "votes": 11551,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်ခိုင်",
        "en": "U TUN AUNG KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012007"
  },
  {
    "votes": 27698,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းအောင်ကျော်",
        "en": "U TUN AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012002"
  },
  {
    "votes": 2398,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းရေွှခိုင်",
        "en": "U WIN SHWE KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012002"
  },
  {
    "votes": 7475,
    "candidate": {
      "name": {
        "my": "ဦမောင်ထွန်းလှ",
        "en": "U MAUNG TUN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012002"
  },
  {
    "votes": 10514,
    "candidate": {
      "name": {
        "my": "ဦဝေမောင်သန်း",
        "en": "U WAI MAUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012002"
  },
  {
    "votes": 17133,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရေွှခိုင်",
        "en": "U TUN SHWE KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012013"
  },
  {
    "votes": 20165,
    "candidate": {
      "name": {
        "my": "ဦးဘထီး",
        "en": "U BA TEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012013"
  },
  {
    "votes": 297494,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်စောဝေ",
        "en": "DAW KHIN SAW WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012008"
  },
  {
    "votes": 6092,
    "candidate": {
      "name": {
        "my": "ဦးဦးသာဇံ",
        "en": "U OO THARZAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012008"
  },
  {
    "votes": 16618,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012008"
  },
  {
    "votes": 40023,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ညို",
        "en": "U MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012001"
  },
  {
    "votes": 2621,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": " Kaman National Progressive Party",
    "constituency": "MMR012001"
  },
  {
    "votes": 14916,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်နီ",
        "en": "U MAUNG MAUNG NI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR012001"
  },
  {
    "votes": 11839,
    "candidate": {
      "name": {
        "my": "ဦးအမိန်ဟင်းဇား(ခ)ဦးစိန်လှ",
        "en": "U ARMAN HINZAR (A) U SEIN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR012001"
  },
  {
    "votes": 2483,
    "candidate": {
      "name": {
        "my": "ဦးဦးစောမောင်",
        "en": "U OO SAW MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012001"
  },
  {
    "votes": 20168,
    "candidate": {
      "name": {
        "my": " ဦးကောင်းစံရှီ",
        "en": "U KAUNG SAN SHE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012001"
  },
  {
    "votes": 5120,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှုိင်ဝင်း",
        "en": "U TIN HLAING WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": " Kaman National Progressive Party",
    "constituency": "MMR012015"
  },
  {
    "votes": 10285,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine Nationals Development Party",
    "constituency": "MMR012015"
  },
  {
    "votes": 6883,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သိမ်း",
        "en": "U MAUNG MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Rakhine State National Force Of Myanmar",
    "constituency": "MMR012015"
  },
  {
    "votes": 8095,
    "candidate": {
      "name": {
        "my": "ဦးဘစော",
        "en": "U BA SAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012015"
  },
  {
    "votes": 16233,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွန်း",
        "en": "U YE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012015"
  },
  {
    "votes": 19723,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစန်လိှုင်",
        "en": "DR. SAN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR012016"
  },
  {
    "votes": 10814,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ကွန်",
        "en": "U MAUNG KON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR012016"
  },
  {
    "votes": 51967,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005014"
  },
  {
    "votes": 16141,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြင့်",
        "en": "U AUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005014"
  },
  {
    "votes": 6862,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညွန့်",
        "en": "U Kyaw Nyunt"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR005023"
  },
  {
    "votes": 17238,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်း",
        "en": "U KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005023"
  },
  {
    "votes": 10771,
    "candidate": {
      "name": {
        "my": "ဦးချစ်ဟန်",
        "en": "U CHIT HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005023"
  },
  {
    "votes": 43847,
    "candidate": {
      "name": {
        "my": "ဦးသန်းနွယ်",
        "en": "U THAN NWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005013"
  },
  {
    "votes": 13498,
    "candidate": {
      "name": {
        "my": "ဦးချစ်မောင်(ခ)ဥိးချစ်",
        "en": "U CHIT MAUNG (A) U CHIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005013"
  },
  {
    "votes": 10337,
    "candidate": {
      "name": {
        "my": "ဦးနေဝင်း",
        "en": "U Nay Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR005015"
  },
  {
    "votes": 28343,
    "candidate": {
      "name": {
        "my": "ဦးစောလှထွန်း",
        "en": "U SAW HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005015"
  },
  {
    "votes": 5662,
    "candidate": {
      "name": {
        "my": " ဦးမောင်ကို",
        "en": "U MAUNG KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005015"
  },
  {
    "votes": 9037,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005033"
  },
  {
    "votes": 2376,
    "candidate": {
      "name": {
        "my": " ဦးအေးဖေ",
        "en": "U AYE PAE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005033"
  },
  {
    "votes": 17168,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ကျော်",
        "en": "U Kyaw Kyaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR005034"
  },
  {
    "votes": 23132,
    "candidate": {
      "name": {
        "my": "ဦးအေး",
        "en": "U AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005034"
  },
  {
    "votes": 19762,
    "candidate": {
      "name": {
        "my": "ဦးသန်းညွန့်",
        "en": "U THAN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005034"
  },
  {
    "votes": 21171,
    "candidate": {
      "name": {
        "my": " ဦးကျော်နိုင်ဌေး",
        "en": "U KYAW NAING HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005021"
  },
  {
    "votes": 19806,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဦး",
        "en": "U TIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005021"
  },
  {
    "votes": 49575,
    "candidate": {
      "name": {
        "my": "ဦးတင်လှိုင်",
        "en": "U TIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005027"
  },
  {
    "votes": 32629,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မိုး",
        "en": "U AUNG MOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005027"
  },
  {
    "votes": 29712,
    "candidate": {
      "name": {
        "my": "ဦးခေါ်သူလေွ",
        "en": "U KHAW TUAN LUAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005027"
  },
  {
    "votes": 12972,
    "candidate": {
      "name": {
        "my": "ဦးချစ်သန်း",
        "en": "U CHIT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005028"
  },
  {
    "votes": 14741,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသိမ်း",
        "en": "U SOE THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005028"
  },
  {
    "votes": 101223,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာချမ်းငြိမ်း",
        "en": "DR.CHAN NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005007"
  },
  {
    "votes": 18349,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005007"
  },
  {
    "votes": 64535,
    "candidate": {
      "name": {
        "my": "ဦးကျန်ထွန်း",
        "en": "U KYANT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005017"
  },
  {
    "votes": 5217,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မောင်း",
        "en": "U AUNG MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005017"
  },
  {
    "votes": 29774,
    "candidate": {
      "name": {
        "my": "ဦးမိုးဇော်ဟိန်း",
        "en": "U MOE ZAW HAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005020"
  },
  {
    "votes": 22002,
    "candidate": {
      "name": {
        "my": "ဦးခင်ညွန့်",
        "en": "U KHIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005020"
  },
  {
    "votes": 2250,
    "candidate": {
      "name": {
        "my": "ဦးမေွှးတင်",
        "en": "U MWE TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR005020"
  },
  {
    "votes": 1265,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": "U MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR005020"
  },
  {
    "votes": 25011,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးဇော်အောင်",
        "en": "U Myo Zaw Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR005024"
  },
  {
    "votes": 27184,
    "candidate": {
      "name": {
        "my": " ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005024"
  },
  {
    "votes": 12190,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သန်း",
        "en": "U MYINT THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005024"
  },
  {
    "votes": 60546,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရေွှ",
        "en": "U KHIN MAUNG SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005005"
  },
  {
    "votes": 12766,
    "candidate": {
      "name": {
        "my": " ဦးစိုးမောင်",
        "en": "U SOE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005005"
  },
  {
    "votes": 27510,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညွန့်",
        "en": "U KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005008"
  },
  {
    "votes": 14319,
    "candidate": {
      "name": {
        "my": " ဦးသိ်န်းမောင်",
        "en": "U THEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005008"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဆားမူ",
        "en": "U SAR MU"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005036"
  },
  {
    "votes": 5827,
    "candidate": {
      "name": {
        "my": "ဦးရှီသီး",
        "en": "U SHI THEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005035"
  },
  {
    "votes": 1163,
    "candidate": {
      "name": {
        "my": "ဦးအလားပေါ့",
        "en": "U A'LAR POP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005035"
  },
  {
    "votes": 11032,
    "candidate": {
      "name": {
        "my": " ဦးမျိုးမြင့်",
        "en": "U MYO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005031"
  },
  {
    "votes": 11562,
    "candidate": {
      "name": {
        "my": "ဦးသောင်း",
        "en": "U THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005031"
  },
  {
    "votes": 42717,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မြင့်",
        "en": "U MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005029"
  },
  {
    "votes": 12328,
    "candidate": {
      "name": {
        "my": "ဆရာစန်း(ဦးစန်း)",
        "en": "SAYAR SAN (A) U SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005029"
  },
  {
    "votes": 3821,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ညို",
        "en": "U KHIN MAUNG NYO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TUOMFONP",
    "constituency": "MMR005012"
  },
  {
    "votes": 22094,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမြင့်ဝေ",
        "en": "Dr. Myint Wai"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR005012"
  },
  {
    "votes": 1923,
    "candidate": {
      "name": {
        "my": "ဦးအေးစိုး",
        "en": "U AYE SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR005012"
  },
  {
    "votes": 23057,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ဉာဏ်",
        "en": "U SEIN NYAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005012"
  },
  {
    "votes": 75718,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဦး",
        "en": "U KHIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005012"
  },
  {
    "votes": 5790,
    "candidate": {
      "name": {
        "my": "ဦးမောင်စိုး",
        "en": "U MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR005003"
  },
  {
    "votes": 39621,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းငေွ",
        "en": "U OHN NGWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005003"
  },
  {
    "votes": 9851,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်း",
        "en": "U KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005003"
  },
  {
    "votes": 5128,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်",
        "en": "U HTAY LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR005002"
  },
  {
    "votes": 13026,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ထွန်း",
        "en": "U SEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005002"
  },
  {
    "votes": 22639,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်းအောင်",
        "en": "U ZAW WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005002"
  },
  {
    "votes": 12189,
    "candidate": {
      "name": {
        "my": "ဦးဇင်းဝမ်",
        "en": "U ZIN WAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005037"
  },
  {
    "votes": 7445,
    "candidate": {
      "name": {
        "my": "ဦးနောင်ဒန်",
        "en": "U NAUNG DAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005037"
  },
  {
    "votes": 68071,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဆန်း",
        "en": "U KYAW SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005019"
  },
  {
    "votes": 4350,
    "candidate": {
      "name": {
        "my": "ဦးစန်းလွင်",
        "en": "U SAN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005019"
  },
  {
    "votes": 22870,
    "candidate": {
      "name": {
        "my": " ဦးမြင့်ဆေွ",
        "en": "U MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005032"
  },
  {
    "votes": 23473,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ဦး",
        "en": "U MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005032"
  },
  {
    "votes": 25634,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသိန်း",
        "en": "U WIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005026"
  },
  {
    "votes": 19659,
    "candidate": {
      "name": {
        "my": " ဦးစက်အောင်",
        "en": "U SAK AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005026"
  },
  {
    "votes": 91015,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MUANG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005001"
  },
  {
    "votes": 18940,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဆေွ",
        "en": "U MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005001"
  },
  {
    "votes": 14490,
    "candidate": {
      "name": {
        "my": "ဦးဆန်းဝင်းနိုင်",
        "en": "U SAN WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR005001"
  },
  {
    "votes": 43724,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမြင့်",
        "en": "U THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005018"
  },
  {
    "votes": 16058,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်",
        "en": "U MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005018"
  },
  {
    "votes": 88332,
    "candidate": {
      "name": {
        "my": " ဒေါက်တာအောင်သန်း",
        "en": "DR. AUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005004"
  },
  {
    "votes": 24697,
    "candidate": {
      "name": {
        "my": " ဦးလှအောင်",
        "en": "U HLA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005004"
  },
  {
    "votes": 30983,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": "U MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005010"
  },
  {
    "votes": 13179,
    "candidate": {
      "name": {
        "my": "ဦးဗိုလ်မောင်",
        "en": "U BO MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005010"
  },
  {
    "votes": 4221,
    "candidate": {
      "name": {
        "my": "ဦးဘိုမောင်း",
        "en": "U BO MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP2",
    "constituency": "MMR005010"
  },
  {
    "votes": 23424,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရေွှ(ခ)ဦးပေါ်ကာ",
        "en": "U KHIN MAUNG SHWE (A) U POKER"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005030"
  },
  {
    "votes": 7767,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းနိုင်",
        "en": "U WIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005030"
  },
  {
    "votes": 10165,
    "candidate": {
      "name": {
        "my": "ဦးခမ်အင်းဒို",
        "en": "U KAM INN DO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "CPP",
    "constituency": "MMR005030"
  },
  {
    "votes": 50966,
    "candidate": {
      "name": {
        "my": "ဦးတင်မြင့်",
        "en": "U TIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005011"
  },
  {
    "votes": 10201,
    "candidate": {
      "name": {
        "my": "ဦးတိုက်ဦး",
        "en": "U TIK OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005011"
  },
  {
    "votes": 2110,
    "candidate": {
      "name": {
        "my": "ဦးဘဆေွ",
        "en": "U BA SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR005011"
  },
  {
    "votes": 22485,
    "candidate": {
      "name": {
        "my": "ဦးစိုးစိုး",
        "en": "U SOE SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005022"
  },
  {
    "votes": 19480,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လိှုင်",
        "en": "U SEIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005022"
  },
  {
    "votes": 83252,
    "candidate": {
      "name": {
        "my": "ဦးသူရအေးမြင့်",
        "en": "U THURA AYE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005006"
  },
  {
    "votes": 11374,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005006"
  },
  {
    "votes": 23055,
    "candidate": {
      "name": {
        "my": "ဦးစိုးပိုင်",
        "en": "U SOE PAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005025"
  },
  {
    "votes": 8957,
    "candidate": {
      "name": {
        "my": "ဦးလှရေွှ",
        "en": "U HLA SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005025"
  },
  {
    "votes": 36373,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ဦး",
        "en": "U AUNG KYAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005009"
  },
  {
    "votes": 15366,
    "candidate": {
      "name": {
        "my": "ဦးညီညီစိုး",
        "en": "U NYI NYI SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005009"
  },
  {
    "votes": 41431,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းဟန်",
        "en": "U THAUNG HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR005016"
  },
  {
    "votes": 17126,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်မောင်",
        "en": "U NYUNT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR005016"
  },
  {
    "votes": 5758,
    "candidate": {
      "name": {
        "my": "ဦးကျလော",
        "en": "U KYA LAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016001"
  },
  {
    "votes": 14109,
    "candidate": {
      "name": {
        "my": "စိုင်းဝန်း",
        "en": "Sai Won"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016001"
  },
  {
    "votes": 46057,
    "candidate": {
      "name": {
        "my": "ဦးစတီဖန်",
        "en": "U STEVEN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016001"
  },
  {
    "votes": 1739,
    "candidate": {
      "name": {
        "my": "ဒေါ်နန်းလှ",
        "en": "DAW NANA HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016001"
  },
  {
    "votes": 2700,
    "candidate": {
      "name": {
        "my": "ဦးစောဖီးလစ်",
        "en": "U SAW PHILIP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WNUP",
    "constituency": "MMR016001"
  },
  {
    "votes": 3373,
    "candidate": {
      "name": {
        "my": "ဦးယောသပ်",
        "en": "U YAW THUP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016004"
  },
  {
    "votes": 4627,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ယွန်းမွန်း",
        "en": "U AIK YON MON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR016004"
  },
  {
    "votes": 230,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်လုံနော်ပန်း",
        "en": "U AIK LON NAW PAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016010"
  },
  {
    "votes": 2918,
    "candidate": {
      "name": {
        "my": "ဦးအက်စ်မာဂရက်",
        "en": "U S. Margrat"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016010"
  },
  {
    "votes": 7405,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျောက်",
        "en": "U SAI KYAUT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016010"
  },
  {
    "votes": 484,
    "candidate": {
      "name": {
        "my": "ဦးရေွှသာထွန်း",
        "en": "U SHWE THAR TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016010"
  },
  {
    "votes": 2433,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမျိုးနိုင်",
        "en": "SAI MYO NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016006"
  },
  {
    "votes": 16853,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခတ္တိယ",
        "en": "U SAI KATHI YA'"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016006"
  },
  {
    "votes": 883,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းရဲနောင်",
        "en": "U SAI YE NAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016006"
  },
  {
    "votes": 226,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအိုက်ရီ",
        "en": "U SAI AI YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016002"
  },
  {
    "votes": 1161,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်ဟန်",
        "en": "U SAI KYAW HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016002"
  },
  {
    "votes": 4713,
    "candidate": {
      "name": {
        "my": "ဦးစဲဘီကော်",
        "en": "U SAI BE KAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016002"
  },
  {
    "votes": 283,
    "candidate": {
      "name": {
        "my": " ဦးညိမ်းမောင်",
        "en": "U NYEIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016002"
  },
  {
    "votes": 6400,
    "candidate": {
      "name": {
        "my": "စိုင်းလိတ်",
        "en": "Sai Leik"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016007"
  },
  {
    "votes": 15397,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းစိန်",
        "en": "U SAI TUN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016007"
  },
  {
    "votes": 565,
    "candidate": {
      "name": {
        "my": " ဦးစိုင်းတင်ညွန့်",
        "en": "U SAI TIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016007"
  },
  {
    "votes": 3641,
    "candidate": {
      "name": {
        "my": " ဦးယောသပ်",
        "en": "U YAW THUP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR016007"
  },
  {
    "votes": 3225,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကော်တိုင်း",
        "en": "SAI KAW TAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016008"
  },
  {
    "votes": 8716,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ကြည်",
        "en": "U SAI AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016008"
  },
  {
    "votes": 284,
    "candidate": {
      "name": {
        "my": "ဦးလှထေွး",
        "en": "U HLA HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016008"
  },
  {
    "votes": 136,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ဆမ်း",
        "en": "U AIK SAM"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR016003"
  },
  {
    "votes": 2697,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဝင်း",
        "en": "U TIN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016003"
  },
  {
    "votes": 146,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမိုးလွမ်း",
        "en": "U SAI MOE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016003"
  },
  {
    "votes": 940,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းတစ်",
        "en": "U SAI TIT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016003"
  },
  {
    "votes": 5413,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမား",
        "en": "U SAI MAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR016011"
  },
  {
    "votes": 1804,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလုံ",
        "en": "U SAI LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016011"
  },
  {
    "votes": 9409,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစောတင်",
        "en": "U SAI SAW TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016011"
  },
  {
    "votes": 22255,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းသိန်းအောင်",
        "en": "U SAI THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR016009"
  },
  {
    "votes": 20756,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလာ",
        "en": "U SAI LAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR016009"
  },
  {
    "votes": 17582,
    "candidate": {
      "name": {
        "my": "ဦးကျင်ဝမ်း",
        "en": "U KYIN WAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015021"
  },
  {
    "votes": 294,
    "candidate": {
      "name": {
        "my": "ဦးဆိုင်းနပ်",
        "en": "U SAING NUP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WNUP",
    "constituency": "MMR015021"
  },
  {
    "votes": 10457,
    "candidate": {
      "name": {
        "my": "စိုင်းဝင်းခိုင်",
        "en": "Sai Win Khaing"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015002"
  },
  {
    "votes": 10377,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခွန်ဆေ",
        "en": "U SAI KHUN SAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015002"
  },
  {
    "votes": 1301,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်ထွန်း",
        "en": "U SAI KYAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015002"
  },
  {
    "votes": 2569,
    "candidate": {
      "name": {
        "my": "ဦးမောရစ်",
        "en": "U MORIS"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015002"
  },
  {
    "votes": 18206,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစိန်လင်း",
        "en": "U SAI SEIN LINN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015014"
  },
  {
    "votes": 3371,
    "candidate": {
      "name": {
        "my": " ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MUANG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015014"
  },
  {
    "votes": 36998,
    "candidate": {
      "name": {
        "my": "ဦးရဲထွန်း(ခ)မင်းထွန်း",
        "en": "U YE TUN (a) MIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015014"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကျန်တယ်ဝင်း(ခ)ကျန်တဲဝမ်",
        "en": "U KYAN TAWIN (A) KYAN TEWIN"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015023"
  },
  {
    "votes": 3676,
    "candidate": {
      "name": {
        "my": "ဦးစိန္တာ",
        "en": "U SEIN TAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015020"
  },
  {
    "votes": 9675,
    "candidate": {
      "name": {
        "my": "ဦးဟော်ရေှာက်ချမ်း",
        "en": "U HAW SHOUT CHAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015020"
  },
  {
    "votes": 7583,
    "candidate": {
      "name": {
        "my": "ဦးယန်ကျင်တန်",
        "en": "U WAN KYIN TAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015020"
  },
  {
    "votes": 4090,
    "candidate": {
      "name": {
        "my": "စိုင်းစံအေး",
        "en": "Sai San Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015011"
  },
  {
    "votes": 39976,
    "candidate": {
      "name": {
        "my": "ဦးတီခွန်မြတ်",
        "en": "U TEE KHUN MYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015011"
  },
  {
    "votes": 6329,
    "candidate": {
      "name": {
        "my": "ဦးအယ်လ်ခွန်ဆိုင်",
        "en": "U L.KUN SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015011"
  },
  {
    "votes": 16064,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်ဆိုင်",
        "en": "U AIK SAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015011"
  },
  {
    "votes": 20588,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းနော်ခမ်းဦး",
        "en": "U SAI NAW KUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015012"
  },
  {
    "votes": 37010,
    "candidate": {
      "name": {
        "my": "ဦးနယ်လ်ဆင်(ခ)ဦးဆောင်ဆီ",
        "en": "U NELSON (A) U SAUNG HTEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015012"
  },
  {
    "votes": 15126,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရင်",
        "en": "U TUN YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015012"
  },
  {
    "votes": 29002,
    "candidate": {
      "name": {
        "my": "စိုင်းထွန်းအောင်",
        "en": "Sai Htun Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015001"
  },
  {
    "votes": 43295,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမြင့်အေး",
        "en": "U SAI MYINT AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015001"
  },
  {
    "votes": 7711,
    "candidate": {
      "name": {
        "my": "ဦးမရန်ယော်",
        "en": "U MARAN YAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015001"
  },
  {
    "votes": 15579,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ထွန်း",
        "en": "U ZAW TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KDAUP",
    "constituency": "MMR015001"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": " ဦးကျော်နီနိုင်",
        "en": "U KYAW NI NAING"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015022"
  },
  {
    "votes": 8374,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်လှုိင်",
        "en": "U MYINT HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015018"
  },
  {
    "votes": 5384,
    "candidate": {
      "name": {
        "my": "ဦးစိုးမြင့်ဝင်း",
        "en": "U SOE MYINT WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015018"
  },
  {
    "votes": 5176,
    "candidate": {
      "name": {
        "my": " ဦးစိုင်းသန်းဝင်းအောင်",
        "en": "U SAI THAN WIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015018"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအိုက်မုန်း",
        "en": "U AIK MONG"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015019"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဂျဘ္ကပ်လမ်နပ်",
        "en": "U GUTUP LANUP"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015008"
  },
  {
    "votes": 12920,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုင်းနော်",
        "en": "Dr. Sai Naw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015017"
  },
  {
    "votes": 13865,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်(ခ)ဖိုးတုတ်",
        "en": "U KYAW MYINT (A) POE TOPT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015017"
  },
  {
    "votes": 9713,
    "candidate": {
      "name": {
        "my": "စိုင်းသီဟကျော်",
        "en": "Sai Thi Ha Kyaw"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015003"
  },
  {
    "votes": 5103,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွမ်းဆာ",
        "en": "U SAI TWUN SAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015003"
  },
  {
    "votes": 22380,
    "candidate": {
      "name": {
        "my": "စိုင်းဘိုးအောင်",
        "en": "Sai Boe Aung"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015009"
  },
  {
    "votes": 18648,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းယီအင်",
        "en": "U SAI IAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015009"
  },
  {
    "votes": 8612,
    "candidate": {
      "name": {
        "my": "ဦးခွန်ဆိုင်(ခ)ခွန်ကျိန်",
        "en": "U KHUN SAING (A) KUNG GYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015009"
  },
  {
    "votes": 14570,
    "candidate": {
      "name": {
        "my": "ဦးဌေးလွင်(ခ)ဦးဒီလုံး",
        "en": "U HTWE LWIN (A) DE LONE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015016"
  },
  {
    "votes": 4535,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းသိန်း",
        "en": "U WIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015016"
  },
  {
    "votes": 11591,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဟောင်ခမ်း",
        "en": "U SAI HAUNG KHAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015016"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဒေါ်စောနန်းလုံ",
        "en": "DAW SAW NAN LON"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015006"
  },
  {
    "votes": 10488,
    "candidate": {
      "name": {
        "my": "စိုင်းအောင်လှ",
        "en": "Sai Aung Hla"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015015"
  },
  {
    "votes": 8405,
    "candidate": {
      "name": {
        "my": "ဦးစန်းလွင်",
        "en": "SAI SAN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015015"
  },
  {
    "votes": 11508,
    "candidate": {
      "name": {
        "my": "ဦးချစ်တင်",
        "en": "U CHIT TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015010"
  },
  {
    "votes": 979,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှအောင်",
        "en": "U SAI HLA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015010"
  },
  {
    "votes": 21350,
    "candidate": {
      "name": {
        "my": " ဒေါက်တာစိုင်းကျော်အုန်း",
        "en": "DR. SAI KYAW OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015010"
  },
  {
    "votes": 15462,
    "candidate": {
      "name": {
        "my": "ဦးရေွွှမောင်",
        "en": "U SHWE MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "TNP",
    "constituency": "MMR015010"
  },
  {
    "votes": 20710,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ချို",
        "en": "U KHIN MAUNG CHO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015013"
  },
  {
    "votes": 22701,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မြင့်",
        "en": "U KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015013"
  },
  {
    "votes": 14947,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဖူးအောင်",
        "en": "U SAI PU AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015013"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်ထွန်း",
        "en": "U SAI AUNG TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015005"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးဂျစ်ဆိန်း",
        "en": "U JEP SEAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015007"
  },
  {
    "votes": 13424,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဆာ(ခ)ဦးစိုင်းထွန်းဝင်း",
        "en": "TUN SAR (A) SAI TUN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR015004"
  },
  {
    "votes": 11352,
    "candidate": {
      "name": {
        "my": "ဥိးစိုင်းခွန်လိှုင်",
        "en": "U SAI KHUN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR015004"
  },
  {
    "votes": 1955,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမြင့်ဆေွ",
        "en": "U SAI MYINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR015004"
  },
  {
    "votes": 1661,
    "candidate": {
      "name": {
        "my": "ဦးပီတာ(ခ)ဦးဆမ်ဗရား",
        "en": "U PETER (A) SAN BARAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WDP",
    "constituency": "MMR015004"
  },
  {
    "votes": 1946,
    "candidate": {
      "name": {
        "my": "ဦးလုပ်ပေါင်း",
        "en": "U LOT PAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "WNUP",
    "constituency": "MMR015004"
  },
  {
    "votes": 2551,
    "candidate": {
      "name": {
        "my": "ဦးစောရီ",
        "en": "U SAW YE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "LHNDP",
    "constituency": "MMR015004"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခွန်သိန်းဖေ",
        "en": "U KHUN THEIN PAE"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014003"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": " ဦးဝင်းကို",
        "en": " U WIN KO"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014004"
  },
  {
    "votes": 27848,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်မြင့်",
        "en": "U MAUNG MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014005"
  },
  {
    "votes": 52543,
    "candidate": {
      "name": {
        "my": "ဦးတင်နိုင်သိန်း",
        "en": "U TIN NAING THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014005"
  },
  {
    "votes": 8255,
    "candidate": {
      "name": {
        "my": "နန်းဝါနု",
        "en": "Nan Wah Nu"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014014"
  },
  {
    "votes": 5665,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဖုန်းခေါင်",
        "en": "U SAI PHONE KHAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014014"
  },
  {
    "votes": 4152,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်ခိုင်",
        "en": "U SAI KYAW KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014015"
  },
  {
    "votes": 12992,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအွမ်ဆိုင်မိုင်း",
        "en": "SAI AONG SAING MAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014015"
  },
  {
    "votes": 9406,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလှကျော်",
        "en": "SAI HLA KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014012"
  },
  {
    "votes": 6966,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းစံဝင်း",
        "en": "U SAI SAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014012"
  },
  {
    "votes": 9774,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းမောင်တင်",
        "en": "U SAI MAUNG TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014018"
  },
  {
    "votes": 2637,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းထွန်းအောင်",
        "en": "U SAI TUN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014018"
  },
  {
    "votes": 1389,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းဘုန်းမြင့်",
        "en": "U SAI BONE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014018"
  },
  {
    "votes": 46821,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014008"
  },
  {
    "votes": 15545,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဦး",
        "en": "U TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014008"
  },
  {
    "votes": 11481,
    "candidate": {
      "name": {
        "my": "ဦးစပ်ဆိုင်မိုင်း",
        "en": "U SAP SAING MAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014011"
  },
  {
    "votes": 18039,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်သီ",
        "en": "U KHIN MUANG THI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014011"
  },
  {
    "votes": 6743,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းလုံ",
        "en": "U SAI LONG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014011"
  },
  {
    "votes": 4422,
    "candidate": {
      "name": {
        "my": "စိုင်းတင်အောင် (ခ) စိုင်းအွတ်",
        "en": "Sai Tin Aung (a) Sai Oak"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014020"
  },
  {
    "votes": 5181,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းငေါင်းဆိုင်ဟိန်း",
        "en": "U SAI NAUNG SAING HAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014020"
  },
  {
    "votes": 7567,
    "candidate": {
      "name": {
        "my": "စိုင်းလောင်ပန်ဖ",
        "en": "Sai Lao Pan Pha"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014017"
  },
  {
    "votes": 8576,
    "candidate": {
      "name": {
        "my": " ဦးကျော်တင်ရေွှ",
        "en": "U KYAW TIN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014017"
  },
  {
    "votes": 7358,
    "candidate": {
      "name": {
        "my": "စိုင်းထွန်းအေး",
        "en": "Sai Htun Aye"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014016"
  },
  {
    "votes": 2166,
    "candidate": {
      "name": {
        "my": " ဦးစိုင်းလုံ",
        "en": "U SAI LONG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014016"
  },
  {
    "votes": 8349,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းကျော်မြင့်",
        "en": "U SAI KYAW MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014019"
  },
  {
    "votes": 4257,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းအောင်သန်း",
        "en": "U SAI AUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014019"
  },
  {
    "votes": 3929,
    "candidate": {
      "name": {
        "my": "စိုင်းထွန်းမြင့်ဦး",
        "en": "U Htun Myint Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014021"
  },
  {
    "votes": 3319,
    "candidate": {
      "name": {
        "my": "ဦးစိုင်းခမ်းစင်",
        "en": "U SAI KHAM SIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014021"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ဆေွ",
        "en": "U MAUNG MAUNG SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014013"
  },
  {
    "votes": 30048,
    "candidate": {
      "name": {
        "my": "ဦးစံရေွှ",
        "en": "U SAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014002"
  },
  {
    "votes": 7239,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လင်း",
        "en": "U AUNG LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014002"
  },
  {
    "votes": 52195,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဆေွ",
        "en": "U WIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "INDP",
    "constituency": "MMR014002"
  },
  {
    "votes": 12743,
    "candidate": {
      "name": {
        "my": "ခွန်ယူဂျင်း",
        "en": "U KHUN EUGENE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KNP2",
    "constituency": "MMR014010"
  },
  {
    "votes": 15028,
    "candidate": {
      "name": {
        "my": "ဦးဖရန်စစ္စကို",
        "en": "U FRANCISCO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014010"
  },
  {
    "votes": 7826,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သန်း",
        "en": "U KYAW THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014010"
  },
  {
    "votes": 26459,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းကြည်",
        "en": "U WIN KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014006"
  },
  {
    "votes": 10901,
    "candidate": {
      "name": {
        "my": " ဦးအုန်းရေွှ",
        "en": "U OHN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014006"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးခွန်မောင်သောင်း",
        "en": "U KHUN MAUNG THAUNG"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PNO",
    "constituency": "MMR014009"
  },
  {
    "votes": 76983,
    "candidate": {
      "name": {
        "my": "ဦးစံလင်း",
        "en": "U SAN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014001"
  },
  {
    "votes": 31415,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MUANG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014001"
  },
  {
    "votes": 23165,
    "candidate": {
      "name": {
        "my": "စပ်ယွန်းပိုင်(ခ)စောယွန်းပိုင်",
        "en": "SAP WONG PAING (A) SAW WONG PAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "SNDP",
    "constituency": "MMR014001"
  },
  {
    "votes": 33338,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဇေယျ(ခ)ဦးခွန်ရဲသေွး",
        "en": "U KYAW ZAYAR (A) KHUN YE THWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014001"
  },
  {
    "votes": 9502,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဦး",
        "en": "U ZAW OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR014007"
  },
  {
    "votes": 2624,
    "candidate": {
      "name": {
        "my": " ဦးလှထွဏ်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR014007"
  },
  {
    "votes": 23922,
    "candidate": {
      "name": {
        "my": " ဦးအောင်သိန်း",
        "en": "U AUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR014007"
  },
  {
    "votes": 20746,
    "candidate": {
      "name": {
        "my": "ဦးအောင်လင်းလှုိင်",
        "en": "U AUNG LIN KHAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006010"
  },
  {
    "votes": 10281,
    "candidate": {
      "name": {
        "my": "ဦးရေွှထွန်း",
        "en": "U SHWE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006010"
  },
  {
    "votes": 14531,
    "candidate": {
      "name": {
        "my": "ဦးတိုးပို",
        "en": "U Toe Po"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR006001"
  },
  {
    "votes": 28119,
    "candidate": {
      "name": {
        "my": "ဦးကြည်မင်း",
        "en": "U KYI MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006001"
  },
  {
    "votes": 11819,
    "candidate": {
      "name": {
        "my": "ဦးသန်းလှ",
        "en": "U THAN HLA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006001"
  },
  {
    "votes": 29350,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ထွန်း",
        "en": "U MYINT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006009"
  },
  {
    "votes": 14491,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းရှိန်",
        "en": "U THAUNG SHAIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006009"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသိန်း",
        "en": "U SOE THU"
      }
    },
    "acclaim": "Y",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006006"
  },
  {
    "votes": 37620,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာကျော်ကျော်ဌေး",
        "en": "DR. KYAW KYAW TAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006002"
  },
  {
    "votes": 17819,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006002"
  },
  {
    "votes": 74843,
    "candidate": {
      "name": {
        "my": "ဦးဌေးမြင့်",
        "en": "U HTAY MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006005"
  },
  {
    "votes": 36729,
    "candidate": {
      "name": {
        "my": "ဦီးပြည်အေး",
        "en": "U PYI AYE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006005"
  },
  {
    "votes": 47299,
    "candidate": {
      "name": {
        "my": "ဦးမောင်လှ(ခ)ဦးလှမြင့်",
        "en": "U MAUNG HLA (A) U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006007"
  },
  {
    "votes": 9627,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးညွန့်",
        "en": "U MYO NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006007"
  },
  {
    "votes": 20482,
    "candidate": {
      "name": {
        "my": "ဦးယုံဘီး",
        "en": "U YON BEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006008"
  },
  {
    "votes": 19101,
    "candidate": {
      "name": {
        "my": "ဦးစိုးရီ",
        "en": "U SOE YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006008"
  },
  {
    "votes": 26970,
    "candidate": {
      "name": {
        "my": "ဦးကင်းစိန်",
        "en": "U KIN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006003"
  },
  {
    "votes": 14977,
    "candidate": {
      "name": {
        "my": " ဦးညိုသိမ်း",
        "en": "U NYO THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006003"
  },
  {
    "votes": 11096,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သက်",
        "en": "U Aung Thet"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR006004"
  },
  {
    "votes": 14377,
    "candidate": {
      "name": {
        "my": "ဦးလှအောင်",
        "en": "U HLA AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR006004"
  },
  {
    "votes": 17452,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းဦး",
        "en": "U WIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR006004"
  },
  {
    "votes": 8899,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းမြင့်",
        "en": "U OHN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013037"
  },
  {
    "votes": 1360,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်အောင်",
        "en": "U MYINT AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013037"
  },
  {
    "votes": 1949,
    "candidate": {
      "name": {
        "my": "ဒေါ်မြမြအောင်",
        "en": "DAW MAY MAY AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013037"
  },
  {
    "votes": 805,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းကြည်",
        "en": "U THEIN KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR013037"
  },
  {
    "votes": 13683,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရည်(ခ)ဦးခိုင်မောင်ရီ",
        "en": "U KHIN MAUNG YEE (A) U KHIANG MAUNG YI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013037"
  },
  {
    "votes": 15769,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမောင်မောင်ဝင့်",
        "en": "DR. MAUNG MAUNG WINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013044"
  },
  {
    "votes": 3099,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းကြိုင်",
        "en": "U OHN KYAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013044"
  },
  {
    "votes": 5052,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ငြိမ်း",
        "en": "U AUNG NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013044"
  },
  {
    "votes": 14786,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မြတ်ထွန်း",
        "en": "U AUNG MYAT TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013044"
  },
  {
    "votes": 1086,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းထိန်ဦး",
        "en": "U Win Htein Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013017"
  },
  {
    "votes": 10902,
    "candidate": {
      "name": {
        "my": "ဦးစိန်မြင့်",
        "en": "U SEIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013017"
  },
  {
    "votes": 8081,
    "candidate": {
      "name": {
        "my": "ဦးမင်းအောင်",
        "en": "U MIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013017"
  },
  {
    "votes": 653,
    "candidate": {
      "name": {
        "my": "ဦးအေးဖေ",
        "en": "U AYE PE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013032"
  },
  {
    "votes": 122,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းအောင်",
        "en": "U THEIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013032"
  },
  {
    "votes": 6608,
    "candidate": {
      "name": {
        "my": "ဦးနန္ဒာကျော်စွာ",
        "en": "U NANDA KYAW SWA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013043"
  },
  {
    "votes": 1128,
    "candidate": {
      "name": {
        "my": "ဦးမြစိန်",
        "en": "U MYA SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013043"
  },
  {
    "votes": 3473,
    "candidate": {
      "name": {
        "my": "ဦးသာမြင့်ထူး",
        "en": "U THAR MYINT HTOO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013043"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သူ",
        "en": "U MYINT THU"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013020"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးအေးစိန်",
        "en": "U AYE SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013020"
  },
  {
    "votes": 0,
    "candidate": {
      "name": {
        "my": "ဦးကြီးမြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013020"
  },
  {
    "votes": 4352,
    "candidate": {
      "name": {
        "my": "ဦးကမ်ခိုပေါင်",
        "en": "U KAM KHO PAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013019"
  },
  {
    "votes": 32736,
    "candidate": {
      "name": {
        "my": "ဦးဇော်လွင်",
        "en": "U ZAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013019"
  },
  {
    "votes": 7883,
    "candidate": {
      "name": {
        "my": "ဦးမြစိုး",
        "en": "U MYA SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013019"
  },
  {
    "votes": 4575,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဇော်အောင်",
        "en": "U THAN ZAW AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013019"
  },
  {
    "votes": 34800,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်နွယ်ဦး",
        "en": "DAW TIN NWE OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013019"
  },
  {
    "votes": 8742,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းဝ",
        "en": "U TUN WA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013021"
  },
  {
    "votes": 20119,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထိုက်",
        "en": "U THEIN HTAIK"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013021"
  },
  {
    "votes": 7554,
    "candidate": {
      "name": {
        "my": "ှုဦးဝင်းသိန်း",
        "en": "U WIN THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013021"
  },
  {
    "votes": 61875,
    "candidate": {
      "name": {
        "my": "ဒေါ်စုစုလှိုင်",
        "en": "DAW SU SU HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013018"
  },
  {
    "votes": 9600,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဆေွ",
        "en": "U TINT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013018"
  },
  {
    "votes": 46828,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဇေယျဝင်း",
        "en": "U KYAW ZEYA WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013018"
  },
  {
    "votes": 14346,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013018"
  },
  {
    "votes": 2575,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဇော်ဦး",
        "en": "U Than Zaw Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013030"
  },
  {
    "votes": 33460,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်းဦး",
        "en": "U HLA TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013030"
  },
  {
    "votes": 5866,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်မြင့်",
        "en": "U KHIN MAUNG MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013030"
  },
  {
    "votes": 3522,
    "candidate": {
      "name": {
        "my": "ဦးစန်းအောင်",
        "en": "U SAN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013030"
  },
  {
    "votes": 11033,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ဝင်း",
        "en": "U AUNG KYAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013030"
  },
  {
    "votes": 7053,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်တင်ရီ",
        "en": "DAW TIN TIN YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013030"
  },
  {
    "votes": 14739,
    "candidate": {
      "name": {
        "my": "ဦးလှမြင့်",
        "en": "U HLA MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013014"
  },
  {
    "votes": 6697,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းထွန်း",
        "en": "U THEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013014"
  },
  {
    "votes": 1430,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ပိုင်(ခ)ဦးအပါ",
        "en": "U AUNG PAING (a) U PAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013014"
  },
  {
    "votes": 12147,
    "candidate": {
      "name": {
        "my": "ဦးမင်းမင်းထွန်း",
        "en": "U MIN MIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013014"
  },
  {
    "votes": 8270,
    "candidate": {
      "name": {
        "my": "ဒေါ်ယူဇာမော်ထွန်း",
        "en": "Daw Wuzar Maw Htun"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013040"
  },
  {
    "votes": 5255,
    "candidate": {
      "name": {
        "my": "ဦးသူရထွန်း",
        "en": "U THUYA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013040"
  },
  {
    "votes": 24238,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဝင်းမြင့်",
        "en": "DR. WIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013040"
  },
  {
    "votes": 4993,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013040"
  },
  {
    "votes": 17921,
    "candidate": {
      "name": {
        "my": "ဦးခင်ကျော်",
        "en": "U KHIN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013040"
  },
  {
    "votes": 3136,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဦး",
        "en": "U Khin Oo"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013008"
  },
  {
    "votes": 2561,
    "candidate": {
      "name": {
        "my": "ဦးပြည်သူ (ခ) ဦးစိန်လှ",
        "en": "U Pyithu (aka) U Sein Hla"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013008"
  },
  {
    "votes": 18606,
    "candidate": {
      "name": {
        "my": "ဒေါ်ညွန့် ယဉ်ဝင်း",
        "en": "Daw Nyunt Yin Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013008"
  },
  {
    "votes": 2167,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်းနိုင်",
        "en": "U Kyaw Min Naing"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013008"
  },
  {
    "votes": 44317,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သိန်း",
        "en": "U MYINT THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013008"
  },
  {
    "votes": 54841,
    "candidate": {
      "name": {
        "my": "ဦးဉာဏ်ဝင်း",
        "en": "U NYAN WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013008"
  },
  {
    "votes": 34558,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ထေွး",
        "en": "U KHIN MAUNG HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013008"
  },
  {
    "votes": 7561,
    "candidate": {
      "name": {
        "my": "ဦးဌေး",
        "en": "U HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP2",
    "constituency": "MMR013008"
  },
  {
    "votes": 1830,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဇော်",
        "en": "U MYINT ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013008"
  },
  {
    "votes": 81625,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်စိုး",
        "en": "U KHIN MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013004"
  },
  {
    "votes": 10234,
    "candidate": {
      "name": {
        "my": "ဒေါ်သိန်းဌေး",
        "en": "DAW THEIN HTAY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013004"
  },
  {
    "votes": 13815,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ခိုင်",
        "en": "U MYINT KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013004"
  },
  {
    "votes": 65278,
    "candidate": {
      "name": {
        "my": "ဦးရီမြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013003"
  },
  {
    "votes": 14558,
    "candidate": {
      "name": {
        "my": "ဦးညွန့်ရေွှ",
        "en": "U NYUNT SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013003"
  },
  {
    "votes": 11151,
    "candidate": {
      "name": {
        "my": "ဦးဇော်လွင်",
        "en": "U ZAW LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013003"
  },
  {
    "votes": 5069,
    "candidate": {
      "name": {
        "my": "ဦးစောစံနီဘွိုင်း",
        "en": "U SAW SONNY BOY"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013003"
  },
  {
    "votes": 4908,
    "candidate": {
      "name": {
        "my": "ဦးမင်းမင်းထွန်း",
        "en": "U MIN MIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013006"
  },
  {
    "votes": 37238,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းရီ",
        "en": "U THEIN YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013006"
  },
  {
    "votes": 10775,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဒွန်း",
        "en": "U KYAW DUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013006"
  },
  {
    "votes": 8417,
    "candidate": {
      "name": {
        "my": "ဦးမိုးကျော်",
        "en": "U MOE KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013006"
  },
  {
    "votes": 19500,
    "candidate": {
      "name": {
        "my": "ဦးလှထွန်း",
        "en": "U HLA TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013001"
  },
  {
    "votes": 57195,
    "candidate": {
      "name": {
        "my": "ဦးအေးမြင့်",
        "en": "U AYE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013001"
  },
  {
    "votes": 17789,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းတင်",
        "en": "U THEIN TIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013001"
  },
  {
    "votes": 22099,
    "candidate": {
      "name": {
        "my": "ဦးမိုးထွန်း",
        "en": "U MOE TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013001"
  },
  {
    "votes": 4743,
    "candidate": {
      "name": {
        "my": "ဦးအေးနိုင်",
        "en": "U AYE NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR013001"
  },
  {
    "votes": 6237,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာဖုန်းဝင်း",
        "en": "Dr. Phone Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013041"
  },
  {
    "votes": 1525,
    "candidate": {
      "name": {
        "my": "ဦးအောင်မျိုးထွန်း",
        "en": "U AUNG MYO TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013041"
  },
  {
    "votes": 9143,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာစိုးရင်",
        "en": "DR. SOE YIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013041"
  },
  {
    "votes": 2240,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာမင်းမြင့်",
        "en": "DR. MIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013041"
  },
  {
    "votes": 846,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်းမြင့်",
        "en": "U AUNG THAN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013041"
  },
  {
    "votes": 8971,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မင်းလှုိင်",
        "en": "U KYAW MIN HLAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013041"
  },
  {
    "votes": 42176,
    "candidate": {
      "name": {
        "my": "ဦးစိုးတင့်",
        "en": "U SOE TINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013028"
  },
  {
    "votes": 13944,
    "candidate": {
      "name": {
        "my": "ဦးအုန်းသောင်း",
        "en": "U OHN THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013028"
  },
  {
    "votes": 1638,
    "candidate": {
      "name": {
        "my": "ဦးဝင်းမောင်",
        "en": "U WIN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "MPP",
    "constituency": "MMR013028"
  },
  {
    "votes": 4757,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်သောင်း",
        "en": "U MYINT THAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013028"
  },
  {
    "votes": 256,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းတန်",
        "en": "U Thein Tan"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013026"
  },
  {
    "votes": 63979,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်သိန်း",
        "en": "U MAUNG MAUNG THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013026"
  },
  {
    "votes": 12509,
    "candidate": {
      "name": {
        "my": "ဦးသန်းမောင်",
        "en": "U THAN MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013026"
  },
  {
    "votes": 9388,
    "candidate": {
      "name": {
        "my": "ဦးကျော်သူရ",
        "en": "U KYAW THUYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013026"
  },
  {
    "votes": 57731,
    "candidate": {
      "name": {
        "my": "ဦးလွန်းသီ",
        "en": "U LUN THI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013029"
  },
  {
    "votes": 5304,
    "candidate": {
      "name": {
        "my": "ဦးမင်းနိုင်",
        "en": "U MIN NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013029"
  },
  {
    "votes": 3639,
    "candidate": {
      "name": {
        "my": "ဦးတင်လင်း",
        "en": "U TIN LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013029"
  },
  {
    "votes": 418,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရေွှ",
        "en": "U Tun Shwe"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Wunthanu NLD (The Union Of Myanmar)",
    "constituency": "MMR013033"
  },
  {
    "votes": 864,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဇော်ဝင်း",
        "en": "U KHIN ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013033"
  },
  {
    "votes": 4668,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013033"
  },
  {
    "votes": 1235,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ညွန့်",
        "en": "U KYAW NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013033"
  },
  {
    "votes": 1108,
    "candidate": {
      "name": {
        "my": "ဦးသန့်ဇင်မြင့်",
        "en": "U THANT ZIN MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013033"
  },
  {
    "votes": 119,
    "candidate": {
      "name": {
        "my": "စောဝင်း",
        "en": "SAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "PDP2",
    "constituency": "MMR013033"
  },
  {
    "votes": 4065,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013033"
  },
  {
    "votes": 31738,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကျော်ခင်",
        "en": "U AUNG KYAW KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013024"
  },
  {
    "votes": 11366,
    "candidate": {
      "name": {
        "my": "ဦးစိုးလွင်",
        "en": "U SOE LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013024"
  },
  {
    "votes": 30996,
    "candidate": {
      "name": {
        "my": "ဦးကြည်သန်း",
        "en": "U KYI THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013024"
  },
  {
    "votes": 6128,
    "candidate": {
      "name": {
        "my": "ဦးကျော်မိုးလွင်",
        "en": "U KYAW MOE LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013038"
  },
  {
    "votes": 19225,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိန်",
        "en": "U THAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013038"
  },
  {
    "votes": 4280,
    "candidate": {
      "name": {
        "my": "ဦးသန်းစိန်",
        "en": "U THAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013038"
  },
  {
    "votes": 966,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်းဦး",
        "en": "U AUNG San OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": " Peace and Diversity Party",
    "constituency": "MMR013038"
  },
  {
    "votes": 14920,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်ကြီး",
        "en": "U MAUNG MAUNG GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013038"
  },
  {
    "votes": 335,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဌေး",
        "en": "U Khin Maung Htay"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013035"
  },
  {
    "votes": 254,
    "candidate": {
      "name": {
        "my": "ဦးသက်ထွန်း",
        "en": "U Thet Tun"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013035"
  },
  {
    "votes": 747,
    "candidate": {
      "name": {
        "my": "ဒေါ်မြင့်မြင့်စိန်",
        "en": "DAW MYINT MYINT SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013035"
  },
  {
    "votes": 4333,
    "candidate": {
      "name": {
        "my": "ဦးမင်းမင်းထွန်းမြတ်",
        "en": "U MIN MIN TUN MYAT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013035"
  },
  {
    "votes": 1578,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ရီ (ခ) ဦးရီ",
        "en": "U KHIN MAUNG YEE(a)U YEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013035"
  },
  {
    "votes": 1145,
    "candidate": {
      "name": {
        "my": "ဦးဘိုးမင်္ဂလာ",
        "en": "U BO MINGALAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013035"
  },
  {
    "votes": 6830,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဝင်း",
        "en": "U KHIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013035"
  },
  {
    "votes": 3487,
    "candidate": {
      "name": {
        "my": "ဒေါ်သင်းသင်းမိုး",
        "en": "DAW THIN THIN MOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013036"
  },
  {
    "votes": 1309,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခေခိုင်အောင်ခင်",
        "en": "DAW KAY KHINE AUNG KHIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013036"
  },
  {
    "votes": 5289,
    "candidate": {
      "name": {
        "my": "ဦးကြည်မြင့်",
        "en": "U KYEE MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013036"
  },
  {
    "votes": 9488,
    "candidate": {
      "name": {
        "my": "ဦးသန်းဦး",
        "en": "U THAN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013042"
  },
  {
    "votes": 41183,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာပွင့်ဆန်း",
        "en": "DR. PWINT SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013042"
  },
  {
    "votes": 8925,
    "candidate": {
      "name": {
        "my": "ဦးလွန်းစိန်",
        "en": "U LONE SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013042"
  },
  {
    "votes": 23305,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်ဖြူဖြူငြိမ်း",
        "en": "DAW KHIN PHYU PHYU NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013042"
  },
  {
    "votes": 3524,
    "candidate": {
      "name": {
        "my": "ဦးကျော်ဇေယျ",
        "en": "U KYAW ZAYAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR013042"
  },
  {
    "votes": 74170,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဝင်း",
        "en": "U TIN MAUNG WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013002"
  },
  {
    "votes": 15739,
    "candidate": {
      "name": {
        "my": "ဦးဇော်မင်း",
        "en": "U ZAW MIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013002"
  },
  {
    "votes": 15612,
    "candidate": {
      "name": {
        "my": "ဦးကြည်ဝင်း",
        "en": "U KYI WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NPAL",
    "constituency": "MMR013002"
  },
  {
    "votes": 6292,
    "candidate": {
      "name": {
        "my": "ဦးညိုမင်းလွင်",
        "en": "U NYO MIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": " Peace and Diversity Party",
    "constituency": "MMR013002"
  },
  {
    "votes": 20257,
    "candidate": {
      "name": {
        "my": "ဦးသုဝေ",
        "en": "U THU WEI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013022"
  },
  {
    "votes": 1717,
    "candidate": {
      "name": {
        "my": "ဦးလှမျိုးမြင့်",
        "en": "U Hla Myo Myint"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013022"
  },
  {
    "votes": 28566,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ကြည်",
        "en": "U AUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013022"
  },
  {
    "votes": 4292,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်လွင်",
        "en": "U KHIN MAUNG LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013022"
  },
  {
    "votes": 5442,
    "candidate": {
      "name": {
        "my": "ဦးဇော်ဝင်း",
        "en": "U ZAW WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR013022"
  },
  {
    "votes": 53396,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာသန်းဝင်း",
        "en": "(Dr.) Than Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013012"
  },
  {
    "votes": 18739,
    "candidate": {
      "name": {
        "my": "ဦးဘတင့်ဆေွ",
        "en": "U Ba Tint Swe"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013012"
  },
  {
    "votes": 5819,
    "candidate": {
      "name": {
        "my": "ဦးဖိုးထောင်",
        "en": "U PHO HTAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013012"
  },
  {
    "votes": 45584,
    "candidate": {
      "name": {
        "my": "ဦးမြင့်ဟန်",
        "en": "U MYINT HAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013012"
  },
  {
    "votes": 10318,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သန်း",
        "en": "U AUNG THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013012"
  },
  {
    "votes": 1562,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာတင်အောင်ရေွှ",
        "en": "Dr. Tin Aung Shwe"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013034"
  },
  {
    "votes": 794,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထေွး",
        "en": "U THAN HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013034"
  },
  {
    "votes": 1990,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ဆေွ",
        "en": "U KHIN MAUNG SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDPD",
    "constituency": "MMR013034"
  },
  {
    "votes": 4261,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထေွး",
        "en": "U TIN HTWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013034"
  },
  {
    "votes": 5447,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဆန်း",
        "en": "U AUNG SAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013034"
  },
  {
    "votes": 258,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးညွန့်",
        "en": "U MYO NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR013034"
  },
  {
    "votes": 2565,
    "candidate": {
      "name": {
        "my": "ဦးရန်ကျော် (ခ) ဦးနေဝင်း",
        "en": "U Yan Kyaw (aka) U Nay Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013016"
  },
  {
    "votes": 7245,
    "candidate": {
      "name": {
        "my": "ဦးတင်ထွန်းဦး",
        "en": "U TIN TUN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013016"
  },
  {
    "votes": 1131,
    "candidate": {
      "name": {
        "my": "ဦးလှစိန်",
        "en": "U HLA SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013016"
  },
  {
    "votes": 7335,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ဇင်",
        "en": "U AUNG ZIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013016"
  },
  {
    "votes": 5162,
    "candidate": {
      "name": {
        "my": "ဒေါ်နေရီဗဆေွ",
        "en": "DAW NAY YI BA SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013039"
  },
  {
    "votes": 2939,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းလွင်",
        "en": "U THEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013039"
  },
  {
    "votes": 12286,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာပိုင်စိုး",
        "en": "DR.PAING SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013039"
  },
  {
    "votes": 2938,
    "candidate": {
      "name": {
        "my": "ဦးညွန်ဆေွ",
        "en": "U NYUNT SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013039"
  },
  {
    "votes": 13713,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဝင်း",
        "en": "U SOE WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013039"
  },
  {
    "votes": 8701,
    "candidate": {
      "name": {
        "my": "ဦးမောင်ပါ",
        "en": "U MAUNG PAR"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013031"
  },
  {
    "votes": 942,
    "candidate": {
      "name": {
        "my": "ဦးမြသန်း",
        "en": "U MYA THAN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013031"
  },
  {
    "votes": 4283,
    "candidate": {
      "name": {
        "my": "ဦးထက်အောင်ကျော်",
        "en": "U HTET AUNG KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013031"
  },
  {
    "votes": 1024,
    "candidate": {
      "name": {
        "my": "ဒေါက်တာနေလင်း",
        "en": "DR. NE LIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013045"
  },
  {
    "votes": 97,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်အုန်း",
        "en": "U KHIN MAUNG OHN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013045"
  },
  {
    "votes": 392,
    "candidate": {
      "name": {
        "my": "ဦးစိုးဦး",
        "en": "U SOE OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013045"
  },
  {
    "votes": 2745,
    "candidate": {
      "name": {
        "my": "ဒေါ်ခင်သိန်းဝင်း",
        "en": "Daw Khin Thein Win"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013007"
  },
  {
    "votes": 16374,
    "candidate": {
      "name": {
        "my": "ဦးနိုင်ထွန်းကျော်",
        "en": "U NAING TUN KYAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013007"
  },
  {
    "votes": 47871,
    "candidate": {
      "name": {
        "my": "ဦးတင်မောင်ဦး",
        "en": "U TIN MAUNG OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013007"
  },
  {
    "votes": 13606,
    "candidate": {
      "name": {
        "my": "ဦးစိန်လွင်",
        "en": "U SEIN LWIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013007"
  },
  {
    "votes": 27497,
    "candidate": {
      "name": {
        "my": "ဦးကံဝင်းဇော်",
        "en": "U KAN WIN ZAW"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013007"
  },
  {
    "votes": 6585,
    "candidate": {
      "name": {
        "my": "ဦးကောင်းမြင့်ထွဋ်",
        "en": "U Kaung Myint Htut"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013011"
  },
  {
    "votes": 43334,
    "candidate": {
      "name": {
        "my": "ဦးအောင်သိန်းလင်း",
        "en": "U AUNG THEIN LINN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013011"
  },
  {
    "votes": 4521,
    "candidate": {
      "name": {
        "my": "ဦးစိန်ထွန်း",
        "en": "U SEIN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013011"
  },
  {
    "votes": 3664,
    "candidate": {
      "name": {
        "my": "ဒေါ်တင်တင်မြ",
        "en": "DAW TIN TIN MYA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013011"
  },
  {
    "votes": 24529,
    "candidate": {
      "name": {
        "my": "ဒေါ်ကမ်းခန့်ဒိမ်း",
        "en": "DAW KAM KHANT DEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013011"
  },
  {
    "votes": 15503,
    "candidate": {
      "name": {
        "my": "ဦးခင်မောင်ကြွယ်",
        "en": "U Khin Maung Kyawel"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013005"
  },
  {
    "votes": 65640,
    "candidate": {
      "name": {
        "my": "ဦးဟန်စိန်",
        "en": "U HAN SEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013005"
  },
  {
    "votes": 36224,
    "candidate": {
      "name": {
        "my": "ဦးစိုးငြိမ်း",
        "en": "U SOE NYEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013005"
  },
  {
    "votes": 12054,
    "candidate": {
      "name": {
        "my": "ဦးဦးမြင့်",
        "en": "U OO MYINT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013005"
  },
  {
    "votes": 4966,
    "candidate": {
      "name": {
        "my": "ဦးတင်ဆန်း",
        "en": "U Tin San"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "Independent Candidates",
    "constituency": "MMR013015"
  },
  {
    "votes": 20424,
    "candidate": {
      "name": {
        "my": "ဦးကိုကိုနိုင်",
        "en": "U KO KO NAING"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013015"
  },
  {
    "votes": 23032,
    "candidate": {
      "name": {
        "my": "ဒေါ်လဲ့လဲ့ဝင်းဆေွ",
        "en": "DAW LEI LEI WIN SWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013015"
  },
  {
    "votes": 7795,
    "candidate": {
      "name": {
        "my": "ဦးအောင်ညွန့်",
        "en": "U AUNG NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013015"
  },
  {
    "votes": 15161,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းတင်အောင်",
        "en": "U THEIN TIN AUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "UDP2",
    "constituency": "MMR013015"
  },
  {
    "votes": 3747,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013015"
  },
  {
    "votes": 14572,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထိုက်ဦး",
        "en": "U THAN HTAIK OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "T8GSYOM",
    "constituency": "MMR013013"
  },
  {
    "votes": 42471,
    "candidate": {
      "name": {
        "my": "ဦးမောင်မောင်စိုး",
        "en": "U MAUNG MAUNG SOE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013013"
  },
  {
    "votes": 11459,
    "candidate": {
      "name": {
        "my": "ဦးသောင်းအိ",
        "en": "UTHAUNG EI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013013"
  },
  {
    "votes": 39618,
    "candidate": {
      "name": {
        "my": "ဦးတင့်ဝေ",
        "en": "U TINT WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013013"
  },
  {
    "votes": 59921,
    "candidate": {
      "name": {
        "my": "ဦးလွင်ဦး",
        "en": "U LWIN OO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013023"
  },
  {
    "votes": 15421,
    "candidate": {
      "name": {
        "my": "ဦးဟန်ရေွှ",
        "en": "U HAN SHWE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013023"
  },
  {
    "votes": 4220,
    "candidate": {
      "name": {
        "my": "ဦးစိုးကြည်",
        "en": "U SOE KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DP",
    "constituency": "MMR013023"
  },
  {
    "votes": 29051,
    "candidate": {
      "name": {
        "my": "ဦးအောင်နိုင်ထွန်း",
        "en": "U AUNG NAING TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013023"
  },
  {
    "votes": 37370,
    "candidate": {
      "name": {
        "my": "ဦးခင်ဝေ",
        "en": "U KHIN WAI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013009"
  },
  {
    "votes": 6639,
    "candidate": {
      "name": {
        "my": "ဦးသိန်း",
        "en": "U THEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013009"
  },
  {
    "votes": 48603,
    "candidate": {
      "name": {
        "my": "ဦးသိန်းညွှန့်",
        "en": "U THEIN NYUNT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013009"
  },
  {
    "votes": 2817,
    "candidate": {
      "name": {
        "my": "ဦးသန်းထွန်း",
        "en": "U THAN TUN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "DAPP",
    "constituency": "MMR013009"
  },
  {
    "votes": 45277,
    "candidate": {
      "name": {
        "my": "ဦးသူရမြင့်အောင်",
        "en": "U THURA MYINT MAUNG"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013025"
  },
  {
    "votes": 16460,
    "candidate": {
      "name": {
        "my": "ဦးထွန်းရှိန်",
        "en": "U TUN SHEIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013025"
  },
  {
    "votes": 15533,
    "candidate": {
      "name": {
        "my": "ဦးကိုကို",
        "en": "U KO KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013025"
  },
  {
    "votes": 9802,
    "candidate": {
      "name": {
        "my": "စောအေးကို",
        "en": "U SAW AYE KO"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "KPP",
    "constituency": "MMR013027"
  },
  {
    "votes": 74589,
    "candidate": {
      "name": {
        "my": "ဦးစိုးသာ",
        "en": "U SOE THA"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013027"
  },
  {
    "votes": 13483,
    "candidate": {
      "name": {
        "my": "ဦးသောင်ကြည်",
        "en": "U THAUNG KYI"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013027"
  },
  {
    "votes": 3797,
    "candidate": {
      "name": {
        "my": "ဒေါ်သော်တာဆိုင်",
        "en": "DAW THAWDA KHINE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013027"
  },
  {
    "votes": 13587,
    "candidate": {
      "name": {
        "my": "ဦးသာဝင်း",
        "en": "U THAR WIN"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "USADP",
    "constituency": "MMR013010"
  },
  {
    "votes": 4269,
    "candidate": {
      "name": {
        "my": "ဦးကိုကြီး",
        "en": "U KO GYEE"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NUP",
    "constituency": "MMR013010"
  },
  {
    "votes": 13176,
    "candidate": {
      "name": {
        "my": "ဦးမျိုးသန့်",
        "en": "U MYO THANT"
      }
    },
    "acclaim": "N",
    "parliament_code": "PTH",
    "year": 2010,
    "party": "NDF",
    "constituency": "MMR013010"
  }
];
