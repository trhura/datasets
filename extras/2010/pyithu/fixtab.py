

def main():
    with open('pyithu-hluttaw.txt', 'r') as iFile:
        region = None
        for line in iFile:
            line = line.strip()
            if line.find('|') == -1:
                region = line[line.find("MMR"):-1]
            else:
                print(region + " | " + line)

main()
