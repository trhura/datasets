## Datasets ##

* [Getting Started] (#getting-started)
* [Example Queries] (#examples)
* [States - states.js](#states)
* [Districts - districts.js](#districts)
* [Townships - townships.js](#townships)
* [Parliaments - parliaments.js](#parliaments)
* [Parties - parties.js](#parties)
* [Populations - populations.js](#populations)
* [Candidate Record - candidate_records.js](#candidate-records)
* [Amyothar Hlutttaw Geojson Data - amyothar.geojson](#amyothar-geojson)
* [Pyithu Hlutttaw Geojson Data - pyithu.geojson](#pyithu-geojson)

### Getting Started ###

* Just run `mongo init.js`. It will drop existing collections and import all data (except geojson).

###  Examples ###

* How many seats did USDP party for Ayeyarwady regional hluttaw in 2010 election?
```javascript
> db.candidate_records.find({'year': 2010, 'parliament_code': 'AYEYARWADY-RGH', "party": "USADP"}).count()
53
```

* How many seats of Ayeyarwady regional hluttaw are elected?
```javascript
> db.parliaments.find({'code': 'AYEYARWADY-RGH'}, {'elected_seats': 1})
{ "_id" : ObjectId("55f19d48d15125b4b07697c8"), "elected_seats" : 54 }
```

* How many seats of did USDP out of 53 candidates for Ayeyarwady regional hluttaw?
```javascript
> db.candidate_records.aggregate([ {$match: {'year': 2010, 'parliament_code': "AYEYARWADY-RGH"}},  {$sort: {votes: 1}}, {$group: {_id: "$constituency", party: {$last: "$party" }}}, {$group: {_id: "$party", count: {$sum : 1}}}])
{ "_id" : "USADP", "count" : 46 }
{ "_id" : "NUP", "count" : 7 }
```

* How many seats each party win in Pyithu Hluttaw?
```javascript
> db.candidate_records.aggregate([ {$match: {'year': 2010, 'parliament_code': "PTH"}},  {$sort: {votes: 1}}, {$group: {_id: "$constituency", party: {$last: "$party" }}}, {$group: {_id: "$party", count: {$sum : 1}}}])
{ "_id" : "Independent Candidates", "count" : 1 }
{ "_id" : "WDP", "count" : 6 }
{ "_id" : "TNP", "count" : 1 }
{ "_id" : "CPP", "count" : 2 }
{ "_id" : "KPP", "count" : 1 }
{ "_id" : "Chin National Party", "count" : 2 }
{ "_id" : "AMRDP", "count" : 3 }
{ "_id" : "PNO", "count" : 3 }
{ "_id" : "Rakhine Nationals Development Party", "count" : 8 }
{ "_id" : "NDF", "count" : 9 }
{ "_id" : "PDP2", "count" : 2 }
{ "_id" : "T8GSYOM", "count" : 1 }
{ "_id" : "INDP", "count" : 1 }
{ "_id" : "SNDP", "count" : 18 }
{ "_id" : "NUP", "count" : 14 }
{ "_id" : "USADP", "count" : 250 }
```

### States ###

* Get state details by `code`.

```javascript
    {
        "area_sqkm": null,
        "code": "MMR007",
        "name": {
            "en": "Bago East",
            "my": "ပဲခူး အရှေ့ပိုင်း"
        }
    },
```


### Districts ###

* Currently, Burmese names are not available yet.
* There is not specific attribute for state, use the first six digit of the `code`.

```javascript
	{

            "name": {
		"en": "Wein Kawng (Wein Kao)_Shan Special Region II (Wa)"
	    },

            "code": "MMR015D332"
	}
```

### Townships ###

* Use `dt_code` to get the district data.
* There is not specific attribute for state, use the first six digit of the `code`.

```javascript
    {
        "code": "MMR015312",
        "dt_code": "MMR015D331",
        "name": {
            "en": "Yin Pang",
            "my": "ရင်ဖန့်"
        }
    }
```

### Parliaments ###

```javascript
	{
	    "code": "PTH",
	    "name": {
		"en": " Pyithu Hluttaw",
		"my": "ပြည်သူ့ လွှတ်တော်"
	    },

	    "total_seats": 440,
	    "elected_seats": 330,
	    "appointed_seats": 110
	},
```

### Parties ###

```javascript
    {
      "name": {
        "en": "Mro National Development Party",
        "my": "မြိုတိုင်းရင်းသားဖွံ့ဖြိုး​ရေးပါတီ"
      },
      "code": "MNDP",
      "PartySeal": "https://storage.googleapis.com/staticassets/political/1-logo.png",
      "PartyFlag": "https://storage.googleapis.com/staticassets/political/1-flag.jpg"
    },
```

### Populations ###

* estimated eligible voters (18+ population ) by location (states, districts, townships)
* from 2014 Census data

```javascript
  {
    "location_code": "MMR015D002", // District
    "population": "280252"
  },
  {
    "location_code": "MMR111", // State
    "population": "3224090"
  },
  {
    "location_code": "MMR001001", //town
    "population": "202120"
  },
```

### Candidate Records ###

* Currently, 2010 and 2015 election records are availabe. (May add 2012 later.)
* The schema may be different based on the parliament seat.

```javascript

// Sample Amyothar Hluttaw record
{
    "constituency ": "Ayeyarwaddy-1", // FIXME: use code
    "year": 2010,
    "name": {
	"my": "ဦးစိုးဟန်လင်း",
	"en": "U SOE HAN LIN"
    },
    "party": "USDP", // use this to get party details
    "votes": 123033,
    "acclaim": "N",
    "parliament_code": "AMH" // use this to get parliament details
}

// Sample Pyithu Hluttaw record
{
    "acclaim": "N",
    "constituency": "MMR017024",
    "name": {
        "en": "U AUNG MYO",
        "my": "ဦးအောင်မျိုး"
    },
    "party": "MPP", // use this to get party details
    "votes": 4814,
    "year": 2010,
    "parliament_code": "PDH" // use this to get parliament details
}

// Sample Regional Hluttaw record
{
    "constituency": "MMR017009", // this may be suffixed by -1/-2/-3 coz there are multiple seats in each region
    "ethnic_seat": true, // exists only if it is for ethnic majority seat
    "ethnicity": " Kayin", // exists only if it is for ethnic majority seat
    "name": {
        "my": "ဒေါက်တာစောစိုင်းမွန်သာ",
        "en": "DR. SIMON THA"
    },
    "party": "KPP", // use this to get party details
    "votes": 204692,
    "acclaim": "N",
    "parliament_code": "AYEYARWADY-RGH", // use this to get parliament details
    "year": 2010
}
```

### Amyothar Geojson ###

* Constituencies geojson data for Amyothar Hluttaw
* Use `st_code` to get state code (to join with state data)

```javascript
            "properties": {
                "st_code": "MMR017",
                "name": {
                    "en": "Ayeyarwady-1",
                    "my": "ဧရာဝတီ-၁"
                }
            },
```

### Pyithu Geojson ###

* Constituencies geojson data for Pyithu Hluttaw
* Use `st_code`, `dt_code` and `ts_code` to get state, district and township codes (to join with collections)

```javascript
      "properties": {
        "Area": 1511.198952,
        "dt_code": "MMR013D001",
        "st_code": "MMR013",
        "ts_code": "MMR013004"
      },
```